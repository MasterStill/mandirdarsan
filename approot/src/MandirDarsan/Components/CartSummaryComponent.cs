﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using MandirDarsan.Models;
using Microsoft.AspNet.Http;

namespace MandirDarsan.Components
{
    [ViewComponent(Name = "CartSummary")]
    public class CartSummaryComponent : ViewComponent
    {
        public CartSummaryComponent(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }
        private ApplicationDbContext DbContext { get; }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (HttpContext.Session.GetString("Language")==null || HttpContext.Session.GetString("Language") == "")
            {
                HttpContext.Session.SetString("Language","English");
            }
            var cartItems = await GetCartItems();
            ViewBag.CartCount = cartItems.Count();
            if (ViewBag.CartCount == 0)
            {
                ViewBag.CartSummary = "No Items In Cart";
            }
            else
            { 
                ViewBag.CartSummary = string.Join("\n", cartItems.Distinct());
            }
            

            return View();
        }
        private async Task<IOrderedEnumerable<string>> GetCartItems()
        {
            var cart = ShoppingCart.GetCart(DbContext, HttpContext);
            return (await cart.GetCartItems())
                .Select(a => a.Product.Title)
                .OrderBy(x => x);
        }
    }
}