﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using Microsoft.AspNet.Mvc;
using MandirDarsan.Models;
namespace MandirDarsan.Components
{
    [ViewComponent(Name = "Culture")]
    public class CultureComponent : ViewComponent
    {
        public CultureComponent(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }
        private ApplicationDbContext DbContext { get; }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<Culture> cultures = new List<Culture>();
            cultures = DbContext.Cultures.Where(x => x.Active == true).Where(x=>x.DelFlg == false).Where(x=>x.Verified==true).ToList();
            ViewBag.CultureHash = "ng-init=\"culture=['English','Nepali','Hindi','Sanskrit'];subCulture=['','Nepali','Hindi','Sanskrit']\"";
            ViewBag.Culture = cultures;
            return View(cultures);
        }
    }
}