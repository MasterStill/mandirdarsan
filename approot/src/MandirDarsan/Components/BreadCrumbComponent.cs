﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using MandirDarsan.Models;

namespace MandirDarsan.Components
{
    [ViewComponent(Name = "BreadCrumb")]
    public class BreadCrumbComponent : ViewComponent
    {
        public BreadCrumbComponent(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }
        private ApplicationDbContext DbContext { get; }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var cartItems = await GetCartItems();
            ViewBag.CartCount = cartItems.Count();
            ViewBag.CartSummary = string.Join("\n", cartItems.Distinct());
            return View();
        }
        private async Task<IOrderedEnumerable<string>> GetCartItems()
        {
            var cart = ShoppingCart.GetCart(DbContext, HttpContext);
            return (await cart.GetCartItems())
                .Select(a => a.Product.Title)
                .OrderBy(x => x);
        }
    }
}