﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using MandirDarsan.Models;
namespace MandirDarsan.Components
{
    [ViewComponent(Name = "RecentPost")]
    public class RecentPostComponent : ViewComponent
    {
        public RecentPostComponent(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }
        private ApplicationDbContext DbContext { get; }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<MandirDarsan.Areas.Admin.Models.Library> articles = DbContext.Articles.Where(x=>x.DelFlg==false).Where(x=>x.Active==true).Where(x=>x.Verified==true).Take(5).ToList();
            return View(articles);
        }
    }
}