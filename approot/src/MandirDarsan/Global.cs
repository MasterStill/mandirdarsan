﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
//using System.Threading.Tasks;
using System.Xml.Serialization;
//using ECommerceHelper.Common;
using KellermanSoftware.CompareNetObjects;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
//using Microsoft.AspNet.Http.Internal;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Mvc;
//using Microsoft.AspNet.Mvc.Filters;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using ImageResizer.Plugins.SimpleFilters;
//using ECommerceHelper.CurrencyConverter;
namespace MandirDarsan
{
    public class RockmanduBase
    {
        private IAudit auditRepository ;
        private static ApplicationDbContext _context ;
        private static IApplicationEnvironment _applicationEnvironment;
        private static IHostingEnvironment _hostingEnvironment;
        public RockmanduBase(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment,IHostingEnvironment hostingEnvironment)
        {
            _applicationEnvironment = applicationEnvironment;
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            auditRepository = new AuditRepository(context);
        }
        public RockmanduBase()
        {
            _context = new ApplicationDbContext();
            auditRepository = new AuditRepository(_context);
        }
        public static string Priceformat(string price,string currency="INR")
        {
            //ICurrencyService ics = new CurrencyService();
            return "NRs. " + price; // + ics.ConvertAsync(Convert.ToDecimal(price), CurrencyCode.USD, CurrencyCode.USD); 
        }

        public static string VerificationStatus(string verified="")
        {
            if (verified.ToLower() == "true")
            {
                return "Verfied";
            }
            return "Un-Verified";
        }
        public static string GetUserName()
        {
            return "";
        }
        public int GetPageSize()
        {
            return 500;
        }
        public static string GetUserId()
        {
            return "";
        }
        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
        public static string GetJsonFromFormCollection(IFormCollection formCollections)
        {
            List<Culture> culture = new List<Culture>();
            culture = _context.Cultures.Where(x => x.DelFlg == false).ToList();
            string collections = string.Empty;
            collections += "{\"Tranlations\":[";
            foreach (var items in culture)
            {
                foreach (var translation in formCollections.Where(x => x.Key.EndsWith(items.Title)).Where(x => x.Value != ""))
                {
                    collections += "{\"key\":";
                    collections += "\"" + translation.Key + "\"";
                    collections += ",\"value\":";
                    if (translation.Key.Contains("Description")){ 
                        collections += "\"" + translation.Value.ToString().Replace(System.Environment.NewLine,"").Replace("\"","'") + "\"";
                    }
                    else
                    {
                        collections += "\"" + translation.Value + "\"";
                    }
                    collections += "},";
                }
            }
            collections += "]}";
            collections.Replace("},]}", "}]}");
            return collections;
        }
        public static string SaveAndGetProfileImage(IFormFile file, string path)
        {
            string imageUrl = string.Empty;
            if (file != null)
            {
                var fileName = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"').Replace(" ", "_");
                fileName = Guid.NewGuid().ToString();
                string filePath = string.Empty;
                if (_hostingEnvironment.IsDevelopment())
                {
                    filePath = _applicationEnvironment.ApplicationBasePath + "\\wwwroot\\" + path + "\\";
                }
                else
                if (_hostingEnvironment.IsProduction())
                {
                    filePath = "C:\\Projects\\mandirdarsan\\wwwroot\\" + path + "\\";
                }
                imageUrl = string.Concat("~/", path.Replace("\\", "/")) + "/" + fileName + ".jpg";
                file.SaveAs(filePath + fileName + ".jpg");
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_thumb.jpg", new ImageResizer.ResizeSettings(
              "width=100;height=100;format=jpg;mode=carve"));
                i.Build();
            }
            return imageUrl;
        }

        public static string SaveAndGetImage(IFormFile file,string path)
        {
            string imageUrl=string.Empty;
            if (file != null)
            {
                var fileName = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"').Replace(" ", "_");
                fileName = Guid.NewGuid().ToString();
                string filePath =  string.Empty;
                if (_hostingEnvironment.IsDevelopment())
                {
                    filePath = _applicationEnvironment.ApplicationBasePath + "\\wwwroot\\" + path + "\\";
                }
                else
                if (_hostingEnvironment.IsProduction    ())
                {
                    filePath = "C:\\Projects\\mandirdarsan\\wwwroot\\" + path + "\\";
                }
                imageUrl = string.Concat("~/",path.Replace("\\","/")) + "/" + fileName + ".jpg";
                file.SaveAs(filePath + fileName + ".jpg");
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_thumb.jpg", new ImageResizer.ResizeSettings(
              "width=100;height=100;format=jpg;mode=carve"));
                i.Build();
                ImageResizer.ImageJob j = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_bigthumb.jpg", new ImageResizer.ResizeSettings(
              "width=276;height=291;format=jpg;mode=carve;"));
                j.Build();
                ImageResizer.ImageJob k = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_smallthumb.jpg", new ImageResizer.ResizeSettings(
"width=140;height=95;format=jpg;mode=carve"));
                k.Build();
                ImageResizer.ImageJob l = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_productthumb.jpg", new ImageResizer.ResizeSettings(
"width=250;height=250;format=jpg;mode=carve"));
                l.Build();
                ImageResizer.ImageJob m = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_Carousel.jpg", new ImageResizer.ResizeSettings(
              "width=680;height=372;format=jpg;mode=carve;roundcorners=45,0,45,0"));
                m.Build();
                ImageResizer.ImageJob n = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_Banner.jpg", new ImageResizer.ResizeSettings(
"width=1200;height=500;format=jpg;mode=carve;roundcorners=45,0,45,0"));
                n.Build();
                ImageResizer.ImageJob q = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_NewsBigThumb.jpg", new ImageResizer.ResizeSettings(
"width=640;height=940;format=jpg;mode=max;roundcorners=45,0,45,0"));
                q.Build();
//                ImageResizer.ImageJob n = new ImageResizer.ImageJob(filePath + fileName + ".jpg", filePath + fileName + "_Banner.jpg", new ImageResizer.ResizeSettings(
//"width=1200;height=500;format=jpg;mode=carve;roundcorners=45,0,45,0"));
//                n.Build();
            }
            return imageUrl;
        }
        public static Boolean SaveAuditData(Guid libId,string userid,object oldObject, object newObject)
        {
            IAudit auditRepository = new AuditRepository(_context);
            RockAuditLog rockAuditLog = new RockAuditLog();
            
            rockAuditLog = RockmanduBase.AuditTrail(AuditActionType.Update, libId, oldObject, newObject, userid);
            auditRepository.Add(rockAuditLog, userid);
            auditRepository.Commit();
            return false;
        }
        //public static string FindallValues(AuditActionType Action, Guid KeyFieldID, Object OldObject, Object NewObject, string User)
        //{

        //    CompareLogic compObjects = new CompareLogic();
        //    compObjects.Config.MaxDifferences = 99;
        //    ComparisonResult compResult = compObjects.Compare(OldObject, NewObject);
        //    List<AuditDelta> deltaList = new List<AuditDelta>();
        //    foreach (var change in compResult.Differences)
        //    {
        //        AuditDelta delta = new AuditDelta();
        //        if (change.PropertyName.Substring(0, 1) == ".")
        //            delta.FieldName = change.PropertyName.Substring(1, change.PropertyName.Length - 1);
        //        delta.ValueBefore = change.Object1Value;
        //        delta.ValueAfter = change.Object2Value;
        //        deltaList.Add(delta);
        //    }
        //    RockAuditLog audit = new RockAuditLog();
        //    audit.AuditActionTypeENUM = (int)Action;
        //    //audit.DataModel = this.GetType().Name;
        //    audit.DateTimeStamp = DateTime.Now;
        //    audit.KeyFieldID = KeyFieldID;
        //    audit.ValueBefore = JsonConvert.SerializeObject(OldObject);
        //    audit.CreatedBy = User;
        //    audit.ValueAfter = JsonConvert.SerializeObject(NewObject);
        //    audit.Changes = JsonConvert.SerializeObject(deltaList);
        //    return audit;
        //}
        public static  RockAuditLog AuditTrail(AuditActionType Action, Guid KeyFieldID, Object OldObject, Object NewObject,string User)
        {
            // get the differance
            CompareLogic compObjects = new CompareLogic();
            compObjects.Config.MaxDifferences = 99;
            ComparisonResult compResult = compObjects.Compare(OldObject, NewObject);
            List<AuditDelta> deltaList = new List<AuditDelta>();
            foreach (var change in compResult.Differences)
            {
                AuditDelta delta = new AuditDelta();
                if (change.PropertyName.Substring(0, 1) == ".")
                delta.FieldName = change.PropertyName.Substring(1, change.PropertyName.Length - 1);
                delta.ValueBefore = change.Object1Value;
                delta.ValueAfter = change.Object2Value;
                deltaList.Add(delta);
            }
            RockAuditLog audit = new RockAuditLog();
            audit.AuditActionTypeENUM = (int)Action;
            //audit.DataModel = this.GetType().Name;
            audit.DateTimeStamp = DateTime.Now;
            audit.KeyFieldID = KeyFieldID;
            audit.ValueBefore = JsonConvert.SerializeObject(OldObject);
            audit.CreatedBy = User;
            audit.ValueAfter = JsonConvert.SerializeObject(NewObject);
            audit.Changes = JsonConvert.SerializeObject(deltaList);
            return audit;
        }
        public List<AuditChange> GetAudit(Guid Id)
        {
            List<AuditChange> rslt = new List<AuditChange>();

            var AuditTrail = _context.RockAuditLogs.Where(s => s.KeyFieldID == Id).OrderByDescending(s => s.DateTimeStamp);
            var serializer = new XmlSerializer(typeof(AuditDelta));
            foreach (var record in AuditTrail)
            {
                AuditChange Change = new AuditChange();
                Change.DateTimeStamp = record.DateTimeStamp.ToString();
                Change.AuditActionType = (AuditActionType)record.AuditActionTypeENUM;
                Change.AuditActionTypeName = Enum.GetName(typeof(AuditActionType), record.AuditActionTypeENUM);
                List<AuditDelta> delta = new List<AuditDelta>();
                delta = JsonConvert.DeserializeObject<List<AuditDelta>>(record.Changes);
                Change.Changes.AddRange(delta);
                foreach (var items in Change.Changes)
                {
                    RockDiff rd = new RockDiff(items.ValueBefore,items.ValueAfter);
                    items.Difference = rd.Build();
                    if (items.ValueBefore.Length > 100)
                    {
                        items.ValueBefore = RockmanduHtmlRemoval.StripTagsCharArray(items.ValueBefore);
                        items.ValueBefore = String.Concat(items.ValueBefore.Substring(0,100),"...");
                        
                    }
                    if (items.ValueAfter.Length > 100)
                    {
                        items.ValueAfter = RockmanduHtmlRemoval.StripTagsCharArray(items.ValueAfter);
                        items.ValueAfter = String.Concat(items.ValueAfter.Substring(0, 100), "...");
                    }
                    
                }
                rslt.Add(Change);
            }
            return rslt;
        }
        public string GetCultures()
        {
            //var cultures = 
            return "";
        }
        public interface ICultureBase
        {
            string MultiLingual { get; set; }
        }
        public static Dictionary<string,object> FetchCultureobject(string culture, string strMultilingual,string id,string imageurl=null,string title=null,string summary=null, string description = null,string price=null)
        {
            Dictionary<string, object> subdictionary = new Dictionary<string, object>();
            var kk = strMultilingual.Replace("{\"Tranlations\":", "");
            kk = kk.Substring(0, kk.Length - 1);
            var data = (JArray)JsonConvert.DeserializeObject(kk);
            foreach (var item in data)
            {
                var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);
                string heading = idata["key"].Value<string>();
                string content = idata["value"].Value<string>();
                if (!string.IsNullOrEmpty(content.Trim())) { 
                if (heading.IndexOf(culture) > -1)
                {
                    string removalContent = RockmanduHtmlRemoval.StripTagsCharArray(content);
                    if ((removalContent != "br&nbsp;") && (!string.IsNullOrEmpty(removalContent)) && (removalContent != "&nbsp;"))
                    {
                            // Console.WriteLine(heading.Replace(culture, "") + " : " + content);
                            if(heading.ToLower() != price) {

                                subdictionary.Add(heading.Replace(culture, ""), content);

                            }
                        }

                }
                }

            }

            string TranslationNeeded = " <Translation Needed Here>";
                 subdictionary.Add("Id",id);
                try
                {
                    if (!string.IsNullOrEmpty(title))
                    subdictionary.Add("Title", title + TranslationNeeded);
                }
            catch
                (Exception)
                {
                }
            try
            {
                if (!string.IsNullOrEmpty(imageurl))
                    subdictionary.Add("ImageUrl", imageurl); // + TranslationNeeded);
            }
            catch
                (Exception)
                {
                }
            try
            {
                if (!string.IsNullOrEmpty(summary))
                    subdictionary.Add("Summary", TranslationNeeded + summary);

            }
            catch
                (Exception)
            {
            }
            try
            {
                if (!string.IsNullOrEmpty(description))
                    subdictionary.Add("Description", TranslationNeeded + description);

            }
            catch
                (Exception)
            {
            }
            try
            {
                if (!string.IsNullOrEmpty(price))
                    subdictionary.Add("Price", price );

            }
            catch
                (Exception)
            {
            }
            return (subdictionary);
        }
        public string Getculture()
        {
          string culture =   Thread.CurrentThread.CurrentCulture.Name;
            return "";
        }

    }
    public static class RockmanduHtmlRemoval
    {
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
    
}
