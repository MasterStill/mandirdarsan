﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.ViewModels.Mandir
{
    public class ShoppingCartViewModel
    {
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
    public class ServiceViewModel
    {
    public Package Package { get; set; }
    public Puja Puja { get; set; }
    //public IEnumerable<Temple> Temple { get; set; }
    public IEnumerable<Package> Packages { get; set; }
    public IEnumerable<Puja> Pujas { get; set; }
        //public IEnumerable<God> God { get; set; }
    }

    public class SearchViewModeltest
    {
        public List<Search> Results { get; set; }
    }
    public class Search
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
    public class LibraryViewModel
    {
        public Library Library { get; set; }
        public List<Library> RecentPost { get; set; }
        public List<Library> RecentArticle { get; set; }
        public List<Library> RelatedArticle { get; set; }
        public List<Gallery> Images { get; set; }
        public string Culture { get; set; }

    }
    //public class ServiceViewModel
    //{
    //    //public  Library { get; set; }
    //   // public service
    //    //public List<Pa> RecentPost { get; set; }
    //    public List<Library> RecentArticle { get; set; }
    //    public List<Library> RelatedArticle { get; set; }
    //    public List<Gallery> Images { get; set; }
    //    public string Culture { get; set; }

    //}
    public class ProductViewModel
    {
        public OrderPhone op { get; set; }
        public Product prod { get; set; }

    }
    public class CartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public int DeleteId { get; set; }
    }
    public class ShoppingCartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public Guid DeleteId { get; set; }
    }
    public class StoreViewModel
    {
        public IQueryable<Categories> Categories { get; set; }
        public IQueryable<Puja> Puja { get; set; }
        public IQueryable<Package> Package { get; set; }
        public IQueryable<Product> Product { get; set; }
    }
}
