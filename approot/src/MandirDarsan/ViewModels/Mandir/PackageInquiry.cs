﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;

namespace Mandirdarsan.ViewModels.Mandir
{
    public class PackageInquiry
    {
        public Package Package { get; set; }
        public Enquiry Enquiry { get; set; }
    }
}
