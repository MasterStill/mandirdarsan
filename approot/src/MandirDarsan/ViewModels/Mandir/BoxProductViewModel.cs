﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Models;

namespace MandirDarsan.ViewModels.Mandir
{
    public class BoxProductViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Image { get; set; }
    }
}