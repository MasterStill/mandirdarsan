﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MandirDarsan.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Firstname")]
        public string Fname { get; set; }

        [Required]
        [Display(Name = "Lastname")]
        public string Lname { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class UserEditViewModel
    {
        [Required]
        [Display(Name = "Firstname")]
        public string Fname { get; set; }

        [Required]
        [Display(Name = "Lastname")]
        public string Lname { get; set; }

        [Required]
        [Display(Name = "Lastname")]
        public string UserName { get; set; }

        public String Telophone { get; set; }
        public String FAddress { get; set; }


        public String PCode { get; set; }
        public String City { get; set; }
        public String Country { get; set; }

        [Display(Name = "Biography")]
        public String Description { get; set; }

    }
}
