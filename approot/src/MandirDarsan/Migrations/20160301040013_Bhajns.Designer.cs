using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using MandirDarsan.Models;

namespace MandirDarsan.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160301040013_Bhajns")]
    partial class Bhajns
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.ActivityLog", b =>
                {
                    b.Property<Guid>("ActivityLogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("Table");

                    b.Property<Guid>("UserId");

                    b.Property<string>("UserId1");

                    b.HasKey("ActivityLogId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Bhajan", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("SelectedTags");

                    b.Property<string>("Summary");

                    b.Property<string>("Tags");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.Property<string>("YouTubeId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.BhajanTag", b =>
                {
                    b.Property<Guid>("BhajanId");

                    b.Property<Guid>("TagId");

                    b.HasKey("BhajanId", "TagId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Book", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<int>("Adult");

                    b.Property<int>("Children");

                    b.Property<string>("City")
                        .IsRequired();

                    b.Property<string>("Contact")
                        .IsRequired();

                    b.Property<Guid>("CountryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<DateTime>("Date");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Info");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid>("PackageId");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Cart", b =>
                {
                    b.Property<Guid>("RecordId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CartId");

                    b.Property<int>("Count");

                    b.Property<DateTime>("DateCreated");

                    b.Property<Guid>("ProductId");

                    b.HasKey("RecordId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Categories", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<int>("Categoryof");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<int>("DisplayOrder");

                    b.Property<bool>("HasSubCategory");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Contact", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Message")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Country", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.CountryState", b =>
                {
                    b.Property<Guid>("CountryId");

                    b.Property<Guid>("StateId");

                    b.HasKey("CountryId", "StateId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Culture", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Code");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Enquiry", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Message")
                        .IsRequired();

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid>("PackageId");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.File", b =>
                {
                    b.Property<int>("FileId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ContentId");

                    b.Property<string>("Controllerr");

                    b.Property<string>("FileLocation");

                    b.HasKey("FileId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Gallery", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Controller");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("DataId");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Summary");

                    b.Property<string>("Tag");

                    b.Property<string>("Title");

                    b.Property<string>("Url");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Globalization", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<Guid>("CultureId");

                    b.Property<bool?>("DelFlg");

                    b.Property<Guid>("LanguageTitleId");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 4000);

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.God", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<bool>("Display");

                    b.Property<string>("ImageUrl")
                        .IsRequired();

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Summary");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Guru", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<Guid>("CountryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("FullName");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.GuruPuja", b =>
                {
                    b.Property<Guid>("GuruId");

                    b.Property<Guid>("PujaId");

                    b.Property<Guid?>("PujaId1");

                    b.HasKey("GuruId", "PujaId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.GuruState", b =>
                {
                    b.Property<Guid>("GuruId");

                    b.Property<Guid>("StateId");

                    b.HasKey("GuruId", "StateId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.LanguageTitle", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<Guid?>("SiteSectionsId");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Library", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<DateTime?>("PublishDate");

                    b.Property<string>("Segment");

                    b.Property<string>("Summary");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.LibraryTag", b =>
                {
                    b.Property<Guid>("LibraryId");

                    b.Property<Guid>("TagId");

                    b.HasKey("LibraryId", "TagId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Mantra", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.Property<string>("YouTube");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Newsletter", b =>
                {
                    b.Property<Guid>("NewsletterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.HasKey("NewsletterId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Order", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 70);

                    b.Property<string>("City")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 40);

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 160);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 160);

                    b.Property<DateTime>("OrderDate");

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 24);

                    b.Property<string>("PostalCode");

                    b.Property<decimal>("Total");

                    b.Property<string>("Username");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.OrderDetail", b =>
                {
                    b.Property<Guid>("OrderDetailId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("OrderId");

                    b.Property<Guid>("ProductId");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("UnitPrice");

                    b.HasKey("OrderDetailId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.OrderPhone", b =>
                {
                    b.Property<int>("OrderPhoneId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.Property<Guid>("ProductId");

                    b.HasKey("OrderPhoneId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Package", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Activity");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("GroupSize");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("Map");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<double>("Price");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.PackagePuja", b =>
                {
                    b.Property<Guid>("PackageId");

                    b.Property<Guid>("PujaId");

                    b.Property<Guid?>("PujaId1");

                    b.HasKey("PackageId", "PujaId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.PackageTemple", b =>
                {
                    b.Property<Guid>("PackageId");

                    b.Property<Guid>("TempleId");

                    b.Property<Guid?>("TempleId1");

                    b.HasKey("PackageId", "TempleId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Product", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<Guid>("CategoryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ImageUrl1");

                    b.Property<string>("ImageUrl2");

                    b.Property<string>("ImageUrl3");

                    b.Property<string>("ImageUrl4");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<decimal>("Price");

                    b.Property<string>("Speciality")
                        .IsRequired();

                    b.Property<string>("Summary")
                        .IsRequired();

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Puja", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.RockAuditLog", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<int>("AuditActionTypeENUM");

                    b.Property<string>("Changes");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("DataModel");

                    b.Property<DateTime>("DateTimeStamp");

                    b.Property<bool?>("DelFlg");

                    b.Property<Guid>("KeyFieldID");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("ValueAfter");

                    b.Property<string>("ValueBefore");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.SiteSection", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Siteset", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AboutText");

                    b.Property<bool?>("Active");

                    b.Property<string>("Copyright");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Email");

                    b.Property<string>("FacebookUrl");

                    b.Property<string>("Logo");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Phone");

                    b.Property<string>("PrivacyPolicy");

                    b.Property<string>("TermsandCondition");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.Property<string>("WelcomeText");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.State", b =>
                {
                    b.Property<Guid>("StateId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CountryId");

                    b.Property<string>("Name");

                    b.Property<string>("Title");

                    b.HasKey("StateId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Tag", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Temple", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Address");

                    b.Property<string>("Address2");

                    b.Property<string>("Area");

                    b.Property<string>("Country");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("DelFlg");

                    b.Property<string>("Description");

                    b.Property<string>("District");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("MultiLingual");

                    b.Property<string>("MultiLingualFiles");

                    b.Property<string>("Summary");

                    b.Property<string>("Title");

                    b.Property<bool?>("Verified");

                    b.Property<string>("VerifiedBy");

                    b.Property<DateTime?>("VerifiedDate");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.TempleGod", b =>
                {
                    b.Property<Guid>("TempleId");

                    b.Property<Guid>("GodId");

                    b.HasKey("TempleId", "GodId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.TemplePuja", b =>
                {
                    b.Property<Guid>("TempleId");

                    b.Property<Guid>("PujaId");

                    b.HasKey("TempleId", "PujaId");
                });

            modelBuilder.Entity("MandirDarsan.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("City");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Country");

                    b.Property<string>("Description");

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FAddress");

                    b.Property<string>("FName");

                    b.Property<string>("LName");

                    b.Property<string>("LanguagePreference");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PCode");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("Telophone");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.ActivityLog", b =>
                {
                    b.HasOne("MandirDarsan.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId1");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.BhajanTag", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Bhajan")
                        .WithMany()
                        .HasForeignKey("BhajanId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Tag")
                        .WithMany()
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Book", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Package")
                        .WithMany()
                        .HasForeignKey("PackageId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Cart", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.CountryState", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.State")
                        .WithMany()
                        .HasForeignKey("StateId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Enquiry", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Package")
                        .WithMany()
                        .HasForeignKey("PackageId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Globalization", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.LanguageTitle")
                        .WithMany()
                        .HasForeignKey("LanguageTitleId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Guru", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.GuruPuja", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Guru")
                        .WithMany()
                        .HasForeignKey("PujaId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Puja")
                        .WithMany()
                        .HasForeignKey("PujaId1");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.GuruState", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Guru")
                        .WithMany()
                        .HasForeignKey("GuruId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.State")
                        .WithMany()
                        .HasForeignKey("StateId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.LanguageTitle", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.SiteSection")
                        .WithMany()
                        .HasForeignKey("SiteSectionsId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.LibraryTag", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Library")
                        .WithMany()
                        .HasForeignKey("LibraryId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Tag")
                        .WithMany()
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.OrderDetail", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Order")
                        .WithMany()
                        .HasForeignKey("OrderId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.OrderPhone", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.PackagePuja", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Package")
                        .WithMany()
                        .HasForeignKey("PujaId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Puja")
                        .WithMany()
                        .HasForeignKey("PujaId1");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.PackageTemple", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Package")
                        .WithMany()
                        .HasForeignKey("TempleId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Temple")
                        .WithMany()
                        .HasForeignKey("TempleId1");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.Product", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Categories")
                        .WithMany()
                        .HasForeignKey("CategoryId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.State", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.TempleGod", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.God")
                        .WithMany()
                        .HasForeignKey("GodId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Temple")
                        .WithMany()
                        .HasForeignKey("TempleId");
                });

            modelBuilder.Entity("MandirDarsan.Areas.Admin.Models.TemplePuja", b =>
                {
                    b.HasOne("MandirDarsan.Areas.Admin.Models.Puja")
                        .WithMany()
                        .HasForeignKey("PujaId");

                    b.HasOne("MandirDarsan.Areas.Admin.Models.Temple")
                        .WithMany()
                        .HasForeignKey("TempleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("MandirDarsan.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("MandirDarsan.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("MandirDarsan.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
