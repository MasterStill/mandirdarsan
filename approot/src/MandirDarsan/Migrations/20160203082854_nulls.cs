using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace MandirDarsan.Migrations
{
    public partial class nulls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_BhajanTag_Tag_TagId", table: "BhajanTag");
            migrationBuilder.DropForeignKey(name: "FK_Book_Country_CountryId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Book_Package_PackageId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Cart_Product_ProductId", table: "Cart");
            migrationBuilder.DropForeignKey(name: "FK_CountryState_State_StateId", table: "CountryState");
            migrationBuilder.DropForeignKey(name: "FK_Enquiry_Package_PackageId", table: "Enquiry");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_Culture_CultureId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_LanguageTitle_LanguageTitleId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Guru_Country_CountryId", table: "Guru");
            migrationBuilder.DropForeignKey(name: "FK_GuruState_Guru_GuruId", table: "GuruState");
            migrationBuilder.DropForeignKey(name: "FK_LanguageTitle_SiteSection_SiteSectionsId", table: "LanguageTitle");
            migrationBuilder.DropForeignKey(name: "FK_LibraryTag_Tag_TagId", table: "LibraryTag");
            migrationBuilder.DropForeignKey(name: "FK_Order_Country_CountryId", table: "Order");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Order_OrderId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Product_ProductId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderPhone_Product_ProductId", table: "OrderPhone");
            migrationBuilder.DropForeignKey(name: "FK_Product_Categories_CategoryId", table: "Product");
            migrationBuilder.DropForeignKey(name: "FK_State_Country_CountryId", table: "State");
            migrationBuilder.DropForeignKey(name: "FK_Temple_Country_CountryId", table: "Temple");
            migrationBuilder.DropForeignKey(name: "FK_TempleGod_God_GodId", table: "TempleGod");
            migrationBuilder.DropForeignKey(name: "FK_TemplePuja_Puja_PujaId", table: "TemplePuja");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "Name", table: "Puja");
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Temple",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Siteset",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "SiteSection",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "RockAuditLog",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Puja",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Product",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Package",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Library",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "LanguageTitle",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Guru",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "God",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Globalization",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Gallery",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Enquiry",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Culture",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Country",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Categories",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Book",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Bhajan",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_BhajanTag_Tag_TagId",
                table: "BhajanTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Country_CountryId",
                table: "Book",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Package_PackageId",
                table: "Book",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Product_ProductId",
                table: "Cart",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_CountryState_State_StateId",
                table: "CountryState",
                column: "StateId",
                principalTable: "State",
                principalColumn: "StateId",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Package_PackageId",
                table: "Enquiry",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_Culture_CultureId",
                table: "Globalization",
                column: "CultureId",
                principalTable: "Culture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_LanguageTitle_LanguageTitleId",
                table: "Globalization",
                column: "LanguageTitleId",
                principalTable: "LanguageTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Guru_Country_CountryId",
                table: "Guru",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_GuruState_Guru_GuruId",
                table: "GuruState",
                column: "GuruId",
                principalTable: "Guru",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_LanguageTitle_SiteSection_SiteSectionsId",
                table: "LanguageTitle",
                column: "SiteSectionsId",
                principalTable: "SiteSection",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_LibraryTag_Tag_TagId",
                table: "LibraryTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_Country_CountryId",
                table: "Order",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Order_OrderId",
                table: "OrderDetail",
                column: "OrderId",
                principalTable: "Order",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Product_ProductId",
                table: "OrderDetail",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderPhone_Product_ProductId",
                table: "OrderPhone",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Categories_CategoryId",
                table: "Product",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Temple_Country_CountryId",
                table: "Temple",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TempleGod_God_GodId",
                table: "TempleGod",
                column: "GodId",
                principalTable: "God",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TemplePuja_Puja_PujaId",
                table: "TemplePuja",
                column: "PujaId",
                principalTable: "Puja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_BhajanTag_Tag_TagId", table: "BhajanTag");
            migrationBuilder.DropForeignKey(name: "FK_Book_Country_CountryId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Book_Package_PackageId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Cart_Product_ProductId", table: "Cart");
            migrationBuilder.DropForeignKey(name: "FK_CountryState_State_StateId", table: "CountryState");
            migrationBuilder.DropForeignKey(name: "FK_Enquiry_Package_PackageId", table: "Enquiry");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_Culture_CultureId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_LanguageTitle_LanguageTitleId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Guru_Country_CountryId", table: "Guru");
            migrationBuilder.DropForeignKey(name: "FK_GuruState_Guru_GuruId", table: "GuruState");
            migrationBuilder.DropForeignKey(name: "FK_LanguageTitle_SiteSection_SiteSectionsId", table: "LanguageTitle");
            migrationBuilder.DropForeignKey(name: "FK_LibraryTag_Tag_TagId", table: "LibraryTag");
            migrationBuilder.DropForeignKey(name: "FK_Order_Country_CountryId", table: "Order");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Order_OrderId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Product_ProductId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderPhone_Product_ProductId", table: "OrderPhone");
            migrationBuilder.DropForeignKey(name: "FK_Product_Categories_CategoryId", table: "Product");
            migrationBuilder.DropForeignKey(name: "FK_State_Country_CountryId", table: "State");
            migrationBuilder.DropForeignKey(name: "FK_Temple_Country_CountryId", table: "Temple");
            migrationBuilder.DropForeignKey(name: "FK_TempleGod_God_GodId", table: "TempleGod");
            migrationBuilder.DropForeignKey(name: "FK_TemplePuja_Puja_PujaId", table: "TemplePuja");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "Title", table: "Puja");
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Temple",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Siteset",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "SiteSection",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "RockAuditLog",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Puja",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Puja",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Puja",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Puja",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Puja",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Puja",
                nullable: false);
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Puja",
                nullable: true);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Product",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Package",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Library",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "LanguageTitle",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Guru",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "God",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Globalization",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Gallery",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Enquiry",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Culture",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Categories",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Book",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "VerifiedDate",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Verified",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "DelFlg",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                table: "Bhajan",
                nullable: false);
            migrationBuilder.AddForeignKey(
                name: "FK_BhajanTag_Tag_TagId",
                table: "BhajanTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Country_CountryId",
                table: "Book",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Package_PackageId",
                table: "Book",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Product_ProductId",
                table: "Cart",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_CountryState_State_StateId",
                table: "CountryState",
                column: "StateId",
                principalTable: "State",
                principalColumn: "StateId",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Package_PackageId",
                table: "Enquiry",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_Culture_CultureId",
                table: "Globalization",
                column: "CultureId",
                principalTable: "Culture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_LanguageTitle_LanguageTitleId",
                table: "Globalization",
                column: "LanguageTitleId",
                principalTable: "LanguageTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Guru_Country_CountryId",
                table: "Guru",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_GuruState_Guru_GuruId",
                table: "GuruState",
                column: "GuruId",
                principalTable: "Guru",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_LanguageTitle_SiteSection_SiteSectionsId",
                table: "LanguageTitle",
                column: "SiteSectionsId",
                principalTable: "SiteSection",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_LibraryTag_Tag_TagId",
                table: "LibraryTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_Country_CountryId",
                table: "Order",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Order_OrderId",
                table: "OrderDetail",
                column: "OrderId",
                principalTable: "Order",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Product_ProductId",
                table: "OrderDetail",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderPhone_Product_ProductId",
                table: "OrderPhone",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Categories_CategoryId",
                table: "Product",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Temple_Country_CountryId",
                table: "Temple",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TempleGod_God_GodId",
                table: "TempleGod",
                column: "GodId",
                principalTable: "God",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TemplePuja_Puja_PujaId",
                table: "TemplePuja",
                column: "PujaId",
                principalTable: "Puja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
