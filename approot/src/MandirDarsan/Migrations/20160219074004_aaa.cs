using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace MandirDarsan.Migrations
{
    public partial class aaa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_BhajanTag_Tag_TagId", table: "BhajanTag");
            migrationBuilder.DropForeignKey(name: "FK_Book_Country_CountryId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Book_Package_PackageId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Cart_Product_ProductId", table: "Cart");
            migrationBuilder.DropForeignKey(name: "FK_CountryState_State_StateId", table: "CountryState");
            migrationBuilder.DropForeignKey(name: "FK_Enquiry_Package_PackageId", table: "Enquiry");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_Culture_CultureId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_LanguageTitle_LanguageTitleId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Guru_Country_CountryId", table: "Guru");
            migrationBuilder.DropForeignKey(name: "FK_GuruState_Guru_GuruId", table: "GuruState");
            migrationBuilder.DropForeignKey(name: "FK_LibraryTag_Tag_TagId", table: "LibraryTag");
            migrationBuilder.DropForeignKey(name: "FK_Order_Country_CountryId", table: "Order");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Order_OrderId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Product_ProductId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderPhone_Product_ProductId", table: "OrderPhone");
            migrationBuilder.DropForeignKey(name: "FK_Product_Categories_CategoryId", table: "Product");
            migrationBuilder.DropForeignKey(name: "FK_State_Country_CountryId", table: "State");
            migrationBuilder.DropForeignKey(name: "FK_TempleGod_God_GodId", table: "TempleGod");
            migrationBuilder.DropForeignKey(name: "FK_TemplePuja_Puja_PujaId", table: "TemplePuja");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddForeignKey(
                name: "FK_BhajanTag_Tag_TagId",
                table: "BhajanTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Country_CountryId",
                table: "Book",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Package_PackageId",
                table: "Book",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Product_ProductId",
                table: "Cart",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_CountryState_State_StateId",
                table: "CountryState",
                column: "StateId",
                principalTable: "State",
                principalColumn: "StateId",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Package_PackageId",
                table: "Enquiry",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_Culture_CultureId",
                table: "Globalization",
                column: "CultureId",
                principalTable: "Culture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_LanguageTitle_LanguageTitleId",
                table: "Globalization",
                column: "LanguageTitleId",
                principalTable: "LanguageTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Guru_Country_CountryId",
                table: "Guru",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_GuruState_Guru_GuruId",
                table: "GuruState",
                column: "GuruId",
                principalTable: "Guru",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_LibraryTag_Tag_TagId",
                table: "LibraryTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_Country_CountryId",
                table: "Order",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Order_OrderId",
                table: "OrderDetail",
                column: "OrderId",
                principalTable: "Order",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Product_ProductId",
                table: "OrderDetail",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderPhone_Product_ProductId",
                table: "OrderPhone",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Categories_CategoryId",
                table: "Product",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TempleGod_God_GodId",
                table: "TempleGod",
                column: "GodId",
                principalTable: "God",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TemplePuja_Puja_PujaId",
                table: "TemplePuja",
                column: "PujaId",
                principalTable: "Puja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_BhajanTag_Tag_TagId", table: "BhajanTag");
            migrationBuilder.DropForeignKey(name: "FK_Book_Country_CountryId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Book_Package_PackageId", table: "Book");
            migrationBuilder.DropForeignKey(name: "FK_Cart_Product_ProductId", table: "Cart");
            migrationBuilder.DropForeignKey(name: "FK_CountryState_State_StateId", table: "CountryState");
            migrationBuilder.DropForeignKey(name: "FK_Enquiry_Package_PackageId", table: "Enquiry");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_Culture_CultureId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Globalization_LanguageTitle_LanguageTitleId", table: "Globalization");
            migrationBuilder.DropForeignKey(name: "FK_Guru_Country_CountryId", table: "Guru");
            migrationBuilder.DropForeignKey(name: "FK_GuruState_Guru_GuruId", table: "GuruState");
            migrationBuilder.DropForeignKey(name: "FK_LibraryTag_Tag_TagId", table: "LibraryTag");
            migrationBuilder.DropForeignKey(name: "FK_Order_Country_CountryId", table: "Order");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Order_OrderId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderDetail_Product_ProductId", table: "OrderDetail");
            migrationBuilder.DropForeignKey(name: "FK_OrderPhone_Product_ProductId", table: "OrderPhone");
            migrationBuilder.DropForeignKey(name: "FK_Product_Categories_CategoryId", table: "Product");
            migrationBuilder.DropForeignKey(name: "FK_State_Country_CountryId", table: "State");
            migrationBuilder.DropForeignKey(name: "FK_TempleGod_God_GodId", table: "TempleGod");
            migrationBuilder.DropForeignKey(name: "FK_TemplePuja_Puja_PujaId", table: "TemplePuja");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddForeignKey(
                name: "FK_BhajanTag_Tag_TagId",
                table: "BhajanTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Country_CountryId",
                table: "Book",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Book_Package_PackageId",
                table: "Book",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Product_ProductId",
                table: "Cart",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_CountryState_State_StateId",
                table: "CountryState",
                column: "StateId",
                principalTable: "State",
                principalColumn: "StateId",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Package_PackageId",
                table: "Enquiry",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_Culture_CultureId",
                table: "Globalization",
                column: "CultureId",
                principalTable: "Culture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Globalization_LanguageTitle_LanguageTitleId",
                table: "Globalization",
                column: "LanguageTitleId",
                principalTable: "LanguageTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Guru_Country_CountryId",
                table: "Guru",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_GuruState_Guru_GuruId",
                table: "GuruState",
                column: "GuruId",
                principalTable: "Guru",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_LibraryTag_Tag_TagId",
                table: "LibraryTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_Country_CountryId",
                table: "Order",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Order_OrderId",
                table: "OrderDetail",
                column: "OrderId",
                principalTable: "Order",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetail_Product_ProductId",
                table: "OrderDetail",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_OrderPhone_Product_ProductId",
                table: "OrderPhone",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Categories_CategoryId",
                table: "Product",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TempleGod_God_GodId",
                table: "TempleGod",
                column: "GodId",
                principalTable: "God",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TemplePuja_Puja_PujaId",
                table: "TemplePuja",
                column: "PujaId",
                principalTable: "Puja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
