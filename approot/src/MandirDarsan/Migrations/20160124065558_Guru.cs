using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace MandirDarsan.Migrations
{
    public partial class Guru : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bhajan",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    SelectedTags = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    Timeduration = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false),
                    YouTubeId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bhajan", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Categoryof = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: false),
                    HasSubCategory = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Message = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Culture",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Culture", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    FileId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContentId = table.Column<string>(nullable: true),
                    Controllerr = table.Column<string>(nullable: true),
                    FileLocation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.FileId);
                });
            migrationBuilder.CreateTable(
                name: "Gallery",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Controller = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DataId = table.Column<string>(nullable: true),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Tag = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "God",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Display = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_God", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Library",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    PublishDate = table.Column<DateTime>(nullable: false),
                    Segment = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Newsletter",
                columns: table => new
                {
                    NewsletterId = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Newsletter", x => x.NewsletterId);
                });
            migrationBuilder.CreateTable(
                name: "Package",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Activity = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    GroupSize = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    Map = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Package", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Puja",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Puja", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "RockAuditLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    AuditActionTypeENUM = table.Column<int>(nullable: false),
                    Changes = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DataModel = table.Column<string>(nullable: true),
                    DateTimeStamp = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    KeyFieldID = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    ValueAfter = table.Column<string>(nullable: true),
                    ValueBefore = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RockAuditLog", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SiteSection",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteSection", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Siteset",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AboutText = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Copyright = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    FacebookUrl = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    PrivacyPolicy = table.Column<string>(nullable: true),
                    TermsandCondition = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false),
                    WelcomeText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siteset", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FAddress = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    LName = table.Column<string>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    PCode = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    Telophone = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUser", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRole", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    ImageUrl1 = table.Column<string>(nullable: true),
                    ImageUrl2 = table.Column<string>(nullable: true),
                    ImageUrl3 = table.Column<string>(nullable: true),
                    ImageUrl4 = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Speciality = table.Column<string>(nullable: false),
                    Summary = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Guru",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guru", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Guru_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    PostalCode = table.Column<string>(nullable: true),
                    Total = table.Column<decimal>(nullable: false),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    StateId = table.Column<Guid>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.StateId);
                    table.ForeignKey(
                        name: "FK_State_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Temple",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Dis = table.Column<string>(nullable: false),
                    GeoLocation = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: false),
                    InFooter = table.Column<bool>(nullable: false),
                    Location = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Temple", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Temple_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Adult = table.Column<int>(nullable: false),
                    Children = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Contact = table.Column<string>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Info = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Book_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Book_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Enquiry",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Message = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "PackagePuja",
                columns: table => new
                {
                    PackageId = table.Column<Guid>(nullable: false),
                    PujaId = table.Column<Guid>(nullable: false),
                    PujaId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackagePuja", x => new { x.PackageId, x.PujaId });
                    table.ForeignKey(
                        name: "FK_PackagePuja_Package_PujaId",
                        column: x => x.PujaId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackagePuja_Puja_PujaId1",
                        column: x => x.PujaId1,
                        principalTable: "Puja",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "LanguageTitle",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    SiteSectionsId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LanguageTitle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LanguageTitle_SiteSection_SiteSectionsId",
                        column: x => x.SiteSectionsId,
                        principalTable: "SiteSection",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "BhajanTag",
                columns: table => new
                {
                    BhajanId = table.Column<Guid>(nullable: false),
                    TagId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BhajanTag", x => new { x.BhajanId, x.TagId });
                    table.ForeignKey(
                        name: "FK_BhajanTag_Bhajan_BhajanId",
                        column: x => x.BhajanId,
                        principalTable: "Bhajan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BhajanTag_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "LibraryTag",
                columns: table => new
                {
                    LibraryId = table.Column<Guid>(nullable: false),
                    TagId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryTag", x => new { x.LibraryId, x.TagId });
                    table.ForeignKey(
                        name: "FK_LibraryTag_Library_LibraryId",
                        column: x => x.LibraryId,
                        principalTable: "Library",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LibraryTag_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "ActivityLog",
                columns: table => new
                {
                    ActivityLogId = table.Column<Guid>(nullable: false),
                    Action = table.Column<string>(nullable: true),
                    Table = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    UserId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityLog", x => x.ActivityLogId);
                    table.ForeignKey(
                        name: "FK_ActivityLog_ApplicationUser_UserId1",
                        column: x => x.UserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserLogin<string>", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRoleClaim<string>", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserRole<string>", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "Cart",
                columns: table => new
                {
                    RecordId = table.Column<Guid>(nullable: false),
                    CartId = table.Column<string>(nullable: true),
                    Count = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Cart_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "OrderPhone",
                columns: table => new
                {
                    OrderPhoneId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPhone", x => x.OrderPhoneId);
                    table.ForeignKey(
                        name: "FK_OrderPhone_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "GuruPuja",
                columns: table => new
                {
                    GuruId = table.Column<Guid>(nullable: false),
                    PujaId = table.Column<Guid>(nullable: false),
                    PujaId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuruPuja", x => new { x.GuruId, x.PujaId });
                    table.ForeignKey(
                        name: "FK_GuruPuja_Guru_PujaId",
                        column: x => x.PujaId,
                        principalTable: "Guru",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GuruPuja_Puja_PujaId1",
                        column: x => x.PujaId1,
                        principalTable: "Puja",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "OrderDetail",
                columns: table => new
                {
                    OrderDetailId = table.Column<Guid>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetail", x => x.OrderDetailId);
                    table.ForeignKey(
                        name: "FK_OrderDetail_Order_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetail_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "CountryState",
                columns: table => new
                {
                    CountryId = table.Column<Guid>(nullable: false),
                    StateId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryState", x => new { x.CountryId, x.StateId });
                    table.ForeignKey(
                        name: "FK_CountryState_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CountryState_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "StateId",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "GuruState",
                columns: table => new
                {
                    GuruId = table.Column<Guid>(nullable: false),
                    StateId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuruState", x => new { x.GuruId, x.StateId });
                    table.ForeignKey(
                        name: "FK_GuruState_Guru_GuruId",
                        column: x => x.GuruId,
                        principalTable: "Guru",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GuruState_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "StateId",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "PackageTemple",
                columns: table => new
                {
                    PackageId = table.Column<Guid>(nullable: false),
                    TempleId = table.Column<Guid>(nullable: false),
                    TempleId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageTemple", x => new { x.PackageId, x.TempleId });
                    table.ForeignKey(
                        name: "FK_PackageTemple_Package_TempleId",
                        column: x => x.TempleId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackageTemple_Temple_TempleId1",
                        column: x => x.TempleId1,
                        principalTable: "Temple",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "TempleGod",
                columns: table => new
                {
                    TempleId = table.Column<Guid>(nullable: false),
                    GodId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TempleGod", x => new { x.TempleId, x.GodId });
                    table.ForeignKey(
                        name: "FK_TempleGod_God_GodId",
                        column: x => x.GodId,
                        principalTable: "God",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TempleGod_Temple_TempleId",
                        column: x => x.TempleId,
                        principalTable: "Temple",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "TemplePuja",
                columns: table => new
                {
                    TempleId = table.Column<Guid>(nullable: false),
                    PujaId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplePuja", x => new { x.TempleId, x.PujaId });
                    table.ForeignKey(
                        name: "FK_TemplePuja_Puja_PujaId",
                        column: x => x.PujaId,
                        principalTable: "Puja",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TemplePuja_Temple_TempleId",
                        column: x => x.TempleId,
                        principalTable: "Temple",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "Globalization",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CultureId = table.Column<Guid>(nullable: false),
                    DelFlg = table.Column<bool>(nullable: false),
                    LanguageTitleId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    MultiLingual = table.Column<string>(nullable: true),
                    MultiLingualFiles = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Globalization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Globalization_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Globalization_LanguageTitle_LanguageTitleId",
                        column: x => x.LanguageTitleId,
                        principalTable: "LanguageTitle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");
            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName");
            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("ActivityLog");
            migrationBuilder.DropTable("BhajanTag");
            migrationBuilder.DropTable("Book");
            migrationBuilder.DropTable("Cart");
            migrationBuilder.DropTable("Contact");
            migrationBuilder.DropTable("CountryState");
            migrationBuilder.DropTable("Enquiry");
            migrationBuilder.DropTable("File");
            migrationBuilder.DropTable("Gallery");
            migrationBuilder.DropTable("Globalization");
            migrationBuilder.DropTable("GuruPuja");
            migrationBuilder.DropTable("GuruState");
            migrationBuilder.DropTable("LibraryTag");
            migrationBuilder.DropTable("Newsletter");
            migrationBuilder.DropTable("OrderDetail");
            migrationBuilder.DropTable("OrderPhone");
            migrationBuilder.DropTable("PackagePuja");
            migrationBuilder.DropTable("PackageTemple");
            migrationBuilder.DropTable("RockAuditLog");
            migrationBuilder.DropTable("Siteset");
            migrationBuilder.DropTable("TempleGod");
            migrationBuilder.DropTable("TemplePuja");
            migrationBuilder.DropTable("AspNetRoleClaims");
            migrationBuilder.DropTable("AspNetUserClaims");
            migrationBuilder.DropTable("AspNetUserLogins");
            migrationBuilder.DropTable("AspNetUserRoles");
            migrationBuilder.DropTable("Bhajan");
            migrationBuilder.DropTable("Culture");
            migrationBuilder.DropTable("LanguageTitle");
            migrationBuilder.DropTable("Guru");
            migrationBuilder.DropTable("State");
            migrationBuilder.DropTable("Library");
            migrationBuilder.DropTable("Tag");
            migrationBuilder.DropTable("Order");
            migrationBuilder.DropTable("Product");
            migrationBuilder.DropTable("Package");
            migrationBuilder.DropTable("God");
            migrationBuilder.DropTable("Puja");
            migrationBuilder.DropTable("Temple");
            migrationBuilder.DropTable("AspNetRoles");
            migrationBuilder.DropTable("AspNetUsers");
            migrationBuilder.DropTable("SiteSection");
            migrationBuilder.DropTable("Categories");
            migrationBuilder.DropTable("Country");
        }
    }
}
