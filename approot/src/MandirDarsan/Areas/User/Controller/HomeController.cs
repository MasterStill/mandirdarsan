﻿using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Mvc.Abstractions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI.WebControls;
using ImageResizer.Resizing;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Controllers;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Account;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
namespace MandirDarsan.Areas.User.Controllers
{
    [Area("User")]
    [Authorize]
    public class HomeController : Controller
    {
        private IApplicationEnvironment _applicationEnvironment;
        private IHostingEnvironment _hostingEnvironment;
        //private static ApplicationDbContext _context;
        private RockmanduBase rBase;
       // private IGod godRepository;
        private static ApplicationDbContext _context;
        string pathToCreate = "Images\\Users";
        public HomeController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;
            rBase = new RockmanduBase(_context, _applicationEnvironment, _hostingEnvironment);
        }

        [HttpGet]
        public IActionResult LanguageSelection()
        {
            //if (id.ToLower() == "languageselection")
            //{
            ViewBag.Dashboardsegment = "Profile";
            ViewBag.DashboardSubsegment = "Langaugeselection";
            var languages = _context.Globalizations
                .Include(x => x.Culture)
                .Where(x => x.DelFlg == false)
                .Where(x => x.LanguageTitle.Title.ToLower().Contains("i can read and write in")).ToList();
            UserRoleLanguageViewModel rlvm = new UserRoleLanguageViewModel();
            rlvm.Globalization = languages;
            return View(rlvm);
        }

        [HttpGet]
        [Authorize]
        public IActionResult EditUserDetails()
        {
            UserEditViewModel uvm=  new UserEditViewModel();
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();

            uvm.Telophone = user.Telophone;
            uvm.Description = user.Description;
            uvm.City = user.City;
            uvm.Country = user.Country;
            uvm.Fname = user.FName;
            uvm.FAddress = user.FAddress;
            uvm.Lname = user.LName;
            return View(uvm);
        }

        [HttpPost]
        public async Task<IActionResult> EditUserDetails(IFormCollection formCollections)
        {
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
            try
            {
            user.FName = formCollections.Where(x => x.Key == "Fname").SingleOrDefault().Value;


            }
            catch (Exception)
            {
                    
                
            }

            try
            {
                user.LName = formCollections.Where(x => x.Key == "Lname").SingleOrDefault().Value;

            }
            catch (Exception)
            {
                    
               
            }
            try
            {
                user.Telophone = formCollections.Where(x => x.Key == "Telophone").SingleOrDefault().Value;

            }
            catch (Exception)
            {
                    
               
            }
            try
            {
                user.City = formCollections.Where(x => x.Key == "City").SingleOrDefault().Value;

            }
            catch (Exception)
            {
                    
               
            }
            try
            {
                user.Country = formCollections.Where(x => x.Key == "Country").SingleOrDefault().Value;

            }
            catch (Exception)
            {
                    
               
            }
            try
            {
                user.Description = formCollections.Where(x => x.Key == "Description").SingleOrDefault().Value;

            }
            catch (Exception)
            {
                    
               
            }

            _context.Users.Update(user);
            _context.SaveChanges();

            return Redirect("~/Admin/Home");
        }


        [HttpPost]
        public IActionResult ChangePicture(IFormCollection fc , IFormFile profilepic)
        {
           // fc.Files.
            var currentUser = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
            currentUser.Image = RockmanduBase.SaveAndGetProfileImage(profilepic, pathToCreate);
            _context.Users.Update(currentUser);
            _context.SaveChanges();
            return Redirect("~/Admin/Home");
        }
    }
}
public class UserRoleLanguageViewModel
{
    public List<Globalization> Globalization { get; set; }
    public List<UserLanguage> UserLanguage { get; set; }
}

public class UserLanguage
{
    public  Culture Culture { get; set; }
    public string Selected { get; set; }
}