﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using MandirDarsan.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity.ValueGeneration;
using PagedList;

namespace MandirDarsan.Areas.Admin.Models
{
    public class Categories : RockMandatory, IRockmanduBase
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public String Title { get; set; }
        [Required]
        public Boolean HasSubCategory { get; set; }
        public int Categoryof { get; set; }
        public int DisplayOrder { get; set; }
        public string Description { get; set; }
        public List<Product> Products { get; set; }
    }
    //public class Search// : RockMandatory, IRockmanduBase
    //{
    //    [Key]
    //    public Guid Id { get; set; }
    //    public String Title { get; set; }
    //}
    public class Product : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        [Required]
        [Display(Name = "Name")]
        public String Title { get; set; }
        [Required]
        [Display(Name = "Description")]
        public String Description { get; set; }
        [Required]
        [Display(Name = "Price")]
        public Decimal Price { get; set; }
        [Required]
        [Display(Name = "Summary")]
        public String Summary { get; set; }
        [Required]
        [Display(Name = "Speciality")]
        public String Speciality { get; set; }
        [Display(Name = "Main Image")]
        public String ImageUrl1 { get; set; }
        [Display(Name = "Secondary Image-1")]
        public String ImageUrl2 { get; set; }
        [Display(Name = "Secondary Image-2")]
        public String ImageUrl3 { get; set; }
        [Display(Name = "Secondary Image-3")]
        public String ImageUrl4 { get; set; }
        public string ImageUrl { get; set; }
        public Categories Category { get; set; }
    }
    public class File
    {
        public int FileId { get; set; }
        public string FileLocation { get; set; }
        public string Controllerr { get; set; }
        public string ContentId { get; set; }
    }
    
    //public class LibraryViewModel
    //{
    //    public Guid Id { get; set; }
    //    public string Title { get; set; }
    //    [Display(Name = "Image")]
    //    public string ImageUrl { get; set; }
    //    public string Description { get; set; }
    //    public DateTime PublishDate { get; set; }
    //    public List<LibraryTag> PostTags { get; set; }
    //    public string Summary { get; set; }
    //    public string MultiLingual { get; set; }
    //}
    public class Library : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Display(Name = "Image")]
        public string ImageUrl { get; set; }
        [Required]
        public string Description { get; set; }
        [DataType(DataType.Date)]
//        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}"]
        public DateTime? PublishDate { get; set; }
        public string Segment { get; set; }
        public List<LibraryTag> PostTags { get; set; }
        public string Summary { get; set; }
    }
    public class Book : RockMandatory, IRockmanduBase
    {
        [Key]
        public Guid Id { get; set; }
        [Display(Name = "Full Name")]
        [Required]
        public String Name { get; set; }
        [Display(Name = "Email")]
        [Required]
        public String Email { get; set; }
        [Display(Name = "Contact")]
        [Required]
        public String Contact { get; set; }
        public Guid CountryId { get; set; }
        public virtual Country Country { get; set; }
        [Display(Name = "City")]
        [Required]
        public String City { get; set; }
        [Display(Name = "Full Address")]
        [Required]
        public String Address { get; set; }
        public Guid PackageId { get; set; }
        public virtual Package Package { get; set; }
        [Display(Name = "Preferrable Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Display(Name = "Children")]
        [Range(1, 10)]
        [Required]
        public int Children { get; set; }
        [Display(Name = "Adult")]
        [Range(1, 10)]
        [Required]
        public int Adult { get; set; }
        [Display(Name = "Additional Information")]
        [DataType(DataType.MultilineText)]
        public String Info { get; set; }

    }
    public class Cart
    {
        //public Cart()
        //{
        //    this.Product = new Product();
        //}
        [Key]
        public Guid RecordId { get; set; }
        public string CartId { get; set; }
        public Guid ProductId { get; set; }
        public int Count { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        public virtual Product Product { get; set; }

    }
    public class Contact
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Full Name")]
        public String Name { get; set; }
        [Required]
        [Display(Name = "Full Address")]
        public String Address { get; set; }
        [Required]
        [Display(Name = "Email Address")]
        public String Email { get; set; }
        [Required]
        [Display(Name = "Phone/ Mobile No.")]
        public String Phone { get; set; }
        [Required]
        [Display(Name = "Inquiry/ Comments")]
        [DataType(DataType.MultilineText)]
        public String Message { get; set; }

    }
    public class Country : RockMandatory, IRockmanduBase// : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }

        public List<CountryState> States { get; set; }
    }
    public class Enquiry : RockMandatory, IRockmanduBase
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public String Name { get; set; }
        [Required]
        public String Email { get; set; }
        public Guid PackageId { get; set; }
        public virtual Package Package { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public String Message { get; set; }
    }
    public class Gallery : RockMandatory, IRockmanduBase// : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        public string Controller { get; set; }
        public string DataId { get; set; }
        public string Tag { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Url {get; set; }
        public string Summary { get; set; }
    }
    public class God : RockMandatory, IRockmanduBase// : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Gods")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Some Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Image")]
        public String ImageUrl { get; set; }
        [Display(Name = "Famous Temple")]
        public bool Display { get; set; }
        public string Summary { get; set; }
        public List<TempleGod> TempleGods { get; set; } 
        
        //public  virtual ICollection<Temple> Temples { get; set; }
        //public virtual ICollection<Puja> Puja { get; set; }
    }
    public class Siteset : RockMandatory, IRockmanduBase
    {
        
        public Guid Id { get; set; }
        public string AboutText { get; set; }
        public string WelcomeText { get; set; }
        public string Copyright { get; set; }
        public string Logo { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string FacebookUrl { get; set; }
        public string PrivacyPolicy { get; set; }
        public string TermsandCondition { get; set; }
    }
    public class Newsletter
    {
        [Key]
        public Guid NewsletterId { get; set; }
        [Required]
        [Display(Name = "Email")]
        public String Email { get; set; }
    }
    public class Order // : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        public string Username { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        [StringLength(160)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name")]
        [StringLength(160)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [StringLength(70)]
        public string Address { get; set; }
        [Required(ErrorMessage = "City is required")]
        [StringLength(40)]
        public string City { get; set; }
        public string PostalCode { get; set; }
        //public Guid CountryId { get; set; }
        //public virtual Country Country { get; set; }
        [Required(ErrorMessage = "Phone is required")]
        [StringLength(24)]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Email Address is required")]
        [Display(Name = "Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
          ErrorMessage = "Email is is not valid.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public decimal Total { get; set; }
        public DateTime OrderDate { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
    public class OrderDetail // : RockMandatory
    {
        [Key]
        public Guid OrderDetailId { get; set; }
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
    public class OrderPhone // : RockMandatory
    {
        public int OrderPhoneId { get; set; }
        [Required]
        [Display(Name = "Full Name")]
        public String Name { get; set; }
        [Required]
        [Display(Name = "Email")]
        public String Email { get; set; }
        [Required]
        [Display(Name = "Contact Number")]
        public String Phone { get; set; }
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
    public class BillingDetails // : RockMandatory
    {
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
    public class DeliveryDetails // : RockMandatory
    {
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    public class Package : RockMandatory, IRockmanduBase// : RockMandatory
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Activity { get; set; }
        public string GroupSize { get; set; }
        public string Summary { get; set; }
        public string itinerary { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Map { get; set; }
        
        public List<PackageTemple> PackageTemples { get; set; }
        public List<PackagePuja> PackagePuja { get; set; }
    }
    //public class SiteImage // : RockMandatory
    //{culture
    //    public int SiteImageId { get; set; }
    //    public String ImageUrl { get; set; }
    //}
    public class Guru : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; } 
        public string FullName { get; set; }
        public string Description { get; set; }
        public Guid CountryId { get; set; }
        
        public virtual Country Country { get; set; }
        public List<GuruState> GuruState { get; set; }
        public List<GuruPuja> GuruPujas { get; set; }
    }
    public class State
    {
        public Guid StateId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }

        public Guid CountryId { get; set; }
        public virtual Country Country { get; set; }
        public List<GuruState> GuruState { get; set; }
    }
    public class Puja : RockMandatory, IRockmanduBase// : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public string Summary { get; set; }
        public String ImageUrl { get; set; }
        public double Price { get; set; }
        public List<TemplePuja>  TemplePujas { get; set; }
        public List<PackagePuja> PackagePujas { get; set; }
        public List<GuruPuja> GuruPujas { get; set; }
    }

    public class Mantra:RockMandatory,IRockmanduBase
    {
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string YouTube { get; set; }
    }
    public class Bhajan : RockMandatory, IRockmanduBase
    {
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string YouTubeId { get; set; }
        public string ImageUrl { get; set; }
        //[ScaffoldColumn(false)]
        //public string SelectedTags { get; set; }
        //public List<BhajanTag> BhajanTags { get; set; }
    }
    public class AdminBhajanViewModel
    {
        public Bhajan Bhajan { get; set; }
        public List<SelectListItem> AllTagsInTable { get; set; }
        public string[] SelectedTags { get; set; }
    }
    public class RockAuditLog : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public Guid KeyFieldID { get; set; }
        public System.DateTime DateTimeStamp { get; set; }
        public string DataModel { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public string Changes { get; set; }
        public int AuditActionTypeENUM { get; set; }
    }
    public class AuditDelta
    {
        public string FieldName { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
        public string Difference { get; set; }
    }
    public enum AuditActionType
    {
        Create = 1,
        Update,
        Delete
    }
    public class AuditChange
    {
        public string DateTimeStamp { get; set; }
        public AuditActionType AuditActionType { get; set; }
        public string AuditActionTypeName { get; set; }
        public List<AuditDelta> Changes { get; set; }
        public AuditChange()
        {
            Changes = new List<AuditDelta>();
        }
    }
    public class Globalization : RockMandatory,IRockmanduBase
    {
        public Guid Id { get; set; }
        [Display(Name = "Title")]
        public Guid LanguageTitleId { get; set; }
        [Required]
        [StringLength(4000)]
        public string Value { get; set; }
        [Required]
        public Guid CultureId { get; set; }
       // public Guid SiteSectionsId { get; set; }

        public virtual Culture Culture { get; set; }
        public virtual LanguageTitle LanguageTitle { get; set; }
    }
    public class LanguageTitle : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public Guid? SiteSectionsId { get; set; }
        public virtual SiteSection SiteSections { get; set; }


    }
    public class SiteSection : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
     }
    public class Culture : RockMandatory, IRockmanduBase
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }
    public class Language
    {
        public string MultiLingual { get; set; }
        public string MultiLingualFiles{ get; set; }
    }
    public class RockMandatory : Language
    {
        [ScaffoldColumn(false)]
        public string ModifiedBy { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }
        [ScaffoldColumn(false)]
        public string VerifiedBy { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? ModifiedDate { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? VerifiedDate { get; set; }
        [ScaffoldColumn(false)]
        public Boolean? Verified { get; set; }
        [ScaffoldColumn(false)]
        public Boolean? DelFlg { get; set; }
        [ScaffoldColumn(false)]
        public Boolean? Active { get; set; }

    }
    public class ActivityLog
    {
        public Guid ActivityLogId { get; set; }
        public string  Action { get; set; }
        public string Table { get; set; }
        public Guid UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    public class Temple : RockMandatory, IRockmanduBase// : RockMandatory
    {
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string Address { get; set; } 
        public string Address2 { get; set; } 
        public string Area { get; set; }
        public string District{ get; set; } 
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Summary { get; set; }
        public List<PackageTemple> PackageTemples { get; set; }
        public List<TemplePuja> TemplePujas { get; set; }
        public List<TempleGod> TempleGods { get; set; }
        //public IPagedList<Temple> Temples { get; set; }
    }
    public class TempleGod
    {
        public Guid TempleId { get; set; }    
        public Guid GodId { get; set; }
        public Temple Temple { get; set; }
        public God God { get; set; }
    }
    public class Tag
    {
        [Key]
        public Guid Id { get; set; }
        [Display(Name="Tag Name")]
        public string Title { get; set; }
        public List<LibraryTag> LibraryTags { get; set; }
        //public List<BhajanTag> BhajanTags { get; set; }
    }
    public class LibraryTag
    {
        public Guid LibraryId { get; set; }
        public Guid TagId { get; set; }

        public Library Library { get; set; }
        public Tag Tag { get; set; }
    }
    //public class BhajanTag
    //{
    //    public Guid BhajanId { get; set; }
    //    public Guid TagId { get; set; }

    //    public Tag Tag { get; set; }
    //    public Bhajan Bhajan { get; set; }
    //    }
    public class PackageTemple
    {
        public Guid PackageId { get; set; }
        public Guid TempleId { get; set; }


        public Temple Temple { get; set; }
        public Package Package { get; set; }
    }
    public class TemplePuja
    {
        public Guid TempleId { get; set; }
        public Guid PujaId { get; set; }

        public Temple Temple { get; set; }
        public Puja Puja { get; set; }
    }
    public class PackagePuja
    {
        public Guid PackageId { get; set; }
        public Guid PujaId { get; set; }

        public Package Package { get; set; }
        public Puja Puja { get; set; }
    }
    public class GuruPuja
    {

        public Guid GuruId { get; set; }
        public Guid PujaId { get; set; }


        public Guru Guru { get; set; }
        public Puja Puja { get; set; }
        
    }
    public class CountryState
    {

        public Guid CountryId { get; set; }
        public Guid StateId { get; set; }


        public Country Country { get; set; }
        public State State { get; set; }

    }
    public class GuruState
    {
        public Guid GuruId { get; set; }
        public Guid StateId { get; set; }
        public Guru Country { get; set; }
        public State State { get; set; }
    }
    public class Currency
    {
        public Guid Id { get; set; }
        public string CurrencyName { get; set; }
        public string UnitPrice { get; set; }
    }
}