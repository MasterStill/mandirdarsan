﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using PagedList;
using MandirDarsan.Areas.Admin.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.Extensions.PlatformAbstractions;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class GalleriesController : Controller
    {
        string pathToCreate = "Images\\Gallery";
        // IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private IGallery galleryRepository;
        private IApplicationEnvironment _applicationEnvironment;
        private IHostingEnvironment _hostingEnvironment;
        public GalleriesController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _applicationEnvironment = applicationEnvironment;
            _hostingEnvironment = hostingEnvironment;
            rBase = new RockmanduBase(_context, _applicationEnvironment,_hostingEnvironment);
            galleryRepository = new GalleryRepository(_context);
        }
        public ActionResult Index(string sortingOrder, string searchTerm, string filterValue, int? pageNo, string cid, string ctrl, FormCollection form)
        {

            if (ctrl.ToLower() == "banners")
            {
                ViewBag.DashboardSegment = "admin";
                ViewBag.DashboardSubSegment = "bannerimage";
            }
            if (ctrl.ToLower() == "carousel")
            {
                ViewBag.DashboardSegment = "admin";
                ViewBag.DashboardSubSegment = "carouselimage";
            }
            if (ctrl.ToLower() == "galleryimage" && cid.ToLower() == "main")
            {
                ViewBag.DashboardSegment = "admin";
                ViewBag.DashboardSubSegment = "galleryimage";
            }

            if (cid == null) { cid = "Main" ;}
            if (ctrl != null && cid != null)
            {
                int totalPageSize = rBase.GetPageSize();
                var noOfPage = (pageNo ?? 1);
                if (!User.IsInRole("Admin"))
                {
                    try
                    {
                        return View(galleryRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Controller == ctrl).Where(x => x.DataId == cid).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
                    }
                    catch (Exception)
                    {
                        return View(galleryRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Controller == ctrl).Where(x => x.DataId == cid).ToPagedList(noOfPage, totalPageSize));
                    }
                }
                try
                {
                    return View(galleryRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).Where(x => x.Controller.ToLower() == ctrl.ToLower()).Where(x => x.DataId.ToLower() == cid.ToLower()).ToPagedList(noOfPage, totalPageSize));
                }
                catch
                (Exception)
                {
                    return View(galleryRepository.GetAll().Where(x => x.Controller.ToLower() == ctrl.ToLower()).Where(x => x.DataId.ToLower() == cid.ToLower()).ToPagedList(noOfPage, totalPageSize));
                }
                
            }
            return HttpNotFound();
        }
        public ActionResult Create(string ctrl, string cid)
        {
            if (ctrl != null && cid != null)
            {
                if (ctrl.ToLower() == "banners" || ctrl.ToLower() == "carousel")
                {
                    if (User.IsInRole("Admin"))
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Gallery gallery, IEnumerable<IFormFile> files, IFormCollection form)
        {
            if (ModelState.IsValid)
            {
                string cid=string.Empty;
                try
                {
                    var  ccid = form.Where(x => x.Key == "cid").Select(x => x.Value).SingleOrDefault();
                    cid= ccid.ToString();
                }
                catch (Exception)
                {
                    {
                        cid = "main";
                    }
                }
                var ctrl= form.Where(x => x.Key == "ctrl").Select(x=>x.Value).SingleOrDefault();
                if (files != null)
                {
                    foreach (IFormFile file in files)
                    {
                        Gallery g = new Gallery();

                        g.Title = gallery.Title;
                        g.Description = gallery.Description;
                        g.Summary = gallery.Summary;
                        g.Url = gallery.Url;
                        g.Id = Guid.NewGuid();
                        galleryRepository = new GalleryRepository(_context);
                        g.ImageUrl = RockmanduBase.SaveAndGetImage(file, pathToCreate);
                        g.Controller = ctrl;
                        g.DataId = cid;
                        galleryRepository.Add(g, User.Identity.Name);
                        galleryRepository.Commit();
                    }
                }
                return Redirect("~/Admin/Galleries/Index/" + ctrl + "/" + cid);

               // return RedirectToAction("Index", new { @cid = cid, @ctrl = ctrl });
            }
            return View(gallery);
        }
        public ActionResult Edit(string controller,Guid id,string cid)
        {
            if (cid == null)
            {
                return HttpNotFound();
            }
            Gallery gallery = _context.Galleries.Where(x=>x.Id== Guid.Parse(cid)).SingleOrDefault();
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Gallery gallery, FormCollection form, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                var cid = HttpUtility.HtmlEncode(form["DataId"].ToString());
                var ctrl = HttpUtility.HtmlEncode(form["Controller"].ToString());
                _context.Entry(gallery).State = EntityState.Modified;
                if (ImageUrl != null)
                {
                    gallery.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate);
                }
                if (User.IsInRole("Admin") || gallery.CreatedBy == User.Identity.Name)
                {
                    _context.SaveChanges();
                }
                return Redirect("~/Admin/Galleries/" + ctrl + "/" + cid);
                // return RedirectToAction("Index", new { cid = @cid, ctrl = @ctrl });
            }
            return View(gallery);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(galleryRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedGallery = galleryRepository.GetSingle(Guid.Parse(id));
            galleryRepository.Verify(unverifiedGallery, User.Identity.Name);
            galleryRepository.Commit();
            return RedirectToAction("Unverified");
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            if (!User.IsInRole("Admin"))
            {
                return View(galleryRepository.GetAll().Where(x => x.DelFlg == false).ToPagedList(noOfPage, totalPageSize));
            }
            var god = galleryRepository.GetAllDeleted(User.Identity.Name);
            return View(god.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var god = galleryRepository.GetSingle(id);
            galleryRepository.Undelete(god);
            galleryRepository.Commit();
            return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Gallery> god = galleryRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in god)
            {
                galleryRepository.ConfirmDelete(items.Id);
            }
            galleryRepository.Commit();
            return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            galleryRepository.ConfirmDelete(id);
            galleryRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
            return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {

            var gallery = galleryRepository.GetSingle(id);
            string ctrl = gallery.Controller;
            string cid = gallery.DataId;
            galleryRepository.Delete(gallery);
            galleryRepository.Commit();
            galleryRepository.Commit();
            return RedirectToAction("Index", new { ctrl = ctrl, cid = cid });
        }
    }
}
