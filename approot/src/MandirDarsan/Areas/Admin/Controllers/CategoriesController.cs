﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CategoriesController : Controller
	{
		
        string pathToCreate = "Images\\Categories";
        IApplicationEnvironment _applicationEnvironment;
        IHostingEnvironment _hostingEnvironment;

        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private ICategories categoriesRepository;
        public CategoriesController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;
            categoriesRepository =new CategoryRepository(_context);
			rBase = new RockmanduBase(_context, _applicationEnvironment,_hostingEnvironment);
        }

        // GET: Admin/Categories
        public ActionResult Index(int? pageNo, string searchTerm="")
        {

            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "categories";


            var categories = categoriesRepository.GetAll().Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
			return View(categories.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/Categories/Create
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "categories";
            return View();
        }

        // POST: Admin/Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
        			public ActionResult Create(Categories categories, IFormCollection formCollections)
		
        {
            if (ModelState.IsValid)
            {
			categories.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
			
        			    
			
				
                categoriesRepository.Add(categories, User.Identity.Name);
                categoriesRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(categories);
        }

        public ActionResult Edit(Guid id)
        {

            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "categories";
            var categories = categoriesRepository.GetSingle(id);
            if (categories == null)
            {
                return HttpNotFound();
            }    
            return View(categories);
        }

        // POST: Admin/Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
					public ActionResult Edit(Categories categories, IFormCollection formCollections)
		
		
        {
            if (ModelState.IsValid)
            {
			var oldcategories = _context.Categories.AsNoTracking().FirstOrDefault(x=>x.Id == categories.Id);
        	categories.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
                if (User.IsInRole("Admin") || categories.CreatedBy == User.Identity.Name)
                {
                    categoriesRepository.Edit(categories, User.Identity.Name);
                    categoriesRepository.Commit();
                }
                RockmanduBase.SaveAuditData(categories.Id,User.Identity.Name,oldcategories,categories);
	                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categories);
        }
        public ActionResult Trash(int? pageNo)
        {
            var categories = categoriesRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(categories.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {

            var categories = categoriesRepository.GetSingle(id);
            categoriesRepository.Undelete(categories);
            categoriesRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Categories> categories = categoriesRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in categories)
            {
                categoriesRepository.ConfirmDelete(items.Id);
            }
            categoriesRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            categoriesRepository.ConfirmDelete(id);
            categoriesRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  categories =  categoriesRepository.GetSingle(id);
             categoriesRepository.Delete( categories);
             categoriesRepository.Commit();
             categoriesRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
