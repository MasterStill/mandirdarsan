﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;

namespace MandirDarsan.Areas.Admin.Controllers

{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class SiteSectionsController : Controller
	{
		
        string pathToCreate = "Images\\SiteSection";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        private ISiteSection siteSectionRepository;
        public SiteSectionsController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            siteSectionRepository =new SiteSectionRepository(_context);
        }

        // GET: Admin/SiteSections
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            var siteSection = siteSectionRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x => x.Title.Contains(searchTerm.ToLower()));
			int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
			return View(siteSection.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/SiteSections/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/SiteSections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create(SiteSection siteSection)
		
        {
            if (ModelState.IsValid)
            {
				siteSection.CreatedBy =  User.Identity.Name;
                siteSection.ModifiedBy = User.Identity.Name;
        			    
			
				
                siteSectionRepository.Add(siteSection, User.Identity.Name);
                siteSectionRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(siteSection);
        }

        public ActionResult Edit(Guid id)
        {
           var siteSection = siteSectionRepository.GetSingle(id);
            if (siteSection == null)
            {
                return HttpNotFound();
            }    
            return View(siteSection);
        }

        // POST: Admin/SiteSections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit(SiteSection siteSection)
		
		
        {
            if (ModelState.IsValid)
            {
				siteSection.ModifiedBy = User.Identity.Name;
					                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(siteSection);
        }
        public ActionResult Trash(int? pageNo)
        {
            var siteSection = siteSectionRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(siteSection.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var siteSection = siteSectionRepository.GetSingle(id);
            siteSectionRepository.Undelete(siteSection);
            siteSectionRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<SiteSection> siteSection = siteSectionRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in siteSection)
            {
                siteSectionRepository.ConfirmDelete(items.Id);
            }
            siteSectionRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            siteSectionRepository.ConfirmDelete(id);
            siteSectionRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  siteSection =  siteSectionRepository.GetSingle(id);
             siteSectionRepository.Delete( siteSection);
             siteSectionRepository.Commit();
             siteSectionRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
