﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Migrations;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ProductsController : Controller
	{
        string pathToCreate = "Images\\Product";
        IHostingEnvironment _hostingEnvironment;
        private IApplicationEnvironment _applicationEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private IProduct productRepository;
        public ProductsController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;

            productRepository = new ProductRepository(_context);
			rBase = new RockmanduBase(_context, _applicationEnvironment, _hostingEnvironment);
        }
        public ActionResult Index(int? pageNo, string searchTerm="")
        {

            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "products";

            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(productRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(productRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));


   //         var products = _context.Products.Include(p => p.Category);
			//var product = productRepository.AllIncluding(x=>x.Category).Where(x => x.CreatedBy == User.Identity.Name || x.ModifiedBy == User.Identity.Name).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			//int totalPageSize = rBase.GetPageSize();
   //         var noOfPage = (pageNo ?? 1);
			//return View(product.ToPagedList(noOfPage ,totalPageSize ));
        }
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "products";
            List<SelectListItem> categoryList = new List<SelectListItem>();
            foreach (var item in _context.Categories)
            {
                categoryList.Add(new SelectListItem()
                {
                    Text = item.Title,
                    Value = item.Id.ToString()
                });
            }

            ViewBag.CategoryId = categoryList; //new SelectList(, "Id", "Title");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product product, IFormFile ImageUrl, IFormCollection formCollections)
        {
            if (ModelState.IsValid)
            {
			    product.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
    		    product.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate)  ;    
	            productRepository.Add(product, User.Identity.Name);
                productRepository.Commit();
                return Redirect("~/Admin/Galleries/Create/Products/" + product.Id);
                //return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(_context.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }
        public ActionResult Edit(Guid id)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "products";
            var product = productRepository.GetSingle(id);
            ViewBag.CategoryId = new SelectList(_context.Categories, "Id", "Title");
            if (product == null)
            {
                return HttpNotFound();
            }    
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Product product,IFormFile IImageUrl, IFormCollection formCollections)
        {
            if (ModelState.IsValid)
            {
			var oldproduct = _context.Products.AsNoTracking().FirstOrDefault(x=>x.Id == product.Id);
        	product.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
				 
				 if ( product.ImageUrl !=null && IImageUrl != null){    
				product.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);   
			 }
                if (User.IsInRole("Admin") || product.CreatedBy == User.Identity.Name)
                {
                    productRepository.Edit(product, User.Identity.Name);
                    productRepository.Commit();
                }
                RockmanduBase.SaveAuditData(product.Id,User.Identity.Name,oldproduct,product);
	                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(_context.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "products";
            var product = productRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(product.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var product = productRepository.GetSingle(id);
            productRepository.Undelete(product);
            productRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Product> product = productRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in product)
            {
                productRepository.ConfirmDelete(items.Id);
            }
            productRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            productRepository.ConfirmDelete(id);
            productRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
            return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  product =  productRepository.GetSingle(id);
             productRepository.Delete( product);
             productRepository.Commit();
             productRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "products";
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(productRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var UnverifiedProduct = productRepository.GetSingle(Guid.Parse(id));
            productRepository.Verify(UnverifiedProduct, User.Identity.Name);
            productRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
