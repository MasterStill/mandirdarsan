﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
using Newtonsoft.Json;
namespace MandirDarsan.Areas.Admin.Controllers
{
	[Authorize]
	[Area("Admin")]
    public class BhajansController : Controller
	{
        string pathToCreate = "Images\\Bhajan";
        IApplicationEnvironment _applicationEnvironment;
	    private IHostingEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
	    private RockmanduBase rBase;// = new RockmanduBase();
        private IBhajan bhajanRepository;
        public BhajansController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;
            bhajanRepository =new BhajanRepository(_context);
            rBase =new RockmanduBase(_context,applicationEnvironment,hostingEnvironment);
        }
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            ViewBag.Segment = "Bhajans";
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";

            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(bhajanRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(bhajanRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
   //         var bhajan = bhajanRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			//int totalPageSize = rBase.GetPageSize();
   //         var noOfPage = (pageNo ?? 1);
			//return View(bhajan.ToPagedList(noOfPage ,totalPageSize ));
        }
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";
            AdminBhajanViewModel vm = new AdminBhajanViewModel();
            var alltags = _context.Tags.ToList().Select(x=>new {x.Id,x.Title});
            String serializedResult = JsonConvert.SerializeObject(alltags);
            ViewBag.Tags = serializedResult;
            return View(vm);
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bhajan bhajan, IFormFile ImageUrl, IFormCollection formCollections)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";
            if (ModelState.IsValid)
                if (ModelState.IsValid)
                {
                    //bhajan.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
                    bhajan.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate);
                    bhajanRepository.Add(bhajan, User.Identity.Name);
                    bhajanRepository.Commit();
                    return RedirectToAction("Index");
                }

            return View(bhajan);
        }
        public ActionResult Edit(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";
            var bhajan = bhajanRepository.GetSingle(id);
            if (bhajan == null)
            {
                return HttpNotFound();
            }    
            return View(bhajan);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Bhajan bhajan, IFormFile IImageUrl, IFormCollection formCollections)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";
            if (ModelState.IsValid)
            {
                var oldbhajan = _context.Gods.AsNoTracking().FirstOrDefault(x => x.Id == bhajan.Id);
                bhajan.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);

                if (bhajan.ImageUrl != null && IImageUrl != null)
                {
                    bhajan.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);
                }
                if (User.IsInRole("Admin") || bhajan.CreatedBy == User.Identity.Name)
                {
                    bhajanRepository.Edit(bhajan, User.Identity.Name);
                    bhajanRepository.Commit();
                }
                RockmanduBase.SaveAuditData(bhajan.Id, User.Identity.Name, oldbhajan, bhajan);
                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bhajan);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedbhajans";
            ViewBag.DashboardSegment = "contribute";
            ViewBag.DashboardSubSegment = "bhajan";
            var bhajan = bhajanRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(bhajan.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var bhajan = bhajanRepository.GetSingle(id);
            bhajanRepository.Undelete(bhajan);
            bhajanRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Bhajan> bhajan = bhajanRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in bhajan)
            {
                bhajanRepository.ConfirmDelete(items.Id);
            }
            bhajanRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            bhajanRepository.ConfirmDelete(id);
            bhajanRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  bhajan =  bhajanRepository.GetSingle(id);
             bhajanRepository.Delete( bhajan);
             bhajanRepository.Commit();
             bhajanRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(bhajanRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedBhajan = bhajanRepository.GetSingle(Guid.Parse(id));
            bhajanRepository.Verify(unverifiedBhajan, User.Identity.Name);
            bhajanRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}