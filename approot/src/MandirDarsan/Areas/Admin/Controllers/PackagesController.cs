﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;

namespace MandirDarsan.Areas.Admin.Controllers

{
    //[Authorize(Roles = "PackageContributers")]
    [Area("Admin")]
    public class PackagesController : Controller
	{
		
        string pathToCreate = "Images\\Package";
        IHostingEnvironment _hostingEnvironment;
        private IApplicationEnvironment _applicationEnvironment;

        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private IPackage packageRepository;
        public PackagesController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;

            packageRepository = new PackageRepository(_context);
			rBase = new RockmanduBase(_context, _applicationEnvironment, _hostingEnvironment);
        }

        // GET: Admin/Packages
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(packageRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(packageRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
   //         var package = packageRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			//int totalPageSize = rBase.GetPageSize();
   //         var noOfPage = (pageNo ?? 1);
			//return View(package.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/Packages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Packages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
        			public async Task<IActionResult> Create(Package package, IFormFile ImageUrl, IFormCollection formCollections)
		
        {
            if (ModelState.IsValid)
            {
			package.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
			
         
		package.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate)  ;    
		
				
                packageRepository.Add(package, User.Identity.Name);
                packageRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(package);
        }

        public ActionResult Edit(Guid id)
        {
           var package = packageRepository.GetSingle(id);
            if (package == null)
            {
                return HttpNotFound();
            }    
            return View(package);
        }

        // POST: Admin/Packages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
					public async Task<IActionResult> Edit(Package package,IFormFile IImageUrl, IFormCollection formCollections)
		
		
        {
            if (ModelState.IsValid)
            {
			var oldpackage = _context.Packages.AsNoTracking().FirstOrDefault(x=>x.Id == package.Id);
        	package.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
				 
				 if ( package.ImageUrl !=null && IImageUrl != null){    
				package.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);   
			 }
                if (User.IsInRole("Admin") || package.CreatedBy == User.Identity.Name)
                {
                    packageRepository.Edit(package, User.Identity.Name);
                    packageRepository.Commit();
                }
                RockmanduBase.SaveAuditData(package.Id,User.Identity.Name,oldpackage,package);
	                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(package);
        }
        public ActionResult Trash(int? pageNo)
        {
            var package = packageRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(package.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var package = packageRepository.GetSingle(id);
            packageRepository.Undelete(package);
            packageRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Package> package = packageRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in package)
            {
                packageRepository.ConfirmDelete(items.Id);
            }
            packageRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            packageRepository.ConfirmDelete(id);
            packageRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  package =  packageRepository.GetSingle(id);
             packageRepository.Delete( package);
             packageRepository.Commit();
             packageRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(packageRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedpackage = packageRepository.GetSingle(Guid.Parse(id));
            packageRepository.Verify(unverifiedpackage, User.Identity.Name);
            packageRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
