﻿using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Mvc.Abstractions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ImageResizer.Resizing;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Area("Admin")]
	[Authorize]
    public class HomeController : Controller
	{
        string pathToCreate = "Images\\article";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index(string id = null)
        {
            ViewBag.Dashboardsegment = "Profile";
            ViewBag.DashboardSubsegment = "profile";

        ProfileViewModel pvm = new ProfileViewModel();
            pvm.ActivityLog = _context.ActivityLogs.ToList();

            if (id != null)
            {
                if (id.ToLower() == "index")
                {
                    pvm.ApplicationUser = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
                }
                else
                {
                    pvm.ApplicationUser = _context.Users.Where(x => x.UserName == id).SingleOrDefault();
                }
            }
            else
            {
                pvm.ApplicationUser = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
            }
            return View(pvm);
        }
        [Authorize(Roles = "Admin")]
        public IActionResult ProcessMoney()
        {
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create("http://api.fixer.io/latest?base=USD");
            myReq.ContentType = "application/json";
            var response = (HttpWebResponse)myReq.GetResponse();
            string text;
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            return View();
        }

        [HttpGet]
        public IActionResult LanguageSelection()
        {
            //if (id.ToLower() == "languageselection")
            //{
                return View(_context.Globalizations.Include(x => x.LanguageTitle).Include(x => x.Culture).Where(x => x.DelFlg == false)
                .Where(x => x.LanguageTitle.Title.ToLower().Contains("i can read and write in")));
            //}
        }

    }
}
