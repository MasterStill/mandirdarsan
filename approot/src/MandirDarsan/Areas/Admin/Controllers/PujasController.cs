﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
namespace MandirDarsan.Areas.Admin.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class PujasController : Controller
	{
        string pathToCreate = "Images\\Puja";
        IHostingEnvironment _hostingEnvironment;
        private IApplicationEnvironment _applicationEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private IPuja pujaRepository;
        public PujasController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;
            pujaRepository =new PujaRepository(_context);
			rBase = new RockmanduBase(_context, _applicationEnvironment, _hostingEnvironment);
        }
        // GET: Admin/Pujas
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(pujaRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(pujaRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
   //         var puja = pujaRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			
			//return View(puja.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/Pujas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Pujas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
        			public async Task<IActionResult> Create(Puja puja, IFormFile ImageUrl, IFormCollection formCollections)
		
        {
            if (ModelState.IsValid)
            {
			puja.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
			
         
		puja.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate)  ;    
		
				
                pujaRepository.Add(puja, User.Identity.Name);
                pujaRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(puja);
        }

        public ActionResult Edit(Guid id)
        {
           var puja = pujaRepository.GetSingle(id);
            if (puja == null)
            {
                return HttpNotFound();
            }    
            return View(puja);
        }

        // POST: Admin/Pujas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					
		
					public async Task<IActionResult> Edit(Puja puja,IFormFile IImageUrl, IFormCollection formCollections)
		
		
        {
            if (ModelState.IsValid)
            {
			var oldpuja = _context.Pujas.AsNoTracking().FirstOrDefault(x=>x.Id == puja.Id);
        	puja.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
				 
				 if ( puja.ImageUrl !=null && IImageUrl != null){    
				puja.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);   
			 }
                if (User.IsInRole("Admin") || puja.CreatedBy == User.Identity.Name)
                {
                    pujaRepository.Edit(puja, User.Identity.Name);
                    pujaRepository.Commit();
                }
                RockmanduBase.SaveAuditData(puja.Id,User.Identity.Name,oldpuja,puja);
	                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(puja);
        }
        public ActionResult Trash(int? pageNo)
        {
            var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            if (!User.IsInRole("Admin"))
            {
                return View(pujaRepository.GetAll().Where(x => x.DelFlg == true).Where(x => x.CreatedBy == User.Identity.Name).ToPagedList(noOfPage, totalPageSize));
            }
            return View(pujaRepository.GetAll().Where(x => x.DelFlg == true).ToPagedList(noOfPage, totalPageSize));
            //var puja = pujaRepository.GetAllDeleted(User.Identity.Name);
            
            //return View(puja.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var puja = pujaRepository.GetSingle(id);
            pujaRepository.Undelete(puja);
            pujaRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Puja> puja = pujaRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in puja)
            {
                pujaRepository.ConfirmDelete(items.Id);
            }
            pujaRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            pujaRepository.ConfirmDelete(id);
            pujaRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  puja =  pujaRepository.GetSingle(id);
             pujaRepository.Delete( puja);
             pujaRepository.Commit();
             pujaRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(pujaRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var UnverifiedPuja = pujaRepository.GetSingle(Guid.Parse(id));
            pujaRepository.Verify(UnverifiedPuja, User.Identity.Name);
            pujaRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
