﻿using System;
using System.Linq;
using MandirDarsan.Controllers;
using MandirDarsan.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
namespace A11_RBS.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        ApplicationDbContext context;
        public RoleController(ApplicationDbContext db)
        {
            context = db; 
        }
        public ActionResult Index()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "roles";
            var Roles = context.Roles.ToList();
            return View(Roles);
        }
        public ActionResult Create()
        {
            var Role = new IdentityRole();
            return View(Role);
        }
       [HttpPost]
        public ActionResult Create(IdentityRole Role)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "roles";
            context.Roles.Add(Role);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
      public ActionResult SetRoleToUser()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "roles";
            var list = context.Roles.OrderBy(role => role.Name).ToList().Select(role => new SelectListItem { Value = role.Name.ToString(), Text = role.Name }).ToList();
            var Users = context.Users.ToList().Select(role => new SelectListItem { Value = role.UserName, Text = role.UserName }).ToList();
            ViewBag.Roles = list;
            ViewBag.Users = Users;
            return View();
        }
    }
}