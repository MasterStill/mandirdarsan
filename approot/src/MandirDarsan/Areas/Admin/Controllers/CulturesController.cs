﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CulturesController : Controller
	{
		
        string pathToCreate = "Images\\Culture";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        private ICulture cultureRepository;
        public CulturesController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            cultureRepository =new CultureRepository(_context);
        }

        // GET: Admin/Cultures
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "cultures";

            var culture = cultureRepository.GetAll().Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
			return View(culture.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/Cultures/Create
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "cultures";
            return View();
        }

        // POST: Admin/Cultures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create(Culture culture)
		
        {
            if (ModelState.IsValid)
            {
				culture.CreatedBy =  User.Identity.Name;
                culture.ModifiedBy = User.Identity.Name;
        			    
			
				
                cultureRepository.Add(culture, User.Identity.Name);
                cultureRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(culture);
        }

        public ActionResult Edit(Guid id)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "cultures";
            var culture = cultureRepository.GetSingle(id);
            if (culture == null)
            {
                return HttpNotFound();
            }    
            return View(culture);
        }

        // POST: Admin/Cultures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit(Culture culture)
		
		
        {
            if (ModelState.IsValid)
            {
                if (User.IsInRole("Admin") || culture.CreatedBy == User.Identity.Name)
                {
                    culture.ModifiedBy = User.Identity.Name;
                    cultureRepository.Edit(culture,User.Identity.Name);
                    cultureRepository.Commit();
                    _context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View(culture);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "cultures";
            var culture = cultureRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(culture.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var culture = cultureRepository.GetSingle(id);
            cultureRepository.Undelete(culture);
            cultureRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Culture> culture = cultureRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in culture)
            {
                cultureRepository.ConfirmDelete(items.Id);
            }
            cultureRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            cultureRepository.ConfirmDelete(id);
            cultureRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  culture =  cultureRepository.GetSingle(id);
             cultureRepository.Delete( culture);
             cultureRepository.Commit();
             cultureRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(cultureRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedGod = cultureRepository.GetSingle(Guid.Parse(id));
            cultureRepository.Verify(unverifiedGod, User.Identity.Name);
            cultureRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
