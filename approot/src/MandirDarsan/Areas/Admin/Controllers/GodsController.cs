﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;

namespace MandirDarsan.Areas.Admin.Controllers

{
	[Authorize]
	[Area("Admin")]
    public class GodsController : Controller
	{
		
        string pathToCreate = "Images\\God";
        private IApplicationEnvironment _applicationEnvironment;
        private IHostingEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private IGod godRepository;
        public GodsController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;

            godRepository = new GodRepository(_context);
			rBase = new RockmanduBase(_context,_applicationEnvironment,_hostingEnvironment);
        }

        // GET: Admin/Gods
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            ViewBag.Segment = "Gods";
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(godRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(godRepository.GetAll().Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
   //         var god = godRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
			//int totalPageSize = rBase.GetPageSize();
   //         var noOfPage = (pageNo ?? 1);
			//return View(god.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/Gods/Create
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            return View();
        }

        // POST: Admin/Gods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(God god, IFormFile ImageUrl, IFormCollection formCollections)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            if (ModelState.IsValid)
            {
    			god.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
                god.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate)  ;    
                godRepository.Add(god, User.Identity.Name);
                godRepository.Commit();

                return Redirect("~/Admin/Galleries/Create/Gods/" + god.Id);
           }
            return RedirectToAction("Index");
            //return View(god);
        }
        public ActionResult Edit(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            var god = godRepository.GetSingle(id);
            if (god == null)
            {
                return HttpNotFound();
            }    
            return View(god);
        }
        // POST: Admin/Gods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(God god,IFormFile IImageUrl, IFormCollection formCollections)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            if (ModelState.IsValid)
            {
			var oldgod = _context.Gods.AsNoTracking().FirstOrDefault(x=>x.Id == god.Id);
        	god.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
				 
				 if ( god.ImageUrl !=null && IImageUrl != null){    
				god.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);   
			 }
                if (User.IsInRole("Admin") || god.CreatedBy == User.Identity.Name)
                {
                    godRepository.Edit(god, User.Identity.Name);
                    godRepository.Commit();
                }
                RockmanduBase.SaveAuditData(god.Id,User.Identity.Name,oldgod,god);
	                //_context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(god);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedgods";
            var god = godRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(god.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var god = godRepository.GetSingle(id);
            godRepository.Undelete(god);
            godRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<God> god = godRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in god)
            {
                godRepository.ConfirmDelete(items.Id);
            }
            godRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            godRepository.ConfirmDelete(id);
            godRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  god =  godRepository.GetSingle(id);
             godRepository.Delete( god);
             godRepository.Commit();
             godRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(godRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedGod = godRepository.GetSingle(Guid.Parse(id));
            godRepository.Verify(unverifiedGod, User.Identity.Name);
            godRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
