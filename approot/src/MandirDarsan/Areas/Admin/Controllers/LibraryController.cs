﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Server.Kestrel.Networking;
using Microsoft.Data.Entity;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using PagedList;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class LibraryController : Controller
	{
        string pathToCreate = "Images\\Library";
        private IApplicationEnvironment _applicationEnvironment;
        private IHostingEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;

        private static RockmanduBase rBase; //= new RockmanduBase();
        private ILibrary libraryRepository;
        public LibraryController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;

            libraryRepository = new LibraryRepository(_context);
            rBase = new RockmanduBase(_context,_applicationEnvironment,_hostingEnvironment);
        }
        public ActionResult Index(int? pageNo, string id="News",string searchTerm="")
        {
            ViewBag.Segment = id;
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributed" + id.ToLower();
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(libraryRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.Segment == id).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
            return View(libraryRepository.GetAll().Where(x => x.Title.Contains(searchTerm.ToLower())).Where(x => x.Segment == id).ToPagedList(noOfPage, totalPageSize));

         
            //var library = libraryRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name).Where(x=>x.Segment == id).Where(x=>x.Title.ToLower().Contains(searchTerm.ToLower()));
            //int totalPageSize = rBase.GetPageSize();
            //var noOfPage = (pageNo ?? 1);
            //ViewBag.Segment = RockmanduBase.FirstCharToUpper(id);
            //return View(library.ToPagedList(noOfPage, totalPageSize));
        }
        public ActionResult Create(string id)
	    {
            ViewBag.Dashboardsegment = "contributed";
            ViewBag.DashboardSubSegment = "contributed" + id.ToLower();
            MandirDarsan.Areas.Admin.Models.Library lib = new MandirDarsan.Areas.Admin.Models.Library();
            lib.Segment = id;
            lib.PublishDate = DateTime.Now;
            return View(lib);
	    }
        public ActionResult Details()
        {
            //IEnumerable<MandirDarsan.Areas.Admin.Models.Library> lib = libraryRepository.GetAll();
            //return View(lib);
            return View();
        }
        public JsonResult GetLib()
        {
            //Bhajan bj = new Bhajan();
            return Json(_context.Tags.ToList().Select(x=> new {x.Title,x.Id} ));
        }
        public JsonResult GetCulture()
        {
            return Json(_context.Cultures.Select(x => new { x.Title, x.Code }).ToList());
        }
        public JsonResult GetTrash()
        {
            return Json(libraryRepository.GetAll().ToList().Where(x => x.DelFlg == true).Select(x => new { x.Id, x.Segment, x.Title }));
        }
        public JsonResult Audit(Guid id)
        {
            List<AuditChange> ac = new List<AuditChange>();
            ac = rBase.GetAudit(id);
            return Json(ac);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MandirDarsan.Areas.Admin.Models.Library library, IFormFile ImageUrl,IFormCollection formCollections)
        {
            if (library.Title == null)
            {
                library.Title = "I Don't Know Please Translate";
            }
            ViewBag.Dashboardsegment = "contributed";
            if (ModelState.IsValid)
            {
                library.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
                library.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate);
                libraryRepository.Add(library,User.Identity.Name);
                libraryRepository.Commit();
                return Redirect("~/Admin/Galleries/Create/Library/" + library.Id);
                //return RedirectToAction("Index", "Library", new { id = library.Segment });
            }
            return View(library);
        }
        public ActionResult Edit(Guid id)
        {
            ViewBag.Dashboardsegment = "contributed";
            var library = libraryRepository.GetSingle(id);
            if (library == null)
            {
                return HttpNotFound();
            }    
            return View(library);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MandirDarsan.Areas.Admin.Models.Library library, IFormFile IImageUrl, IFormCollection formCollections)
        {
            library.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
            if (library.ImageUrl != null && IImageUrl != null)
            {

                library.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);

            }
            if (library.ImageUrl == null && IImageUrl != null)
            {
                library.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);
            }
                ViewBag.Dashboardsegment = "contributed";
            if (ModelState.IsValid)
            {
                var oldLibrary = _context.Articles.AsNoTracking().FirstOrDefault(x => x.Id == library.Id);
                if (User.IsInRole("Admin") || library.CreatedBy == User.Identity.Name)
                {
                    libraryRepository.Edit(library, User.Identity.Name);
                    libraryRepository.Commit();
                }
                RockmanduBase.SaveAuditData(library.Id, User.Identity.Name,oldLibrary, library);

                return RedirectToAction("Index", "Library", new { id = library.Segment });
            }
            return View(library);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id,string segment)
        {
            ViewBag.Dashboardsegment = "contributed";
            ViewBag.DashboardSubSegment = "contributed" + segment.ToLower();
            var library = libraryRepository.GetSingle(id);
            libraryRepository.Delete(library);
            libraryRepository.Commit();
            libraryRepository.Commit();
            return RedirectToAction("Index", "Library", new { id = segment });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Trash(string id,int? pageNo)
        {
            ViewBag.Segment = RockmanduBase.FirstCharToUpper(id);

            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(libraryRepository.GetAll().Where(x => x.DelFlg == true).Where(x => x.Segment.ToLower() == id.ToLower()).ToPagedList(noOfPage, totalPageSize));
            }
            return View(libraryRepository.GetAllDeleted(User.Identity.Name).Where(x => x.Segment.ToLower() == id.ToLower()).ToPagedList(noOfPage, totalPageSize));

            //var library = libraryRepository.GetAllDeleted(User.Identity.Name).Where(x=>x.Segment.ToLower()== id.ToLower());
            //var noOfPage = (pageNo ?? 1);
            //int totalPageSize = rBase.GetPageSize();
            //return View(library.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id,string segment)
        {
            var library = libraryRepository.GetSingle(id);
            libraryRepository.Undelete(library);
            libraryRepository.Commit();
            return RedirectToAction("Trash", "Library", new { id = segment });
           // return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash(string segment)
        {
            IEnumerable<MandirDarsan.Areas.Admin.Models.Library> library = libraryRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in library)
            {
                libraryRepository.ConfirmDelete(items.Id);
            }
            libraryRepository.Commit();
            return RedirectToAction("Trash", "Library", new { id = segment });
        }
        public ActionResult DeleteTrashItem(Guid id,string segment)
        {
            libraryRepository.ConfirmDelete(id);
            libraryRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
            return RedirectToAction("Trash", "Library", new { id = segment });
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(libraryRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {

            var unverifiedLibrary = libraryRepository.GetSingle(Guid.Parse(id));
            libraryRepository.Verify(unverifiedLibrary, User.Identity.Name);
            libraryRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}