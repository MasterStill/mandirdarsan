﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Migrations;
using MandirDarsan.Models;
using Microsoft.AspNet.Hosting;
namespace MandirDarsan.Areas.Admin.Controllers
{
	[Authorize]
	[Area("Admin")]
    public class TemplesController : Controller
	{
        string pathToCreate = "Images\\Temple";
        IHostingEnvironment _hostingEnvironment;
        private IApplicationEnvironment _applicationEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase;
        private ITemple templeRepository;
        public TemplesController(ApplicationDbContext context, IApplicationEnvironment applicationEnvironment, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _applicationEnvironment = applicationEnvironment;
            templeRepository =new TempleRepository(_context);
			rBase = new RockmanduBase(_context, _applicationEnvironment, _hostingEnvironment);
        }

        // GET: Admin/Temples
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            ViewBag.Segment = "Temples";
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(templeRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage, totalPageSize));
            }
			return View(templeRepository.GetAll().Where(x => x.Title.ToLower().Contains(searchTerm.ToLower())).ToPagedList(noOfPage ,totalPageSize ));
        }
        // GET: Admin/Temples/Create
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            return View();
        }
        // POST: Admin/Temples/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(Temple temple, IFormFile ImageUrl, IFormCollection formCollections)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            if (ModelState.IsValid)
            {
			    temple.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
    		    temple.ImageUrl = RockmanduBase.SaveAndGetImage(ImageUrl, pathToCreate)  ;    
	            templeRepository.Add(temple, User.Identity.Name);
                templeRepository.Commit();
                return Redirect("~/Admin/Galleries/Create/Temples/" + temple.Id);
                //return RedirectToAction("Index");
            }
            return View(temple);
        }
        public ActionResult Edit(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            var temple = templeRepository.GetSingle(id);
            if (temple == null)
            {
                return HttpNotFound();
            }    
            return View(temple);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(Temple temple,IFormFile IImageUrl, IFormCollection formCollections)
		{
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            if (ModelState.IsValid)
            {
			var oldtemple = _context.Temples.AsNoTracking().FirstOrDefault(x=>x.Id == temple.Id);
        	temple.MultiLingual = RockmanduBase.GetJsonFromFormCollection(formCollections);
                if (temple.ImageUrl != null && IImageUrl != null)
                {

                    temple.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);

                }
                if (temple.ImageUrl == null && IImageUrl != null)
                {
                    temple.ImageUrl = RockmanduBase.SaveAndGetImage(IImageUrl, pathToCreate);
                }
                if (User.IsInRole("Admin") || temple.CreatedBy == User.Identity.Name)
                {
                    templeRepository.Edit(temple, User.Identity.Name);
                    templeRepository.Commit();
                }
				RockmanduBase.SaveAuditData(temple.Id,User.Identity.Name,oldtemple,temple);
                return RedirectToAction("Index");
            }
            return View(temple);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            var temple = templeRepository.GetAllDeleted(User.Identity.Name);
            var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(temple.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            var temple = templeRepository.GetSingle(id);
            templeRepository.Undelete(temple);
            templeRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            IEnumerable<Temple> temple = templeRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in temple)
            {
                templeRepository.ConfirmDelete(items.Id);
            }
            templeRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            templeRepository.ConfirmDelete(id);
            templeRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ViewBag.DashboardSegment = "contributed";
            ViewBag.DashboardSubSegment = "contributedtemples";
            var  temple =  templeRepository.GetSingle(id);
             templeRepository.Delete( temple);
             templeRepository.Commit();
             templeRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult UnVerified(int? pageNo)
        {
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            return View(templeRepository.GetAllUnVerified().ToPagedList(noOfPage, totalPageSize));
        }
        [Authorize(Roles = ("Admin"))]
        [HttpPost]
        public ActionResult Verify(string id)
        {
            var UnverifiedTemple = templeRepository.GetSingle(Guid.Parse(id));
            templeRepository.Verify(UnverifiedTemple, User.Identity.Name);
            templeRepository.Commit();
            return RedirectToAction("Unverified");
        }
    }
}
