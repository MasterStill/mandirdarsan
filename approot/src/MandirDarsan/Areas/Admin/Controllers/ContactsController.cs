﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ContactsController : Controller
	{
		
        string pathToCreate = "Images\\Culture";
        //IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        private ICulture cultureRepository;
        public ContactsController(ApplicationDbContext context)//, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            //_hostingEnvironment = hostingEnvironment;
            //cultureRepository =new CultureRepository(_context);
        }

        // GET: Admin/Cultures
        public ActionResult Index(int? pageNo, string searchTerm="")
        {
            var contacts = _context.Contacts.ToList();
			int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
			return View(contacts.ToPagedList(noOfPage ,totalPageSize ));
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
