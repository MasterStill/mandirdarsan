﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.Areas.Admin.Controllers
{
	//[Authorize]
	[Area("Admin")]
    public class LanguageController : Controller
	{
		
        string pathToCreate = "Images\\Bhajan";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        public LanguageController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
	    public IActionResult Index()
	    {
	        return View();
	    }

	    public string Languages(string lang = "nepali")
	    {
	        lang = lang.ToLower();

	        var jsonObject =
            _context.Globalizations.Where(x => x.Culture.Title.ToLower() == lang).Where(x=>x.DelFlg == false).Select(x => new {x.LanguageTitle.Title, x.Value}).ToList();
	        string json = "{ objects }";
	        string jsval = string.Empty;
	        foreach (var item in jsonObject)
	        {
	            jsval += "\"" + item.Title.Replace(Environment.NewLine,"").Trim() + "\"" + ":" + "\"" +  item.Value.Replace(Environment.NewLine, "").Trim() + "\",";
	        }
            if (jsval.Count() > 0) { 
            jsval = jsval.Substring(0, jsval.Length - 1);
            }
            HttpContext.Session.SetString("Language", lang);
            return (json.Replace("objects",jsval));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
