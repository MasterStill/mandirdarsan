﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;

namespace MandirDarsan.Areas.Admin.Controllers

{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class LanguageTitlesController : Controller
	{
		
        string pathToCreate = "Images\\LanguageTitle";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        private ILanguageTitle languageTitleRepository;
        public LanguageTitlesController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            languageTitleRepository =new LanguageTitleRepository(_context);
        }

        // GET: Admin/LanguageTitles
        public ActionResult Index(int? pageNo, string id="")
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "languagetitles";


            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
            if (!User.IsInRole("Admin"))
            {
                return View(languageTitleRepository.GetAll().Where(x => x.DelFlg == false).Where(x => x.CreatedBy == User.Identity.Name).ToPagedList(noOfPage, totalPageSize));
            }
            return View(languageTitleRepository.GetAll().ToPagedList(noOfPage, totalPageSize));




            //var languageTitles = _context.LanguageTitles.Include(l => l.SiteSections);
            //var neededThings = languageTitleRepository.AllIncluding(x => x.SiteSections);
   //         var languageTitle = languageTitleRepository.GetAll().Where(x=>x.CreatedBy == User.Identity.Name);
			//int totalPageSize = rBase.GetPageSize();
   //         var noOfPage = (pageNo ?? 1);
			//return View(languageTitle.ToPagedList(noOfPage ,totalPageSize ));
        }
    
        // GET: Admin/LanguageTitles/Create
        public ActionResult Create()
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "languagetitles";

            ViewBag.SiteSectionsId = new SelectList(_context.SiteSections.Where(x=>x.DelFlg==false), "Id", "Title");
            return View();
        }

        // POST: Admin/LanguageTitles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
public ActionResult Create(LanguageTitle languageTitle)
        {
            if (ModelState.IsValid)
            {
				languageTitle.CreatedBy =  User.Identity.Name;
                languageTitle.ModifiedBy = User.Identity.Name;
        			    
			
				
                languageTitleRepository.Add(languageTitle, User.Identity.Name);
                languageTitleRepository.Commit();
                return RedirectToAction("Index");
            }

            ViewBag.SiteSectionsId = new SelectList(_context.SiteSections, "Id", "Title", languageTitle.SiteSectionsId);
            return View(languageTitle);
        }
        public ActionResult Edit(Guid id)
        {





            ViewBag.SiteSectionsId = new SelectList(_context.SiteSections, "Id", "Title");
            var languageTitle = languageTitleRepository.GetSingle(id);
            if (languageTitle == null)
            {
                return HttpNotFound();
            }    
            return View(languageTitle);
        }
        // POST: Admin/LanguageTitles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					public ActionResult Edit(LanguageTitle languageTitle)
        {
            if (ModelState.IsValid)
            {
                var oldlanguageTitle = _context.LanguageTitles.AsNoTracking().FirstOrDefault(x => x.Id == languageTitle.Id);
                languageTitle.ModifiedBy = User.Identity.Name;
                if (User.IsInRole("Admin") || languageTitle.CreatedBy == User.Identity.Name)
                {
                    languageTitleRepository.Edit(languageTitle, User.Identity.Name);
                    languageTitleRepository.Commit();
                }
                //RockmanduBase.SaveAuditData(languageTitle.Id, User.Identity.Name, oldlanguageTitle, languageTitle);
                return RedirectToAction("Index");
            }
            ViewBag.SiteSectionsId = new SelectList(_context.SiteSections, "Id", "Title", languageTitle.SiteSectionsId);
            return View(languageTitle);
        }
        public ActionResult Trash(int? pageNo)
        {
            ViewBag.DashboardSegment = "admin";
            ViewBag.DashboardSubSegment = "languagetitles";

            var languageTitle = languageTitleRepository.GetAllDeleted(User.Identity.Name);
             var noOfPage = (pageNo ?? 1);
            int totalPageSize = rBase.GetPageSize();
            return View(languageTitle.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var languageTitle = languageTitleRepository.GetSingle(id);
            languageTitleRepository.Undelete(languageTitle);
            languageTitleRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<LanguageTitle> languageTitle = languageTitleRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in languageTitle)
            {
                languageTitleRepository.ConfirmDelete(items.Id);
            }
            languageTitleRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            languageTitleRepository.ConfirmDelete(id);
            languageTitleRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  languageTitle =  languageTitleRepository.GetSingle(id);
             languageTitleRepository.Delete( languageTitle);
             languageTitleRepository.Commit();
             languageTitleRepository.Commit();
            return RedirectToAction("Index");
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
