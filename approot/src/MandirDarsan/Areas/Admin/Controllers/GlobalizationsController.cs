﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.Areas.Admin.Controllers
{
	[Authorize]
	[Area("Admin")]
    public class GlobalizationsController : Controller
	{
        string pathToCreate = "Images\\Globalization";
        IApplicationEnvironment _hostingEnvironment;
        private static ApplicationDbContext _context;
        private static ApplicationDbContext _languageTitles;
        private RockmanduBase rBase = new RockmanduBase();
        private IGlobalization globalizationRepository;
        public GlobalizationsController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            _context = context;
            _languageTitles = context;
            _hostingEnvironment = hostingEnvironment;
            globalizationRepository =new GlobalizationRepository(_context);
        }
        public ActionResult Index(int? pageNo, string id, string searchTerm="")
        {
            try
            {
                ViewBag.DashboardSubSegment = "globalization_" + id.ToLower();
            }
                catch
            (Exception)
            {
            }
            ViewBag.DashboardSegment = "contributed";
            var globalizations = _context.Globalizations.Include(g => g.Culture).Include(g => g.LanguageTitle);
            if (!User.IsInRole("Admin"))
            {
                globalizations.Where(x => x.Culture.Title == id)
                    .Where(x => x.CreatedBy == User.Identity.Name)
                    .Where(x => x.DelFlg == false)
                    .Where(x => x.LanguageTitle.ToString() == searchTerm.ToString().ToLower());
            }
            //var globalization = globalizationRepository.GetAll();
            ViewBag.Segment = id;
            int totalPageSize = rBase.GetPageSize();
            var noOfPage = (pageNo ?? 1);
			return View(globalizations.ToPagedList(noOfPage ,totalPageSize ));
        }
        public ActionResult Create(string id)
        {
            ViewBag.Segment = id;
            try
            {
                ViewBag.DashboardSubSegment = "globalization_" + id.ToLower();
            }
            catch (Exception)
            {
                
            }
            ViewBag.DashboardSegment = "contributed";
            ViewBag.CultureId = new SelectList(_context.Cultures.Where(x => x.Title.ToString() == id).ToList().ToList(), "Id", "Title");
            List<Globalization> globa = _context.Globalizations.Where(x=>x.Culture.Title.ToString() == id).ToList();
            IEnumerable<LanguageTitle> languageTitles;//= new List<LanguageTitle>();
            languageTitles = _languageTitles.LanguageTitles.ToList();
            foreach (var items in globa)
            {
                languageTitles = languageTitles.Where(x => x.Id.ToString() != items.LanguageTitleId.ToString());
            }
            ViewBag.LanguageTitleId = new SelectList(languageTitles, "Id", "Title");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Globalization globalization)
        {
            if (ModelState.IsValid)
            {
				globalization.CreatedBy =  User.Identity.Name;
                globalization.ModifiedBy = User.Identity.Name;
        			    
			
				
                globalizationRepository.Add(globalization, User.Identity.Name);
                globalizationRepository.Commit();
                return RedirectToAction("Index"); //,new {id = globalization.Culture.Title});

            }

            ViewBag.CultureId = new SelectList(_context.Cultures, "Id", "Title", globalization.CultureId);
            ViewBag.LanguageTitleId = new SelectList(_context.LanguageTitles, "Id", "Title", globalization.LanguageTitleId);
            return View(globalization);
        }
        public ActionResult Edit(Guid id)
        {
            var globalization = globalizationRepository.AllIncluding(x=>x.LanguageTitle,x=>x.Culture).Where(x=>x.Id == id).SingleOrDefault();

            ViewBag.CultureId = new SelectList(_context.Cultures, "Id", "Title", globalization.CultureId);
            ViewBag.LanguageTitleId = new SelectList(_context.LanguageTitles, "Id", "Title", globalization.LanguageTitleId);
            ViewBag.Segment = globalization.Culture.Title;
            ViewBag.DashboardSubSegment = "globalization_" + globalization.Culture.Title.ToLower();
            ViewBag.DashboardSegment = "contributed";
            if (globalization == null)
            {
                return HttpNotFound();
            }    
            return View(globalization);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Globalization globalization)
         {
            if (ModelState.IsValid)
            {
                var oldglobalization = _context.Globalizations.AsNoTracking().FirstOrDefault(x => x.Id == globalization.Id);
                globalization.ModifiedBy = User.Identity.Name;
                if (User.IsInRole("Admin") || globalization.CreatedBy == User.Identity.Name)
                {
                    globalizationRepository.Edit(globalization, User.Identity.Name);
                    globalizationRepository.Commit();
                }
                _context.SaveChanges();
                return RedirectToAction("Index", "Globalization", new { id = globalization.Culture.Title});
            }
            ViewBag.CultureId = new SelectList(_context.Cultures, "Id", "Title", globalization.CultureId);
            ViewBag.LanguageTitleId = new SelectList(_context.LanguageTitles, "Id", "Title", globalization.LanguageTitleId);
            return View(globalization);
        }
        public ActionResult Trash(int? pageNo)
        {
            var globalizations =
                _context.Globalizations.Include(g => g.Culture)
                    .Include(g => g.LanguageTitle)
                    .Where(x => x.CreatedBy == User.Identity.Name);
                 var noOfPage = (pageNo ?? 1);

            int totalPageSize = rBase.GetPageSize();
            return View(globalizations.ToPagedList(noOfPage, totalPageSize));
        }
        [HttpPost, ActionName("RecoverTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult RecoverFromTrash(Guid id)
        {
            var globalization = globalizationRepository.GetSingle(id);
            globalizationRepository.Undelete(globalization);
            globalizationRepository.Commit();
           return RedirectToAction("Trash");
        }
        [HttpPost, ActionName("DeleteTrash")]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyTrash()
        {
            IEnumerable<Globalization> globalization = globalizationRepository.GetAllDeleted(User.Identity.Name);
            foreach (var items in globalization)
            {
                globalizationRepository.ConfirmDelete(items.Id);
            }
            globalizationRepository.Commit();
           return RedirectToAction("Trash");
        }
        public ActionResult DeleteTrashItem(Guid id)
        {
            globalizationRepository.ConfirmDelete(id);
            globalizationRepository.Commit();
            ViewBag.Message = "Item Permanently Deleted";
           return RedirectToAction("Trash");
        }
		  [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var  globalization =  globalizationRepository.GetSingle(id);
             globalizationRepository.Delete(globalization);
             globalizationRepository.Commit();
		      return RedirectToAction("Index"); 
        }
       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
