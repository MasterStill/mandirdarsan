﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
//using EFAuditing;
using MandirDarsan.Areas.Admin.Models;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.ChangeTracking;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PagedList;

namespace MandirDarsan.Models
{
    public interface IRockmanduBase 
    {
        Guid Id { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
        string VerifiedBy { get; set; }

        DateTime? CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        DateTime? VerifiedDate { get; set; }

        Boolean? Verified { get; set; }
        Boolean? DelFlg { get; set; }
        Boolean? Active { get; set; }
    }
    public interface IRockmanduBaseRepository<T> where T : class, IRockmanduBase, new()
    {
        IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> AllIncludingAsync(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetAll();

        IEnumerable<T> GetAllUnVerified();
        
        IEnumerable<T> GetAllDeleted(string user);
        IEnumerable<T> FindFor(string sortingOrder, string searchData, string filterValue, int? pageNo);
        Task<IEnumerable<T>> GetAllAsync();
        T GetSingle(Guid id);
        T GetSingle(Expression<Func<T, bool>> predicate);
        T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<T> GetSingleAsync(Guid id);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate);
        void Add(T entity, string userid);
        void Delete(T entity);
        void Verify(T entity, string userid);
        void Edit(T entity,string userid);
        void Undelete(T entity);
        void Commit();
        void ConfirmDelete(Guid id);
        void Audit(RockAuditLog audit);

    }
    public class RockmanduBaseRepository<T> : IRockmanduBaseRepository<T>
            where T : class, IRockmanduBase, new()
    {
        private ApplicationDbContext _context = new ApplicationDbContext();
        #region Properties
        public RockmanduBaseRepository(ApplicationDbContext context)
        //public RockmanduBaseRepository()
        {
            //_context = new ApplicationDbContext();
            _context = context;
        }
        #endregion
        public virtual IEnumerable<T> GetAll()
        {
            var kk = _context.Set<T>().Where(x=>x.DelFlg == false).AsEnumerable();
            return kk;
        }
        public virtual IEnumerable<T> GetAllDeleted(string userid)
        {
            var kk = _context.Set<T>().Where(x => x.DelFlg == true || x.DelFlg == null && x.CreatedBy == userid).AsEnumerable();
            return kk;
        }
        public virtual IEnumerable<T> GetAllUnVerified()
        {
            var kk = _context.Set<T>().Where(x => x.DelFlg == false && x.Verified == false).AsEnumerable();
            return kk;
        }
        public virtual IEnumerable<T> FindFor(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            //foreach (var item in T)
            //{
            //    item.
            //}
            //T
            var kk = _context.Set<T>().AsEnumerable();
            
            return kk;
        }
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }
        public virtual IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.AsEnumerable();
        }
        public virtual async Task<IEnumerable<T>> AllIncludingAsync(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.ToListAsync();
        }
        public T GetSingle(Guid id)
        {
            return _context.Set<T>().FirstOrDefault(x => x.Id == id);
        }
        public T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().FirstOrDefault(predicate);
        }
        public T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.Where(predicate).FirstOrDefault();
        }
        public async Task<T> GetSingleAsync(Guid id)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
        }
        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public virtual async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>().Where(predicate).ToListAsync();
        }

        public virtual void Add(T entity,string userid )
        {
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            entity.CreatedBy = userid;
                entity.CreatedDate = DateTime.Now;
                entity.DelFlg = false;
                entity.Verified= false;
                entity.Active = false;
            entity.ModifiedDate = DateTime.Now;
           
            if (entity.ModifiedBy == null) { 
                entity.ModifiedBy = RockmanduBase.GetUserId();
            }
            _context.Set<T>().Add(entity);
          //  Commit();
        }
        public virtual void Audit(RockAuditLog audit)
        {
            if (audit.CreatedBy == null)
            {
                audit.CreatedBy = RockmanduBase.GetUserId();
            }
            if (audit.CreatedDate.ToString() == "")
            {
                audit.CreatedDate = DateTime.Now;
            }
            if (audit.ModifiedDate.ToString() == "")
            {
                audit.ModifiedDate = DateTime.Now;
            }
            if (audit.ModifiedBy == null)
            {
                audit.ModifiedBy = RockmanduBase.GetUserId();
            }
            _context.Auditlogs.Add(audit);
            //  Commit();
        }
        public virtual void Edit(T entity,string userid)
        {
            entity.ModifiedDate = DateTime.Now;
            entity.ModifiedBy = userid;
            entity.Verified = false;
            entity.Active= false;
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
        public virtual void Verify(T entity, string userid)
        {
            entity.VerifiedDate = DateTime.Now;
            entity.VerifiedBy = userid;
            entity.Verified = true;
            entity.Active  = true;
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
        public virtual void Undelete(T entity)
        {
            entity.ModifiedDate = DateTime.Now;
            entity.ModifiedBy = RockmanduBase.GetUserId();
            entity.DelFlg = false;
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
            // _context.SaveChanges();
        }
        public virtual void Delete(T entity)
        {
            entity.DelFlg = true;
            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            //dbEntityEntry.State = EntityState.Deleted;
            dbEntityEntry.State = EntityState.Modified;
          //  Commit();
        }
        public virtual void ConfirmDelete(Guid id)
        {
            var entity = _context.Set<T>().FirstOrDefault(x => x.Id == id);
            //entity.DelFlg = true;

            EntityEntry dbEntityEntry = _context.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
            //dbEntityEntry.State = EntityState.Modified;
           // Commit();
        }
        public virtual void Commit()
        {
            _context.SaveChanges();
        }
    }

    public interface IGod : IRockmanduBaseRepository<God>
    {
        string LastTestMero();
    }
    //public interface IContact : IRockmanduBaseRepository<Contact> { }
    public interface ITemple : IRockmanduBaseRepository<Temple> { }
    public interface IGallery : IRockmanduBaseRepository<Gallery> { }
    public interface ICategories : IRockmanduBaseRepository<Categories> { }
    public interface ILibrary: IRockmanduBaseRepository<Library> {}
    public interface ICountry : IRockmanduBaseRepository<Country> { }
    public interface IPuja : IRockmanduBaseRepository<Puja> { }
    public interface IPackage : IRockmanduBaseRepository<Package> { }
    public interface IEnquiry : IRockmanduBaseRepository<Enquiry> { }
    public interface ISiteset : IRockmanduBaseRepository<Siteset> { }
    public interface IProduct : IRockmanduBaseRepository<Product> { }
    public interface ICaterogy : IRockmanduBaseRepository<Categories> { }
    public interface IBook : IRockmanduBaseRepository<Book> { }
    public interface IBhajan : IRockmanduBaseRepository<Bhajan> { }
    public interface IAudit : IRockmanduBaseRepository<RockAuditLog> { }
    public interface IGlobalization : IRockmanduBaseRepository<Globalization> { }
    public interface ILanguageTitle : IRockmanduBaseRepository<LanguageTitle> { }
    public interface ISiteSection : IRockmanduBaseRepository<SiteSection> { }
    public interface ICulture : IRockmanduBaseRepository<Culture>
    {
        
    }
    public class PujaRepository : RockmanduBaseRepository<Puja>, IPuja
    {
        public PujaRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class PackageRepository : RockmanduBaseRepository<Package>, IPackage
    {
        public PackageRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class CultureRepository : RockmanduBaseRepository<Culture>, ICulture
    {
        public CultureRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class CategoryRepository : RockmanduBaseRepository<Categories>, ICategories
    {
        public CategoryRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class SiteSectionRepository : RockmanduBaseRepository<SiteSection>, ISiteSection
    {
        public SiteSectionRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class LanguageTitleRepository : RockmanduBaseRepository<LanguageTitle>, ILanguageTitle
    {
        public LanguageTitleRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class GlobalizationRepository : RockmanduBaseRepository<Globalization>, IGlobalization
    {
        public GlobalizationRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class AuditRepository : RockmanduBaseRepository<RockAuditLog>, IAudit
    {
        public AuditRepository(ApplicationDbContext context)
        //public ArticleRepository()
            : base(context)
        { }
    }
    public class GalleryRepository : RockmanduBaseRepository<Gallery>, IGallery
    {
        public GalleryRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class LibraryRepository : RockmanduBaseRepository<Library>, ILibrary
    {
        public LibraryRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class CategoriesRepository : RockmanduBaseRepository<Categories>, ICaterogy
    {
        public CategoriesRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class ProductRepository : RockmanduBaseRepository<Product>, IProduct
    {
        public ProductRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class CountryRepository : RockmanduBaseRepository<Country>, ICountry
    {
        public CountryRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class EnquiryRepository : RockmanduBaseRepository<Enquiry>, IEnquiry
    {
        public EnquiryRepository(ApplicationDbContext context)
            : base(context) 
        { }
    }
    public class SitesetRepository : RockmanduBaseRepository<Siteset>, ISiteset
    {
        public SitesetRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class BhajanRepository : RockmanduBaseRepository<Bhajan>, IBhajan
    {
        public BhajanRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class GodRepository : RockmanduBaseRepository<God>, IGod
    {
        public GodRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public virtual string LastTestMero()
        {
            return "Awesome Bhayo ni !";
        }
    }
    public class TempleRepository : RockmanduBaseRepository<Temple>, ITemple
    {
        public TempleRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public class BookRepository : RockmanduBaseRepository<Book>, IBook
    {
        public BookRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
    public interface IMandirRepository
    {

        List<Gallery> Gallery(int num,string sitePortion,string guid);
        List<Country> Countries();
        List<Temple> Temples(string culture, string category, int intNumber = 5,Boolean random = false);
        Temple SingleTemple(string culture, Guid guid);
        List<Package> Packages(string culture,int num);
        Package SinglePackage(string culture,Guid num);
        List<Puja> Puja(string culture, int num,string other);
        Puja SinglePuja(string culture,Guid num);
        List<Library> Library(string culture, string segment, int num);
        Library SingleLibrary(string culture, Guid num);
        List<God> Gods(string culture, int num);
        God SingleGod(string culture, Guid id);
        List<Categories> Categories(string culture);
        List<Mantra> Mantra(string culture,int pagesize);
        List<Bhajan> Bhajan(string culture, int pagesize);
        List<Product> Products(string culture, string category, int intNumber = 5);
        Product SingleProducts(Guid guid, string culture);
        SearchViewModeltest Search(string searchData,string culture);
    }
    public class MandirRepository : IMandirRepository
    {
        
        private static ITemple _templeReposotory;
        private static IGod _godReposotory;
        private static IProduct _productReposotory;
        private static IPuja _pujaReposotory;
        private static IPackage _packageReposotory;
        private static ILibrary _libraryReposotory;
        static ApplicationDbContext _context;
        public MandirRepository(ApplicationDbContext context)
        {
            _context = context;
            _templeReposotory = new TempleRepository(_context);
            _godReposotory = new GodRepository(_context);
            _productReposotory = new ProductRepository(_context);
            _pujaReposotory = new PujaRepository(_context);
            _packageReposotory = new PackageRepository(_context);
            _libraryReposotory = new LibraryRepository(_context);
        }
        public List<God> Gods(string culture, int num)
        {
            try
            {
                if (culture != "English")
                {
                    var context = _context.Gods.Where(x => x.DelFlg == false).Select(x => new { x.Id, x.MultiLingual, x.ImageUrl, x.Title, x.Summary }).Take(num).ToList();
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl,aa.Title, aa.Summary)).ToList();
                    var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    if (returnProduct.Replace("{}", "").Replace(",", "").Replace("[]", "").Count() > 0)
                    {
                        List<God> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<God>>(returnProduct).ToList();
                        if (products.Count > 1)
                        {
                            return products;
                        }
                    }
                    return _context.Gods.Where(x => x.DelFlg == false).Take(num).ToList();
                }
                else
                {
                    return _context.Gods.Where(x => x.DelFlg == false).Take(num).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorMessages("SelectPackage : " + ex.Message);
                return null;
            }
        }
        public God SingleGod(string culture, Guid id)
        {
            if (culture != "English")
            {
                var product = (from s in _context.Gods where s.Id == id where s.DelFlg==false select s).SingleOrDefault();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                var kk = product.MultiLingual.Replace("{\"Tranlations\":", "");
                kk = kk.Substring(0, kk.Length - 1);
                var data = (JArray) JsonConvert.DeserializeObject(kk);
                foreach (var item in data)
                {
                    var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                    var idata = (JObject) JsonConvert.DeserializeObject(aaaaaa);
                    string heading = idata["key"].Value<string>();
                    string content = idata["value"].Value<string>();
                    if (heading.IndexOf(culture) > -1)
                    {
                        dictionary.Add(heading.Replace(culture, ""), content);
                    }
                }
                var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                God aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<God>(asdsd);
                return aaaa;
            }
            else
            {
                var product = (from s in _context.Gods where s.Id == id where s.DelFlg==false select s).SingleOrDefault();
                return product;
            }
        }
        public List<Puja> Puja(string culture, int num,string other="*")
        {
            try
            {
                if (culture != "English")
                {
                    var context = _context.Pujas.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Select(x => new { x.Id, x.MultiLingual, x.ImageUrl,x.Title, x.Summary ,x.Description}).Take(num).ToList();
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl, aa.Title, aa.Summary,aa.Description)).ToList();
                    var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    List<Puja> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Puja>>(returnProduct).ToList();
                    return products;
                }
                else
                {
                    return _context.Pujas.Where(x=>x.DelFlg==false).Where(x => x.Active == true).Where(x => x.Verified == true).Take(num).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorMessages("SelectPackage : " + ex.Message);
                return null;
            }
        }
        public Puja SinglePuja(string culture, Guid id)
        {
            if (culture != "English")
            {
                var product = (from s in _context.Pujas where s.Id == id where s.DelFlg == false select s).SingleOrDefault();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                var kk = product.MultiLingual.Replace("{\"Tranlations\":", "");
                kk = kk.Substring(0, kk.Length - 1);
                var data = (JArray)JsonConvert.DeserializeObject(kk);
                foreach (var item in data)
                {
                    var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                    var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);
                    string heading = idata["key"].Value<string>();
                    string content = idata["value"].Value<string>();
                    if (heading.IndexOf(culture) > -1)
                    {
                        dictionary.Add(heading.Replace(culture, ""), content);
                    }
                }
                var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                Puja aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<Puja>(asdsd);
                return aaaa;
            }
            else
            {
                var product = (from s in _context.Pujas where s.Id == id where s.DelFlg==false select s).SingleOrDefault();
                return product;
            }
        }
        public List<Temple> Temples(string culture, string country="Nepal", int intNumber = 5,Boolean random = false)
        {
            try
            {
                if (culture == null)
                {
                    culture = "English";
                }
                try
                {

                    if (country != null && culture != "English")
                    {
                        var context =
                            _context.Temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true)
                                .Select(x => new {x.Id, x.MultiLingual, x.ImageUrl, x.Title, x.Summary })
                                .ToList();
                        List<Dictionary<string, object>> dictionary =
                            context.Select(
                                aa =>
                                    RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl, aa.Title, aa.Summary,
                                        aa.ImageUrl)).ToList();
                        var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);

                        List<Temple> temples =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<List<Temple>>(returnProduct)
                                .Take(intNumber)
                                .ToList();
                        return temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x=>x.Active ==true).Where(x => x.Verified == true).ToList();
                    }


                    if (culture != "English")
                    {
                        var context = _context.Temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).OrderBy(x => Guid.NewGuid()).ToList().Where(x=>x.Verified==true).Select(x => new {x.Id, x.MultiLingual, x.ImageUrl, x.Title, x.Summary }).ToList();
                        List<Dictionary<string, object>> dictionary =
                            context.Select(
                                aa =>
                                    RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl, aa.Title, aa.Summary)).ToList();
                        var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                        if (returnProduct.Replace("{}", "").Replace(",", "").Replace("[]", "").Count() > 0)
                        {
                            List<Temple> products =
                                Newtonsoft.Json.JsonConvert.DeserializeObject<List<Temple>>(returnProduct)
                                    .Take(intNumber)
                                    .ToList();
                            return products;
                                /*.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).ToList();*/
                        }
                        else
                        {
                            return _context.Temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).OrderBy(x => Guid.NewGuid()).ToList().Take(intNumber).ToList().Where(x => x.Active == true).Where(x => x.Verified == true).ToList();
                        }
                    }

                    if (country != null && culture == "English")
                    {
                        return _context.Temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).OrderBy(x => Guid.NewGuid()).ToList().Take(intNumber).ToList().Where(x => x.Active == true).Where(x=>x.Verified ==true).ToList();
                    }
                    return _context.Temples.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).OrderBy(x => Guid.NewGuid()).ToList().Take(intNumber).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Temple SingleTemple(string culture,Guid guid)
        {
            if (culture != "English")
            {
                var product = (from s in _context.Temples where s.Id == guid where s.DelFlg==false select s).SingleOrDefault();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                var kk = product.MultiLingual.Replace("{\"Tranlations\":", "");
                kk = kk.Substring(0, kk.Length - 1);
                var data = (JArray)JsonConvert.DeserializeObject(kk);
                foreach (var item in data)
                {
                    var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                    var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);
                    string heading = idata["key"].Value<string>();
                    string content = idata["value"].Value<string>();
                    if (heading.IndexOf(culture) > -1)
                    {
                        dictionary.Add(heading.Replace(culture, ""), content);
                    }
                }
                var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                Temple aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<Temple>(asdsd);
                return aaaa;
            }
            else
            {
                var product = (from s in _context.Temples where s.Id == guid where s.DelFlg==false select s).SingleOrDefault();
                return product;
            }
        }
        public List<Country> Countries()
        {
            try
            {
                return _context.Countries.Where(x => x.DelFlg == false).Where(x => x.Title.ToString().ToLower() == "nepal").ToList();
            }
            catch (Exception ex)
            {
                ErrorMessages("SelectAll : " + ex.Message);

                return null;
            }
            
        }
        public List<Gallery> Gallery(int num,string ctrl,string guid)
        {
            Guid id;
            if (guid == "All")
            {
                return _context.Galleries.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x => x.ImageUrl != null).Where(x => x.ImageUrl != "").OrderBy(x => Guid.NewGuid()).Take(num).ToList();
            }
            try
            {
                id = Guid.Parse(guid);
                return _context.Galleries.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x=>x.DataId == id.ToString()).Where(x => x.Controller.ToString().ToLower() == ctrl.ToLower()).Where(x => x.ImageUrl != null).Where(x => x.ImageUrl != "").OrderBy(x => Guid.NewGuid()).Take(num).ToList();
            }
            catch (Exception)
            {
                return _context.Galleries.Where(x => x.DelFlg == false).Where(x=>x.Active==true).Where(x=>x.Verified==true).Where(x => x.Controller.ToString().ToLower() == ctrl.ToLower()).OrderBy(x => Guid.NewGuid()).Where(x=>x.ImageUrl != null).Where(x=>x.ImageUrl != "").Take(num).ToList();
            }
        }
        public List<Package> Packages(string culture,int num)
        {
            try
            {
                if (culture != "English") { 
                    var context = _context.Packages.Where(x => x.DelFlg == false).Where(x=>x.Active ==true).Where(x=>x.Verified==true).Select(x => new { x.Id, x.MultiLingual, x.ImageUrl, x.Title, x.Summary }).Take(num).ToList();
                List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl, aa.Title, aa.Summary)).ToList();
                var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                List<Package> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Package>>(returnProduct).ToList();
                    return products;
                }
                else
                {
                    return _context.Packages.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Take(num).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorMessages("SelectPackage : " + ex.Message);
                return null;
            }
        }
        public Package SinglePackage(string culture,Guid num)
        {
            try
            {
                return _context.Packages.Where(x => x.Id == num).SingleOrDefault();

            }
            catch (Exception ex)
            {
                ErrorMessages("PackageDetail : " + ex.Message);
                return null;

            }
        }
        public List<Library> Library(string culture,string segment, int num)
        {
            try
            {
                if (culture != "English")
                {
                    var context = _context.Articles.Where(x => x.DelFlg == false).Where(x=>x.Active==true).Where(x=>x.Verified==true).Where(x=>x.Segment.ToLower() == segment.ToLower()).Select(x => new { x.Id, x.MultiLingual, x.ImageUrl, x.Title, x.Summary }).Take(num).ToList();
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl, aa.Title, aa.Summary)).ToList();
                    var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    List<Library> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Library>>(returnProduct).Where(x => x.Title != null).ToList();
                    if (products.Count() > 1)
                    {
                        return  products;
                    }
                    else
                    {
                        return _context.Articles.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x => x.Segment.ToLower() == segment.ToLower()).Take(num).ToList();
                    }
                }
                else
                {
                    return _context.Articles.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x => x.Segment.ToLower() == segment.ToLower()).Take(num).ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorMessages("Library ("+  segment + ") : " + ex.Message);
                return null;
            }
        }
        public Library SingleLibrary(string culture, Guid id)
        {
            if (culture != "English")
            {
                var product = (from s in _context.Articles where s.Id == id where s.DelFlg == false select s).SingleOrDefault();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                var kk = product.MultiLingual.Replace("{\"Tranlations\":", "");
                kk = kk.Substring(0, kk.Length - 1);
                var data = (JArray)JsonConvert.DeserializeObject(kk);
                foreach (var item in data)
                {
                    var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                    var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);
                    string heading = idata["key"].Value<string>();
                    string content = idata["value"].Value<string>();
                    if (heading.IndexOf(culture) > -1)
                    {
                        dictionary.Add(heading.Replace(culture, ""), content);
                    }
                }
                var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                Library aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<Library>(asdsd);
                return aaaa;
            }
            else
            {
                var product = (from s in _context.Articles where s.Id == id where s.DelFlg == false select s).SingleOrDefault();
                return product;
            }
        }
        public List<Mantra> Mantra(string culture, int num)
        {
            try
            {
                    return _context.Mantras.Take(num).ToList();
            }
            catch (Exception ex)
            {
               // ErrorMessages("Library (" + segment + ") : " + ex.Message);
                return null;
            }
        }
        public List<Bhajan> Bhajan(string culture, int num)
        {
            try
            {
                return _context.Bhajans.Where(x=>x.DelFlg==false).Where(x=>x.Verified==true).Where(x=>x.Active == true).Take(num).ToList();
            }
            catch (Exception ex)
            {
                // ErrorMessages("Library (" + segment + ") : " + ex.Message);
                return null;
            }
        }
        public List<Categories> Categories(string culture = "English")
        {
            if (culture == null){culture = "English";}
            try
            {
                if (culture != "English"){ 
                    var context = _context.Categories.Where(x => x.DelFlg == false).Select(x => new { x.Id, x.MultiLingual ,x.Title}).ToList();
                    string neededItems = string.Empty;
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(),null,aa.Title)).ToList();
                    var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    List<Categories>  ca= Newtonsoft.Json.JsonConvert.DeserializeObject<List<Categories>>(asdsd).ToList();
                   if (ca.Count > 0) { 
                        return ca;
                    }
                }
                return _context.Categories.Where(x => x.DelFlg == false).ToList();
            }
            catch (Exception ex)
            {
                ErrorMessages("ShowProductCateory : " + ex.Message);
                return null;
            }
        }
        public Product SingleProducts(Guid guid, string culture)
        {
            if (culture != "English")
            {
                var singleProduct = (from s in _context.Products where s.Id == guid select s).SingleOrDefault();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                var kk = singleProduct.MultiLingual.Replace("{\"Tranlations\":", "");
                kk = kk.Substring(0, kk.Length - 1);
                var data = (JArray)JsonConvert.DeserializeObject(kk);
                foreach (var item in data)
                {
                    var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
                    var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);
                    string heading = idata["key"].Value<string>();
                    string content = idata["value"].Value<string>();
                    if (heading.IndexOf(culture) > -1)
                    {
                        string removalContent = RockmanduHtmlRemoval.StripTagsCharArray(content);
                        if ((removalContent != "br&nbsp;") && (!string.IsNullOrEmpty(removalContent)))
                        {
                            if (heading.ToLower() != "price" && heading.ToLower() != "id")
                            {
                                try
                                {
                                    dictionary.Add(heading.Replace(culture, ""), content);

                                }
                            catch
                                (Exception)
                                {
                                    
                                    
                                }
                            }

                        }
                    }
                    string TranslationNeeded = " <Translation Needed Here>";
                    try
                    {

                    }
                    catch (Exception)
                    {

                        dictionary.Add("Id", singleProduct.Id);

                    }

                    try
                    {
                      //  if (!string.IsNullOrEmpty(title))
                            dictionary.Add("Title", singleProduct.Title+ TranslationNeeded);
                    }
                    catch
                        (Exception)
                    {
                    }
                    try
                    {
                       // if (!string.IsNullOrEmpty(imageurl))
                            dictionary.Add("ImageUrl", singleProduct.ImageUrl+ TranslationNeeded);

                    }
                    catch
                        (Exception)
                    {
                    }
                    try
                    {
                        //if (!string.IsNullOrEmpty(summary))
                            dictionary.Add("Summary", TranslationNeeded + singleProduct.Summary);

                    }
                    catch
                        (Exception)
                    {
                    }
                    try
                    {
                        //if (!string.IsNullOrEmpty(description))
                            dictionary.Add("Description", TranslationNeeded + singleProduct.Description);

                    }
                    catch
                        (Exception)
                    {
                    }
                    try
                    {
                      //  if (!string.IsNullOrEmpty(price))
                            dictionary.Add("Price", singleProduct.Price);

                    }
                    catch
                        (Exception)
                    {
                    }
                    try
                    {
                        //  if (!string.IsNullOrEmpty(price))
                        dictionary.Add("Speciality", TranslationNeeded + singleProduct.Speciality);

                    }
                    catch
                        (Exception)
                    {
                    }
                }
                var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                Product aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<Product>(asdsd);
                if (!string.IsNullOrEmpty(aaaa.Title) && !string.IsNullOrEmpty(aaaa.Description) && !string.IsNullOrEmpty(aaaa.Summary))
                {
                    return aaaa;
                }
                else
                {
                    return singleProduct;
                }
            }
            else
            {
                var product = (from s in _context.Products where s.Id == guid where s.DelFlg==false  select s).SingleOrDefault();
                return product;
            }
        }
        public List<Product> Products(string culture,string category,int intNumber= 5)
        {
            if (culture == null)
            {
                culture = "English";}
            try
            {

                if (category != null && culture != "English")
                {
                    var context = _context.Products.Where(x => x.DelFlg == false).Where(x=>x.Active == true).Where(x=>x.Verified == true).Where(x => x.Category.Title==category).Select(x => new { x.Id, x.MultiLingual ,x.ImageUrl,x.Title,x.Summary,x.Description,x.Price}).Where(x=>x.Title != null).Take(intNumber).ToList();
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl,aa.Title,aa.Summary, aa.Description,aa.Price.ToString())).ToList();
                    var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    List<Product> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(returnProduct).Take(intNumber).ToList();
                    if (products.Count() > 0)
                    {
                        return products;
                    }
                        return _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x => x.Category.Title == category).Take(intNumber).ToList();
                }
                if (category != null && culture == "English")
                {
                    return _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Where(x => x.Category.Id == Guid.Parse(category)).Take(intNumber).ToList();
                }
                if (culture != "English")
                {
                    var context = _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Select(x => new { x.Id, x.MultiLingual ,x.ImageUrl,x.Title,x.Summary,x.Price}).ToList();
                    List<Dictionary<string, object>> dictionary = context.Select(aa => RockmanduBase.FetchCultureobject(culture, aa.MultiLingual, aa.Id.ToString(), aa.ImageUrl,aa.Title,aa.Summary,null,aa.Price.ToString())).ToList();
                    var returnProduct = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                    if (returnProduct.Replace("{}", "").Replace(",","").Replace("[]","").Count() > 0)
                    {
                        List<Product> products = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(returnProduct).Take(intNumber).ToList();
                        if (products.Count() > 0)
                        {
                            return _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Take(intNumber).ToList();
                        }
                        //else
                        //{
                        //    return _context.Products.Where(x => x.DelFlg == false).Take(intNumber).ToList();
                        //}
                    }
                    else
                    {
                        return _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Take(intNumber).ToList();
                    }
                }
                return _context.Products.Where(x => x.DelFlg == false).Where(x => x.Active == true).Where(x => x.Verified == true).Take(intNumber).OrderBy(x => Guid.NewGuid()).ToList();
                //if (category == "aaa" || category == null)
                //{
                //    return _context.Products.ToList();
                //}
                //else
                //{
                //    return _context.Products.Where(x => x.Category.Title == category).ToList();
                //}
            }
            catch (Exception ex)
            {
                ErrorMessages("Products : " + ex.Message);
                return null;
            }
        }
        public SearchViewModeltest Search(string searchData,string culture)
        {
            SearchViewModeltest sm= new SearchViewModeltest();
            List<Search> results = new List<Search>();
            sm.Results = new List<Search>();

            var temples = _templeReposotory.GetAll().Where(x=>x.Verified==true).Where(x=>x.Active == true).Where(x=>x.DelFlg==false).Where(x=>x.Title.ToLower().Contains(searchData.ToLower()));
            var products = _productReposotory.GetAll().Where(x => x.Verified == true).Where(x => x.Active == true).Where(x => x.DelFlg == false).Where(x => x.Title.ToLower().Contains(searchData.ToLower()));
            var god = _godReposotory.GetAll().Where(x => x.Verified == true).Where(x => x.Active == true).Where(x => x.DelFlg == false).Where(x => x.Title.ToLower().Contains(searchData.ToLower()));
            var puja = _pujaReposotory.GetAll().Where(x => x.Verified == true).Where(x => x.Active == true).Where(x => x.DelFlg == false).Where(x => x.Title.ToLower().Contains(searchData.ToLower()));
            var packages = _packageReposotory.GetAll().Where(x => x.Verified == true).Where(x => x.Active == true).Where(x => x.DelFlg == false).Where(x => x.Title.ToLower().Contains(searchData.ToLower()));
            var library = _libraryReposotory.GetAll().Where(x => x.Verified == true).Where(x => x.Active == true).Where(x => x.DelFlg == false).Where(x => x.Title.ToLower().Contains(searchData.ToLower()));

            foreach (var items in temples)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Temples/Details/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            foreach (var items in products)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Store/Details/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            foreach (var items in god)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Gods/Details/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            foreach (var items in puja)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Services/Details/Puja/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            foreach (var items in packages)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Services/Details/Package/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            foreach (var items in library)
            {
                Search search = new Search();
                search.Title = items.Title;
                search.Description = items.Summary;
                search.Image = items.ImageUrl;
                search.Url = "~/" + culture + "/Library/Details/"+ items.Segment + "/" + items.Title.Replace(" ", "_") + "/" + items.Id;
                sm.Results.Add(search);
            }
            return sm;
        }
        public Boolean ErrorMessages(string message)
        {
            try
            {
                //var lc = new LoggerConfiguration();
                //lc = Startup.GetLoggerConfiguration();
                //lc.CreateLogger().Error(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}