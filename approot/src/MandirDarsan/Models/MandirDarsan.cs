﻿//using System;
//using System.ComponentModel.DataAnnotations;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Security.Policy;
//using System.Threading.Tasks;
////using MandirDarsan.Migrations;
//using Newtonsoft.Json;
////using Serilog.Enrichers;
//namespace MandirDarsan.Models
//{
//    public class Categories : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Required]
//        public String Name { get; set; }
//        [Required]
//        public Boolean HasSubCategory { get; set; }
//        public int Categoryof { get; set; }
//        public int DisplayOrder { get; set; }
//        public string Description { get; set; }
//        public List<Product> Products { get; set; }
//    }
//    public class Language : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//    }
//    public class Translation : RockMandatory, IRockmanduBase// : IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public int LanguageId { get; set; }
//        public string Segment { get; set; }
//        public int Content { get; set; }
//        public int TranslatedContent { get; set; }
//        public virtual Language Language { get; set; }
//    }
//    public class Product : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public int CategoryId { get; set; }

//        [Required]
//        [Display(Name = "Name")]
//        public String Name { get; set; }
//        [Required]
//        [Display(Name = "Description")]
//        public String Description { get; set; }
//        [Required]
//        [Display(Name = "Price")]
//        public Decimal Price { get; set; }
//        [Required]
//        [Display(Name = "Summary")]
//        public String Summary { get; set; }
//        [Required]
//        [Display(Name = "Speciality")]
//        public String Speciality { get; set; }

//        [Display(Name = "Main Image")]
//        public String ImageUrl1 { get; set; }

//        [Display(Name = "Secondary Image-1")]
//        public String ImageUrl2 { get; set; }

//        [Display(Name = "Secondary Image-2")]
//        public String ImageUrl3 { get; set; }

//        [Display(Name = "Secondary Image-3")]
//        public String ImageUrl4 { get; set; }
//        //public virtual List<SiteImage> Images { get; set; }
//        public Categories Category { get; set; }
//    }
//    public class Article : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string Title { get; set; }
//        public string ImageUrl { get; set; }
//        public string Description { get; set; }
//        [DataType(DataType.Date)]
//        public DateTime PublishDate { get; set; }
//        public string Segment { get; set; }
//        public List<ArticleTag> PostTags { get; set; }
//        public string Summary { get; set; }
//    }
//    public class Booking : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Display(Name = "Full Name")]
//        [Required]
//        public String Name { get; set; }
//        [Display(Name = "Email")]
//        [Required]
//        public String Email { get; set; }
//        [Display(Name = "Contact")]
//        [Required]
//        public String Contact { get; set; }
//        public int CountryId { get; set; }
//        public virtual Country Country { get; set; }
//        [Display(Name = "City")]
//        [Required]
//        public String City { get; set; }
//        [Display(Name = "Full Address")]
//        [Required]
//        public String Address { get; set; }
//        public int PackageId { get; set; }
//        public virtual Package Package { get; set; }
//        [Display(Name = "Preferrable Date")]
//        [Required]
//        [DataType(DataType.Date)]
//        public DateTime Date { get; set; }
//        [Display(Name = "Children")]
//        [Range(1, 10)]
//        [Required]
//        public int Children { get; set; }
//        [Display(Name = "Adult")]
//        [Range(1, 10)]
//        [Required]
//        public int Adult { get; set; }
//        [Display(Name = "Additional Information")]
//        [DataType(DataType.MultilineText)]
//        public String Info { get; set; }

//    }
//    public class Cart //: RockMandatory, IRockmanduBase
//    {
//        [Key]
//        public Guid RecordId { get; set; }
//        public string CartId { get; set; }
//        public Guid ProductId { get; set; }
//        public int Count { get; set; }
//        [DataType(DataType.DateTime)]
//        public DateTime DateCreated { get; set; }
//        public virtual Product Product { get; set; }
//    }
//    public class Contact : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Required]
//        [Display(Name = "Full Name")]
//        public String Name { get; set; }
//        [Required]
//        [Display(Name = "Full Address")]
//        public String Address { get; set; }
//        [Required]
//        [Display(Name = "Email Address")]
//        public String Email { get; set; }
//        [Required]
//        [Display(Name = "Phone/ Mobile No.")]
//        public String Phone { get; set; }
//        [Required]
//        [Display(Name = "Inquiry/ Comments")]
//        [DataType(DataType.MultilineText)]
//        public String Message { get; set; }
//    }
//    public class Country : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string CountryName { get; set; }
//    }
//    public class Enquiry : RockMandatory, IRockmanduBase //: IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Required]
//        public String Name { get; set; }
//        [Required]
//        public String Email { get; set; }
//        public int PackageId { get; set; }
//        public virtual Package Package { get; set; }
//        [Required]
//        [DataType(DataType.MultilineText)]
//        public String Message { get; set; }
//    }
//    public class Gallery : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string Controller { get; set; }
//        public string DataId { get; set; }
//        public string Tag { get; set; }
//        public string Description { get; set; }
//        public string Title { get; set; }
//        public string ImageUrl { get; set; }
//        public string Summary { get; set; }
//    }
//    public class God : RockMandatory, IRockmanduBase // RockMandatory
//    {
//        public Guid Id { get; set; }
//        [Required]
//        [Display(Name = "God")]
//        public string Title { get; set; }
//        [Required]
//        [Display(Name = "Some Description")]
//        [DataType(DataType.MultilineText)]
//        public string Description { get; set; }
//        [Required]
//        [Display(Name = "Image")]
//        public String ImageUrl { get; set; }
//        [Display(Name = "Famous Temple")]
//        public bool Display { get; set; }
//        public string Summary { get; set; }
//        //public  virtual ICollection<Temple> Temples { get; set; }
//        //public  virtual ICollection<Puja> Puja { get; set; }
//    }
//    public class Siteset : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string AboutText { get; set; }
//        public string WelcomeText { get; set; }
//        public string Copyright { get; set; }
//        public string Logo { get; set; }
//        public string Phone { get; set; }
//        public string Email { get; set; }
//        public string FacebookUrl { get; set; }
//        public string PrivacyPolicy { get; set; }
//        public string TermsandCondition { get; set; }
//    }
//    public class Newsletter : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Required]
//        [Display(Name = "Email")]
//        public String Email { get; set; }
//    }
//    public class Order : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string Username { get; set; }
//        [Required(ErrorMessage = "First Name is required")]
//        [Display(Name = "First Name")]
//        [StringLength(160)]
//        public string FirstName { get; set; }
//        [Required(ErrorMessage = "Last Name is required")]
//        [Display(Name = "Last Name")]
//        [StringLength(160)]
//        public string LastName { get; set; }
//        [Required(ErrorMessage = "Address is required")]
//        [StringLength(70)]
//        public string Address { get; set; }
//        [Required(ErrorMessage = "City is required")]
//        [StringLength(40)]
//        public string City { get; set; }
//        public string PostalCode { get; set; }
//        public int CountryId { get; set; }
//        public virtual Country Country { get; set; }
//        [Required(ErrorMessage = "Phone is required")]
//        [StringLength(24)]
//        public string Phone { get; set; }
//        [Required(ErrorMessage = "Email Address is required")]
//        [Display(Name = "Email Address")]
//        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
//          ErrorMessage = "Email is is not valid.")]
//        [DataType(DataType.EmailAddress)]
//        public string Email { get; set; }
//        public decimal Total { get; set; }
//        public DateTime OrderDate { get; set; }
//        public List<OrderDetail> OrderDetails { get; set; }
//    }
//    public class OrderDetail : RockMandatory
//    {
//        public Guid OrderDetailId { get; set; }
//        public Guid OrderId { get; set; }
//        public Guid ProductId { get; set; }
//        public int Quantity { get; set; }
//        public decimal UnitPrice { get; set; }
//        public virtual Product Product { get; set; }
//        public virtual Order Order { get; set; }
//    }
//    public class OrderPhone : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        [Required]
//        [Display(Name = "Full Name")]
//        public String Name { get; set; }
//        [Required]
//        [Display(Name = "Email")]
//        public String Email { get; set; }
//        [Required]
//        [Display(Name = "Contact Number")]
//        public String Phone { get; set; }
//        public int ProductId { get; set; }
//        public virtual Product Product { get; set; }
//    }
//    public class Package : RockMandatory, IRockmanduBase
//    {
//        public Package()
//        {
//            this.Temples = new HashSet<Temple>();
//        }
//        public Guid Id { get; set; }
//        [Display(Name = "Package Title")]
//        [Required]
//        public String Title { get; set; }
//        [Display(Name = "Destination")]
//        [Required]
//        public String Destination { get; set; }
//        [Display(Name = "Duration")]
//        [Required]
//        public String Duration { get; set; }
//        [Display(Name = "Price")]
//        [Required]
//        public int Price { get; set; }
//        [Display(Name = "Activities")]
//        [Required]
//        public String Activity { get; set; }
//        [Display(Name = "Group-size")]
//        [Required]
//        public String GroupSize { get; set; }
//        [Display(Name = "Description")]
//        [Required]

//        [DataType(DataType.MultilineText)]
//        public String Description { get; set; }
//        [Display(Name = "ImageUrl")]
//        [Required]
//        public String ImageUrl { get; set; }
//        [Display(Name = "Map")]
//        [Required]

//        public String Map { get; set; }
//        [Display(Name = "In HomePage")]
//        public Boolean InHomePage { get; set; }
//        public ICollection<Temple> Temples { get; set; }

//    }
//    //public class SiteImage // : RockMandatory
//    //{
//    //    public int SiteImageId { get; set; }
//    //    public String ImageUrl { get; set; }
//    //}
//    public class Puja : RockMandatory, IRockmanduBase
//    {
//        public Puja()
//        {
//            // this.Temples = new HashSet<Temple>();
//            //this.Gods =new HashSet<God>();
//        }
//        public Guid Id { get; set; }
//        [Display(Name = "Name")]
//        [Required]
//        public String Name { get; set; }
//        public int TempleId { get; set; }
//        public virtual Temple Temple { get; set; }
//        //public ICollection<Temple> Temples { get; set; }
//        //public ICollection<God> Gods { get; set; }
//        [Required]
//        [Display(Name = "Description")]
//        [DataType(DataType.MultilineText)]
//        public String Description { get; set; }
//        public string Summary { get; set; }

//        [Required]
//        [Display(Name = "ImageUrl")]
//        public String ImageUrl { get; set; }
//    }
//    public class Bhajan : RockMandatory, IRockmanduBase
//    {
//        public Guid Id { get; set; }
//        public string Title { get; set; }
//        public string Summary { get; set; }
//        public string Description { get; set; }
//        public string Timeduration { get; set; }
//        public List<BhajanTag> BhajanTags { get; set; }
//        public string YouTubeId { get; set; }
//    }
//    public class RockMandatory
//    {
//        //public string AuthorUserId { get; set; }
//        public string CreatedBy { get; set; }
//        public DateTime CreatedDate { get; set; }

//        public string ModifiedBy { get; set; }
//        public DateTime ModifiedDate { get; set; }


//        public string VerifiedBy { get; set; }
//        public DateTime VerifiedDate { get; set; }
//        public Boolean DelFlg { get; set; }
//        public Boolean Active { get; set; }

//        //string CreatedBy { get; set; }
//        //DateTime CreatedDate { get; set; }
//        //string ModifiedBy { get; set; }
//        //DateTime ModifiedDate { get; set; }
//        //DateTime VerifiedBy { get; set; }
//        //DateTime VerifiedDate { get; set; }
//        //Boolean DelFlg { get; set; }
//        //Boolean Active { get; set; }
//    }
//    public class Temple : RockMandatory, IRockmanduBase // : RockMandatory
//    {
//        //public Temple()
//        //{
//        //    this.Gods = new HashSet<God>();
//        //    this.Puja = new HashSet<Puja>();
//        //}
//        public Guid Id { get; set; }
//        //public int GodId { get; set; }
//        [Required]
//        public string Name { get; set; }
//        [Required]
//        [Display(Name = "Image")]
//        public string ImageUrl { get; set; }
//        [Required]
//        [Display(Name = "Description")]
//        [DataType(DataType.MultilineText)]
//        public string Description { get; set; }
//        public int CountryId { get; set; }
//        [Display(Name = "Country")]
//        public virtual Country Country { get; set; }
//        [Required]
//        [Display(Name = "District/State")]
//        public string Dis { get; set; }
//        [Required]
//        [Display(Name = "City/VDC")]
//        public string City { get; set; }
//        [Required]
//        public string Location { get; set; }
//        [Display(Name = "Display In Footer?")]
//        public bool InFooter { get; set; }
//        public string GeoLocation { get; set; }
//        public string Summary { get; set; }
//        public virtual God God { get; set; }
//        public ICollection<God> Gods { get; set; }
//        public ICollection<Puja> Puja { get; set; }
//    }
//    public class Tag
//    {
//        public Guid TagId { get; set; }
//        public List<ArticleTag> ArticleTags { get; set; }
//        public List<BhajanTag> BhajanTags { get; set; }
//    }
//    public class ArticleTag
//    {
//        public Guid ArticleId { get; set; }
//        public Article Post { get; set; }

//        public Guid TagId { get; set; }
//        public Tag Tag { get; set; }
//    }
//    public class BhajanTag
//    {
//        public Guid BhajanId { get; set; }
//        public Bhajan Bhajan { get; set; }
//        public Guid TagId { get; set; }
//        public Tag Tag { get; set; }
//    }
//}
