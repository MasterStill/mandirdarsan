﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
//using EFAuditing;
using MandirDarsan.Areas.Admin.Models;
//using FizzWare.NBuilder;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using MandirDarsan.Models;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Extensions.Configuration;
//using MySql.Data.Entity;
//using MySql.Data.MySqlClient;

//using TrackerEnabledDbContext.Identity;

namespace MandirDarsan.Models
{
    //public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, TrackerIdentityContext<ApplicationUser>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> 
    {
        //private ApplicationDbContext _context;
        //public ApplicationDbContext(ApplicationDbContext db)
        //{
        //    _context = db;
        //}
        private IConfigurationRoot Configuration;
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // builder.Entity<IdentityUser>().ToTable("Users");
            //builder.Entity<IdentityRole>().ToTable("Roles");
            //builder.Entity<IdentityUserClaim>().ToTable("Roles");
            builder.Entity<LibraryTag>()
                .HasOne(x => x.Library)
                .WithMany(x => x.PostTags)
                .HasForeignKey(x => x.LibraryId)
                .OnDelete(DeleteBehavior.Restrict);

            //builder.Entity<BhajanTag>()
            //    .HasOne(x => x.Bhajan)
            //    .WithMany(x => x.BhajanTags)
            //    .HasForeignKey(x => x.BhajanId)
            //    .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<TempleGod>()
               .HasOne(x => x.Temple)
               .WithMany(x => x.TempleGods)
               .HasForeignKey(x => x.TempleId)
               .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<TemplePuja>()
                .HasOne(x => x.Temple)
                .WithMany(x => x.TemplePujas)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.TempleId)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<PackageTemple>()
               .HasOne(x => x.Package)
               .WithMany(x => x.PackageTemples)
               .HasForeignKey(x => x.TempleId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<PackagePuja>()
                .HasOne(x => x.Package)
                .WithMany(x => x.PackagePuja)
                .HasForeignKey(x => x.PujaId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<GuruPuja>()
                .HasOne(x => x.Guru)
                .WithMany(x => x.GuruPujas)
                .HasForeignKey(x => x.PujaId)
                .OnDelete(DeleteBehavior.Restrict);
            //.HasKey(x => new {x.LibraryId, x.TagId});

            builder.Entity<CountryState>()
                .HasOne(x => x.Country)
                .WithMany(x => x.States)
                .HasForeignKey(x => x.CountryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<GuruState>()
                .HasOne(x => x.State)
                .WithMany(x => x.GuruState)
                .HasForeignKey(x => x.StateId)
                .OnDelete(DeleteBehavior.Restrict);

            //builder.Entity<BhajanTag>().HasKey(x => new { x.BhajanId, x.TagId });
            builder.Entity<TempleGod>().HasKey(x => new { x.TempleId, x.GodId});
            builder.Entity<TemplePuja>().HasKey(x => new {x.TempleId, x.PujaId});
            builder.Entity<PackageTemple>().HasKey(x => new {x.PackageId, x.TempleId});
            builder.Entity<LibraryTag>().HasKey(x => new { x.LibraryId, x.TagId });
            builder.Entity<PackagePuja>().HasKey(x => new { x.PackageId, x.PujaId});
            builder.Entity<GuruPuja>().HasKey(x => new { x.GuruId, x.PujaId });
            builder.Entity<CountryState>().HasKey(x => new { x.CountryId, x.StateId});
            builder.Entity<GuruState>().HasKey(x => new { x.GuruId, x.StateId });
            //builder.Entity<Globalization>()




            base.OnModelCreating(builder);
        }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Library> Articles { get; set; }
        public DbSet<Temple> Temples { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<God> Gods { get; set; }
        public DbSet<Puja> Pujas { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Enquiry> Enquiries { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        //public DbSet<Booking> Books { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderPhone> OrderPhones { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        //public DbSet<Language> Languages { get; set; }
        //public DbSet<Translation> Translation { get; set; }
        public DbSet<Siteset> SiteSettings { get; set; }
        public DbSet<Bhajan> Bhajans { get; set; }
        public DbSet<RockAuditLog> Auditlogs { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        //public DbSet<Translation> Translations { get; set; }
        public DbSet<RockAuditLog> RockAuditLogs { get; set; }
        public DbSet<Globalization> Globalizations { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Culture> Cultures { get; set; }
        public DbSet<LanguageTitle> LanguageTitles { get; set; }
        public DbSet<SiteSection> SiteSections { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Mantra> Mantras { get; set; }

        //public DbSet<Search> Search { get; set; }
    }
    //public class MyDbConfiguration : DbConfiguration
    //{
    //    public MyDbConfiguration()
    //    {
    //        // Attempt to register ADO.NET provider 
    //        try
    //        {
    //            var dataSet = (DataSet)ConfigurationManager.GetSection("system.data");
    //            dataSet.Tables[0].Rows.Add(
    //                "MySQL Data Provider",
    //                ".Net Framework Data Provider for MySQL",
    //                "MySql.Data.MySqlClient",
    //                typeof(MySqlClientFactory).AssemblyQualifiedName
    //            );
    //        }
    //        catch (ConstraintException)
    //        {
    //            // MySQL provider is already installed, just ignore the exception 
    //        }

    //        // Register Entity Framework provider 
    //        SetProviderServices("MySql.Data.MySqlClient", new MySqlProviderServices());
    //        SetDefaultConnectionFactory(new MySqlConnectionFactory());
    //    }
    //}
}