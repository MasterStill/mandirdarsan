﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Antiforgery;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;

namespace MandirDarsan.Models
{
    public partial class ShoppingCart
    {

        //[FromServices]
        //public ApplicationDbContext DbContext { get; set; }

        //[FromServices]
        //public IAntiforgery Antiforgery { get; set; }

        private readonly ApplicationDbContext _dbContext;
        private string ShoppingCartId { get; set; }
        public ShoppingCart(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public static ShoppingCart GetCart(ApplicationDbContext db, HttpContext context)
        {
            var cart = new ShoppingCart(db);
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }
        public void AddToCart(Guid productid, int qty=1)
        {
            // Get the matching cart and album instances
            var cartItem = _dbContext.Carts.SingleOrDefault(
                c => c.CartId == ShoppingCartId
                && c.ProductId == productid
                );


            if (cartItem == null)
            {
                // Create a new cart item if no cart item exists
                cartItem = new Cart
                {
                    ProductId = productid,
                    CartId = ShoppingCartId,
                    Count = qty,
                    DateCreated = DateTime.Now
                };
                _dbContext.Carts.Add(cartItem);
            }
            else
            {
                // If the item does exist in the cart, 
                // then add one to the quantity
                cartItem.Count = cartItem.Count + qty;
            }
            // Save changes
            _dbContext.SaveChanges();
        }
        public int RemoveFromCart(Guid id)
        {
            // Get the cart

            var cartItem = _dbContext.Carts.Single(
            cart => cart.CartId == ShoppingCartId
            && cart.ProductId == id);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.Count > 1)
                {
                    cartItem.Count--;
                    itemCount = cartItem.Count;
                }
                else
                {
                    _dbContext.Carts.Remove(cartItem);
                }
                // Save changes
               // _dbContext.SaveChanges();
            }
            return itemCount;
}
            public void EmptyCart()
                {
                    var cartItems = _dbContext.Carts.Where(cart => cart.CartId == ShoppingCartId).ToArray();
                    _dbContext.Carts.RemoveRange(cartItems);
                }
        public async Task<List<Cart>> GetCartItems()
        {
            var cartItems = _dbContext.Carts.
                Where(cart => cart.CartId == ShoppingCartId).
                Include(c => c.Product).
                ToListAsync();
            return await cartItems;
        }
        //public List<Cart> GetCartItems()
        //{

        //    return db.Carts.Where(
        //    cart => cart.CartId == ShoppingCartId).ToList();

        //}
        public async Task<int> GetCount()
        {
            // Get the count of each item in the cart and sum them up
            return await(from cartItem in _dbContext.Carts
                         where cartItem.CartId == ShoppingCartId
                         select cartItem.Count).SumAsync();
            //int? count = (from cartItems in db.Carts
            //              where cartItems.CartId == ShoppingCartId
            //              select (int?)cartItems.Count).Sum();
            //// Return 0 if all entries are null
            //return count ?? 0;
        }


        public async Task<decimal> GetTotal()
        {
            // Multiply album price by count of that album to get
            // the current price for each of those albums in the cart
            //  sum all album price totals to get the cart total

            decimal? total = (from cartItems in _dbContext.Carts
                              where cartItems.CartId == ShoppingCartId
                              select (int?)cartItems.Count *
                              cartItems.Product.Price).Sum();

            return total ?? decimal.Zero;


        }

        //public async Task<int> CreateOrder(Order order)
        //{
        //    decimal orderTotal = 0;

        //    var cartItems = GetCartItems();
        //    // Iterate over the items in the cart, 
        //    // adding the order details for each
        //    var orderDetail = new OrderDetail
        //    {
        //        ProductId = item.ProductId,
        //        OrderId = order.OrderId,
        //        UnitPrice = item.Price,
        //        Quantity = item.Count,
        //    };
        //    //foreach (var item in cartItems)
        //    //{
        //    //    var orderDetail = new OrderDetail
        //    //    {
        //    //        ProductId = item.ProductId,
        //    //        OrderId = order.OrderId,
        //    //        UnitPrice = item.Product.Price,
        //    //        Quantity = item.Count
        //    //    };
        //    //    // Set the order total of the shopping cart
        //    //    orderTotal += (item.Count * item.Product.Price);

        //    //    _dbContext.OrderDetails.Add(orderDetail);

        //    //}
        //    // Set the order's total to the orderTotal count
        //    order.Total = orderTotal;
        //    _dbContext.Orders.Add(order);
        //    // Save the order
        //    //_dbContext.SaveChanges();
        //    // Empty the shopping cart
        //    EmptyCart();
        //    // Return the OrderId as the confirmation number
        //    return order.OrderId;
        //}
        public async Task<Guid> CreateOrder(Order order)
        {
            decimal orderTotal = 0;

            var cartItems = await GetCartItems();

            // Iterate over the items in the cart, adding the order details for each
            foreach (var item in cartItems)
            {
                var orderDetail = new OrderDetail
                {
                    ProductId = item.ProductId,
                    OrderId = order.Id,
                    UnitPrice = item.Product.Price,
                    Quantity = item.Count,
                };
                // Set the order total of the shopping cart
                order.Total = orderTotal;
                //orderTotal += (item.Count * album.Price);

                _dbContext.OrderDetails.Add(orderDetail);
            }

            // Set the order's total to the orderTotal count
            order.Total = orderTotal;

            // Empty the shopping cart
            EmptyCart();

            // Return the OrderId as the confirmation number
            return order.Id;
        }
        private string GetCartId(HttpContext context)
        {
            var cartId = context.Session.GetString("Session");

            if (cartId == null)
            {
                //A GUID to hold the cartId. 
                cartId = Guid.NewGuid().ToString();
                // Send cart Id as a cookie to the client.
                context.Session.SetString("Session", cartId);
            }

            return cartId;
        }

        //public void MigrateCart(string userName)
        //{
        //    var shoppingCart = db.Carts.Where(
        //        c => c.CartId == ShoppingCartId);

        //    foreach (Cart item in shoppingCart)
        //    {
        //        item.CartId = userName;
        //    }
        //    db.SaveChanges();
        //}

    }

}
