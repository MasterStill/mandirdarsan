﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MandirDarsan.Models
{
    public class ApplicationUser : IdentityUser
    {
        public String FName { get; set; }
        public String LName { get; set; }
        public String Telophone { get; set; }
        public String FAddress { get; set; }
        //public int CountryId { get; set; }
        //public virtual Country Country { get; set; }
        public String PCode { get; set; }
        public String City { get; set; }
        public String Country { get; set; }
        public String Description { get; set; }
        public string LanguagePreference { get;set;}
        public string Image { get; set; }
        //[InverseProperty("Author")]
        //public List<Library> AuthoredPosts { get; set; }

        //[InverseProperty("Contributor")]
        //public List<Library> ContributedToPosts { get; set; }
    }

    public class ProfileViewModel
    {
        public ApplicationUser ApplicationUser { get; set; }
        public IEnumerable<ActivityLog> ActivityLog { get; set; }

        //Contributions 
        //Purchase History
    }
}
