﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
namespace MandirDarsan.Controllers
{
   [Authorize]
    public class CheckoutController : Controller
    {
        [FromServices]
        public ApplicationDbContext DbContext { get; set; }
        private const string PromoCode = "FREE";
        public async Task<IActionResult> AddressAndPayment()
        {
            var cart = ShoppingCart.GetCart(DbContext, HttpContext);
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = await cart.GetCartItems(),
                CartTotal = await cart.GetTotal()
            };
            if (viewModel.CartItems.Count == 0)
            {

                return Redirect("~/Store");
                //Redirect("http://mandirdarsan.com/" + culture + "/Store");
            }
            return View(viewModel);
            //return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddressAndPayment([FromForm] Order order, CancellationToken requestAborted)
        {
            if (!ModelState.IsValid)
            {
                return View(order);
            }

            var formCollection = await HttpContext.Request.ReadFormAsync();

            try
            {
                if (string.Equals(formCollection["PromoCode"].FirstOrDefault(), PromoCode,
                    StringComparison.OrdinalIgnoreCase) == false)
                {
                    return View(order);
                }
                else
                {
                    order.Username = HttpContext.User.GetUserName();
                    order.OrderDate = DateTime.Now;

                    //Add the Order
                    DbContext.Orders.Add(order);

                    //Process the order
                    var cart = ShoppingCart.GetCart(DbContext, HttpContext);
                    await cart.CreateOrder(order);

                    // Save all changes
                    await DbContext.SaveChangesAsync(requestAborted);

                    return RedirectToAction("Complete", new { id = order.Id });
                }
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }
        public async Task<IActionResult> Complete(Guid id)
        {
            // Validate customer owns this order
            bool isValid = await DbContext.Orders.AnyAsync(
                o => o.Id == id &&
                o.Username == HttpContext.User.GetUserName());

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }

        // GET: Checkout
        //public ActionResult Shipping()
        //{
        //    ViewBag.CountryId = new SelectList(DbContext.Countries, "CountryId", "CountryName");
        //    // Return the view
        //    //var cart = ShoppingCart.GetCart(this.HttpContext);
        //    //ViewData["Cart"] = cart.GetCartItems();
        //    //ViewData["CartTotal"] = cart.GetTotal();
        //    return View();
        //}

        //public ActionResult Complete(int id)
        //{
        //    // Validate customer owns this order
        //    bool isValid = db.Orders.Any(
        //        o => o.OrderId == id &&
        //        o.Username == User.Identity.Name);

        //    if (isValid)
        //    {
        //        return View(id);
        //    }
        //    else
        //    {
        //        return View("Error");
        //    }
        //}
        //[HttpPost]
        //public ActionResult Shipping(FormCollection values)
        //{
        //    ViewBag.CountryId = new SelectList(DbContext.Countries, "CountryId", "CountryName");
        //    //var cart = ShoppingCart.GetCart(this.HttpContext);
        //    //ViewData["Cart"] = cart.GetCartItems();
        //    //ViewData["CartTotal"] = cart.GetTotal();
        //    var order = new Order();
        //    //TryUpdateModel(order);
        //    order.Username = User.Identity.Name;
        //    order.OrderDate = DateTime.Now;

        //    //Save Order
        //    DbContext.Orders.Add(order);
        //    DbContext.SaveChanges();
        //    //Process the order

        //    //cart.CreateOrder(order);

        //    return RedirectToAction("Complete",
        //        new { id = order.OrderId });

        //}
    }
}