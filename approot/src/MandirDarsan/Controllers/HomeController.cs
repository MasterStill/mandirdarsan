﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.AspNet.Server.Kestrel.Http;
using Microsoft.Data.Entity.Query.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using Serilog;
namespace MandirDarsan.Controllers
{
    public class HomeController : Controller
    {
        public static ApplicationDbContext _MContext;
        private IMandirRepository repository;
        public HomeController(ApplicationDbContext context)
        {
            try
            {
                if (HttpContext.Session.GetString("Culture") == null){}
            }
            catch(Exception)
            {
                //HttpContext.Session.Set("Culture","English");
            }
            repository = new MandirRepository(context);
                _MContext = context;
        }
        //public IActionResult किताब(string culture)
        //{
        //    return View(Index(culture));
        //}
        public IActionResult Gallery()
        {
            var gallery = repository.Gallery(10,"Gallery","All");
            return View(gallery);
        }
        public IActionResult Index(string culture)
        {
            if (culture == "" || culture == null)
            {
                return Redirect("/English/Home/");
            }
            if (culture.ToLower() == "english" || culture.ToLower() == "nepali" || culture.ToLower() == "hindi" ||
                culture.ToLower() == "sanskrit" || culture.ToLower() == "chinese")
            {
            }
            else
            {
                return Redirect("/English/Home/");
            }
            //ITemple temple = new TempleRepository(_MContext);
            //IProduct product = new ProductRepository(_MContext);
            //IGallery Gallery = new GalleryRepository(_MContext);
            try
            {
                ViewData["Banners"] = repository.Gallery(3,"Banners","");
                ViewData["Gallery2"] = repository.Gallery(10, "Gallery", "All");
                ViewData["Gallery1"] = repository.Gallery(15, "Gallery", "All");
                ViewData["Carousel"] = repository.Gallery(3, "Carousel", "");
                //ViewData["Gallery2"] = repository.Gallery(3, "Gallery");
            }
            catch (Exception)
            {
            }
            try
            {
                IEnumerable<Temple> temples = repository.Temples(culture,null,4).ToList();
                ViewData["Temple"] = temples;
                IEnumerable<Temple> mostVisited = repository.Temples(culture, null, 3).ToList();
                ViewData["MostVisitedSites"] = mostVisited;
            }
            catch (Exception){}
              IEnumerable<Product> products = repository.Products(culture, null, 3);
              ViewData["Products"] = products;
              MandirRepository _repository  = new MandirRepository(_MContext);
            var package =_repository.Packages(culture,3);
            var articles = _repository.Library(culture, "Article", 3);
            ViewBag.FrontVideo = "http://www.youtube.com/watch?v=OKpxuHLUzuA";
            if (package != null)
            {
                if (package.Count > 0)
                {
                    ViewData["Packages"] = package;
                }
            }
            if (articles != null)
            {
                if (articles.Count > 0)
                {
                    ViewData["Articles"] = articles;
                }
            }
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.BreadCr= "Contact Information";
            return View();
        }
        public IActionResult Cart()
        {
            return View();
        }
        public IActionResult ServiceDetails()
        {
            return View();
        }
        public IActionResult OurServices()
        {
            return View();
        }
        public ActionResult TempleDetails(int? id)
        {
            ViewBag.TempleDetailsActive = "active";
            return View();
        }
        public ActionResult PackageDetails(Enquiry enquiry)
        {
            ViewBag.PackageDetailsActive = "active";
            if (ModelState.IsValid)
            {
                _MContext.Enquiries.Add(enquiry);
                _MContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }
        public ActionResult PackageDetails(Guid? id)
        {
            ViewBag.PackageDetailsActive = "active";
            var package = (from s in _MContext.Packages where s.Id == id select s).ToList();
            ViewData["package"] = package;
            return View();
        }
        public ActionResult About(string culture)
        {
            try
            {
                IMandirRepository _mandirRepository = new MandirRepository(_MContext);
                List<God> god = (List<God>)_mandirRepository.Gods(culture,3);
            }
            catch (Exception ex)
            {
                //Log.Logger.Error(ex.Message);
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(Contact contact)
        {
            ViewBag.ContactActive = "active";
            //if (ModelState.IsValid)
            //{
                _MContext.Contacts.Add(contact);
                return RedirectToAction("Thanks");
            //}
            //return View(contact);
        }
        public ActionResult Thanks()
        {
            return View();
        }
        public ActionResult Contents()
        {
            ViewBag.MoreActive = "active";
            //List<News> news = _mandirRepository.ShowNews();
            //IEnumerable<PressRelease> press = _mandirRepository.ShowPressRelease();
            //IEnumerable<Article> articles = _mandirRepository.ShowArticle();
            ViewBag.HomeActive = "active";
            //ViewData["news"] = news;
            //ViewData["articles"] = articles;
            //ViewData["press"] = press;
            return View();
        }
        public ActionResult Search(string searchData,string culture)
        {
            ViewBag.Title = "Search";
            SearchViewModeltest sm = new SearchViewModeltest();
            if (searchData != null) { 
                searchData = searchData.ToLower();
                ViewBag.BreadCrumbHeader = "MandirDarsan Search";
                ViewBag.BreadCrumbFinalText = "Search Results";
                IMandirRepository mandirRepository = new MandirRepository(_MContext);
                _MContext.SaveChanges();
                sm = mandirRepository.Search(searchData,culture);
            }
            return View(sm);
        }
        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }
        public IActionResult StatusCodePage()
        {
            return View("~/Views/Shared/StatusCodePage.cshtml");
        }
        public IActionResult AccessDenied()
        {
            return View("~/Views/Shared/AccessDenied.cshtml");
        }
        //public IActionResult CompareDiv()
        //{
        //    Dictionary<string, object> dictionary = new Dictionary<string, object>();
        //    Categories c = new Categories();
        //    var context = _MContext.Categories.ToList();
        //    string neededstring ="[";
        //    string culture = "Nepali";
        //    foreach (var aa in context)
        //    {
        //        System.Diagnostics.Debug.WriteLine(aa);
        //        var kk = aa.MultiLingual.Replace("{\"Tranlations\":", "");
        //        kk = kk.Substring(0, kk.Length - 1);
        //        var data = (JArray)JsonConvert.DeserializeObject(kk);
        //        foreach (var item in data)
        //        {
        //            var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
        //            var idata = (JObject) JsonConvert.DeserializeObject(aaaaaa);

        //            string heading = idata["key"].Value<string>();
        //            string content = idata["value"].Value<string>();
        //            if (heading.IndexOf(culture) > -1)
        //            {
        //                dictionary.Add(heading,content);
        //            }
        //        }
        //    }
        //     var asdsd=  Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
        //    Categories aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<Categories>(asdsd);
        //    return View();
        //}
        //public IActionResult CompareDiv()
        //{
        //    List<Dictionary<string, object>> dictionary = new List<Dictionary<string, object>>();
        //    Categories c = new Categories();
        //    var context = _MContext.Categories.ToList();
        //    string neededstring = "[";
        //    string culture = "Nepali";
        //    foreach (var aa in context)
        //    {
        //        System.Diagnostics.Debug.WriteLine(aa);
        //        var kk = aa.MultiLingual.Replace("{\"Tranlations\":", "");
        //        kk = kk.Substring(0, kk.Length - 1);
        //        var data = (JArray)JsonConvert.DeserializeObject(kk);
        //        foreach (var item in data)
        //        {
        //            var aaaaaa = item.ToString().Replace(System.Environment.NewLine, "");
        //            var idata = (JObject)JsonConvert.DeserializeObject(aaaaaa);

        //            string heading = idata["key"].Value<string>();
        //            string content = idata["value"].Value<string>();
        //            if (heading.IndexOf(culture) > -1)
        //            {
        //                dictionary[0].Add(heading, content);
        //            }
        //        }
        //    }
        //    var asdsd = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
        //    List<Categories> aaaa = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Categories>>(asdsd);
        //    return View();
        //}
    }
}