﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity.Query.Internal;
//using Serilog;

namespace MandirDarsan.Controllers
{
    //[RequireHttps]
    public class GodsController : Controller
    {
        public static ApplicationDbContext Context;
        public IMandirRepository _repository;
        public GodsController (ApplicationDbContext context)
        {
            Context = context;
            _repository = new MandirRepository(Context);
        }
        public IActionResult Index(string culture)
        {
            var god = _repository.Gods(culture,8);
            return View(god);
        }
        public IActionResult Details(string culture,string id)
        {
            IEnumerable<Temple> temples = _repository.Temples(culture, null, 3).ToList();
            ViewData["Temple"] = temples;

            IEnumerable<Product> products = _repository.Products(culture, null, 3);
            ViewData["Products"] = products;

            var gods = _repository.Gods(culture, 4);
            ViewData["Gods"] = gods;

            var images = _repository.Gallery(4, "Gods",id);
            ViewData["Images"] = images;

            var god = _repository.SingleGod(culture, Guid.Parse(id));
            return View(god);
        }
    }
}