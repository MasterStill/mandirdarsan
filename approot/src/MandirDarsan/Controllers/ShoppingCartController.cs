using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Antiforgery;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Primitives;
namespace MandirDarsan.Controllers
{
    public class ShoppingCartController : Controller
    {
        [FromServices]
        public ApplicationDbContext DbContext { get; set; }
        public ShoppingCartController (ApplicationDbContext db)
        {
            DbContext = db;
        }
        [FromServices]
        public IAntiforgery Antiforgery { get; set; }

        //
        // GET: /ShoppingCart/
        public async Task<IActionResult> Index(string culture)
        {
            var cart = ShoppingCart.GetCart(DbContext, HttpContext);
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = await cart.GetCartItems(),
                CartTotal = await cart.GetTotal()
            };
            if (viewModel.CartItems.Count == 0)
            {

                return Redirect("~/" + culture + "/Store");
                //Redirect("http://mandirdarsan.com/" + culture + "/Store");
            }
            return View(viewModel);
        }
        //
        // GET: /ShoppingCart/AddToCart/5
        public async Task<IActionResult> AddToCart(Guid id, int qty, CancellationToken requestAborted,IFormCollection formCollection)
        {

            // Retrieve the album from the database
            //if (qty == 0) { 
            //    qty = Convert.ToInt32(formCollection.Where(x => x.Key == "qty").Select(x => x.Value).SingleOrDefault());
            //}

            var addedAlbum = DbContext.Products
                .Single(album => album.Id == id);

            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(DbContext, HttpContext);

           // cart.AddToCart(addedAlbum.ProductId,qty);
            if (qty == 0)
            {
                qty = 1;
            }
            cart.AddToCart(addedAlbum.Id, qty);

            await DbContext.SaveChangesAsync(requestAborted);

            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }

        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public async Task<IActionResult> RemoveFromCart(Guid id, CancellationToken requestAborted)
        {
            var cookieToken = string.Empty;
            var formToken = string.Empty;
            StringValues tokenHeaders;
            string[] tokens = null;

            if (HttpContext.Request.Headers.TryGetValue("RequestVerificationToken", out tokenHeaders))
            {
                tokens = tokenHeaders.First().Split(':');
                if (tokens != null && tokens.Length == 2)
                {
                    cookieToken = tokens[0];
                    formToken = tokens[1];
                }
            }
            try
            {
                Antiforgery.ValidateTokens(HttpContext, new AntiforgeryTokenSet(formToken, cookieToken));
            }
            catch (Exception)
            {
            }
            

            var cart = ShoppingCart.GetCart(DbContext, HttpContext);
            try
            {
                var cartItem = DbContext.Carts
                    .Where(item => item.ProductId == id).Include(x=>x.Product);
               
           
           

            int itemCount = cart.RemoveFromCart(id);

            await DbContext.SaveChangesAsync(requestAborted);

            string removed = (itemCount > 0) ? " 1 copy of " : string.Empty;
            
            var results = new ShoppingCartRemoveViewModel
            {
                Message = removed + /*cartItem.Product.Title +*/
                    " has been removed from your shopping cart.",
                CartTotal = await cart.GetTotal(),
                CartCount = await cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
                return Json(results);
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }
    }
}