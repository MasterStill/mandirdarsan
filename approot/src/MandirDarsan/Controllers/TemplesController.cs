﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using Microsoft.AspNet.Mvc;
namespace MandirDarsan.Controllers
{
    public class TemplesController : Controller
    {
        public static ApplicationDbContext Context;
        public IMandirRepository _repository;
        //IMandirRepository _repository 
        public TemplesController(ApplicationDbContext context)
        {
            Context = context;
            _repository = new MandirRepository(context);
        }
        public IActionResult Details(string culture,string id)
        {

            IEnumerable<Temple> temples = _repository.Temples(culture, null, 3).ToList();
            ViewData["Temple"] = temples;

            IEnumerable<Product> products = _repository.Products(culture, null, 3);
            ViewData["Products"] = products;

            var gods = _repository.Gods(culture, 4);
            ViewData["Gods"] = gods;

            var images = _repository.Gallery(4, "Temples", id);
            ViewData["Images"] = images;

            Temple temple = _repository.SingleTemple(culture, Guid.Parse(id));
            ViewBag.Title = temple.Title + " : " + temple.Country + " : " + temple.District;

            ViewBag.BreadCrumbFinalText = temple.Title;
            ViewBag.BreadCrumbHeader = temple.Title; 

            return View(temple);
        }
        public IActionResult Index(string culture)
        {
            
            ViewBag.Title = "Temple";
            return View(_repository.Temples(culture,"",12));
        }
        public IActionResult Maps(string culture)
        {
            return View();
        }
    }
}
