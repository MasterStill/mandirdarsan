﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using MandirDarsan.Areas.Admin.Models;
using PagedList;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Mvc;

namespace MandirDarsan.Controllers
{
    public class ServicesController : Controller
    {
        private ApplicationDbContext _db;
        private IMandirRepository _repository;
        public ServicesController (ApplicationDbContext db)
        {
            _repository = new MandirRepository(db);
            _db = db;
        }
        public ActionResult Index(string culture,string id="*")
        {
            if (culture == null)
            {
               // return Route("");
            }
           
            ServiceViewModel _svm = new ServiceViewModel();
            _svm.Packages = _repository.Packages(culture,3);
            _svm.Pujas = _repository.Puja(culture,3,"");
            if (id == "*")
            {
                id = "all";
            }
            ViewBag.IsotopeID = id;
            return View(_svm);
            //return View();
        }
        public ActionResult Details(string culture,string segment,string id)
        {
            ServiceViewModel sm = new ServiceViewModel();
            if (segment == "Puja")
            {
                sm.Puja = _repository.SinglePuja(culture, Guid.Parse(id));
                var images = _repository.Gallery(4, "Puja", id);
                ViewData["Images"] = images;
            }
            else{
                sm.Package = _repository.SinglePackage(culture, Guid.Parse(id));
                var images = _repository.Gallery(4, "Package", id);
                ViewData["Images"] = images;
            }
            sm.Pujas = _repository.Puja(culture, 4,null);
            sm.Packages = _repository.Packages(culture, 4);
            ViewBag.ServicesActive = "active";
            return View(sm);
        }
        public ActionResult Booking(string culture)
        {
            
            ViewBag.ServicesActive = "active";
           var package =_repository.Packages(culture,10);
            ViewData["package"] = package;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Booking(string culture,Book book)
        {
          
            var package = _repository.Packages(culture,10);
            ViewData["package"] = package;
            if (ModelState.IsValid)
            {
                return RedirectToAction("Message");
            }
            return View(book);
        }
        public ActionResult Message(string culture)
        {
            IMandirRepository _repository = new MandirRepository(_db);

            ViewBag.ServicesActive = "active";
            var package = _repository.Packages(culture,3);
            ViewData["package"] = package;
            return View();
        }
    }
}