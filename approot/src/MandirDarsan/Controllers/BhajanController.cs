﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Mvc;
namespace MandirDarsan.Controllers
{
    public class BhajanController : Controller
    {
        public static ApplicationDbContext _context;
        public IMandirRepository Repository;
        public BhajanController(ApplicationDbContext context)
        {
            _context = context;
            Repository = new MandirRepository(_context);
        }
       
        //public IActionResult Details(string culture,string segment, Guid id)
        //{
        //    LibraryViewModel lvm = new LibraryViewModel();
        //    lvm.Culture = culture;
        //    lvm.Library = Repository.SingleLibrary(culture, id);
        //    lvm.RelatedArticle = Repository.Library(culture, "Article", 3).ToList();
        //    lvm.Images = Repository.Gallery(3, "News",id.ToString());
        //    if (segment.ToLower() != "article")
        //    {
        //        lvm.RecentArticle = Repository.Library(culture, "Article", 3).ToList();
        //    }
        //    else
        //    {
        //        lvm.RecentArticle = Repository.Library(culture, "News", 3).ToList();
        //    }
        //    lvm.RecentPost= Repository.Library(culture, segment, 3).ToList();
        //    return View(lvm);
        //}
        public IActionResult Index(string culture)
        {
            var getall = Repository.Bhajan(culture, 10).ToList();
            return View(getall);    
        }
    }
}