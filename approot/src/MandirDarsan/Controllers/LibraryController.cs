﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Mvc;
namespace MandirDarsan.Controllers
{
    public class LibraryController : Controller
    {
        public static ApplicationDbContext _context;
        public IMandirRepository Repository;
        public LibraryController(ApplicationDbContext context)
        {
            _context = context;
            Repository = new MandirRepository(_context);
        }
        public IActionResult Details(string culture,string segment,string title, Guid id)
        {
            LibraryViewModel lvm = new LibraryViewModel();
            lvm.Culture = culture;
            lvm.Library = Repository.SingleLibrary(culture, id);
            lvm.RelatedArticle = Repository.Library(culture, "Article", 4).ToList();
            lvm.Images = Repository.Gallery(3, segment, id.ToString());
            if (segment.ToLower() != "article")
            {
                lvm.RecentArticle = Repository.Library(culture, "Article", 3).ToList();
            }
            else
            {
                lvm.RecentArticle = Repository.Library(culture, "News", 3).ToList();
            }
            ViewBag.Title = lvm.Library.Title;
            lvm.RecentPost= Repository.Library(culture, segment, 3).ToList();
            return View(lvm);
        }
        public IActionResult Content(string culture,string segment)
        {
            ViewBag.Title = "MandirDarsan Library : " + segment;
            var getall = Repository.Library(culture, segment, 10).ToList();
            return View(getall);    

        }
    }
}