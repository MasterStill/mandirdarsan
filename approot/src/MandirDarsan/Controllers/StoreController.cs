﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using MandirDarsan.Models;
using System.Threading.Tasks;
using System.Web;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.ViewModels.Mandir;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace MandirDarsan.Controllers
{
    public class StoreController : Controller
    {
        public static ApplicationDbContext _MContext;
        private IMandirRepository repository;
        public StoreController(ApplicationDbContext context)
        {
            repository = new MandirRepository(context);
            _MContext = context;
        }
        public ActionResult Index(string culture,string id)
        {
            ViewBag.ServicesActive = "active";
            var category = repository.Categories(culture);
            var products = repository.Products(culture, id);
            try
            {
                if (category.Any())
                {
                    ViewData["category"] = category;
                }
            }
            catch (Exception)
            {
                
            }
           try
            {
                if (products.Any())
                {
                    ViewData["products"] = products;
                }
            }
        catch
            (Exception)
            {
                    
            }
            return View();
        }
        public ActionResult Details(string culture,string title,Guid id)
        {
            ViewBag.ServicesActive = "active";
            var product = repository.SingleProducts(id,culture);
            if (product == null)
            {
                return new HttpStatusCodeResult(400);
            }
            List<Product> similarproducts = repository.Products(culture,null,4);
            ViewData["similarproducts"] = similarproducts;
            return View(product);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Details(OrderPhone orderPhone, Guid? id)
        //{
        //    var product = (from s in _MContext.Products where s.Id == id select s).ToList();
        //    ViewData["product"] = product;
        //    if (ModelState.IsValid)
        //    {
        //        _MContext.OrderPhones.Add(orderPhone);
        //        await _MContext.SaveChangesAsync();
        //        return RedirectToAction("../store/index");
        //    }

        //    return View(orderPhone);
        //}
   public ActionResult CartSummary()
        {
            return PartialView("CartSummary");
        }
    }
}