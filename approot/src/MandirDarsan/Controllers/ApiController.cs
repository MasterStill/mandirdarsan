﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MandirDarsan;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Net.Http.Headers;
using PagedList;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using MandirDarsan.Areas.Admin.Models;
using MandirDarsan.Models;
namespace MandirDarsan.Controllers
{
    public class ApiController : Controller
    {
        IApplicationEnvironment _hostingEnvironment;
        private IMandirRepository repository;
        private static ApplicationDbContext _context;
        private RockmanduBase rBase = new RockmanduBase();
        public ApiController(ApplicationDbContext context, IApplicationEnvironment hostingEnvironment)
        {
            repository = new MandirRepository(context);
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        public JsonResult God(string culture="English",string id=null)
        {
            if (id != null)
            {
                return Json(repository.SingleGod(culture, Guid.Parse(id)));
            }
            return Json(repository.Gods(culture, PageSize()).Select(x=> new {x.Id,x.Title,x.Summary,x.ImageUrl}));
        }
        public JsonResult News(string culture = "English", string id =  null)
        {
            if (id != null)
            {
                return Json(repository.SingleLibrary(culture, Guid.Parse(id)));
            }
            return Json(repository.Library(culture,"News",PageSize()));
        }
        public JsonResult Article(string culture = "English", string id =  null)
        {
            if (id != null)
            {
                return Json(repository.SingleLibrary(culture, Guid.Parse(id)));
            }
            return Json(repository.Library(culture,"Article",PageSize()));
        }
        public JsonResult Mantra(string culture = "English", string id = null)
        {
            //if (id != null)
            //{
            //    return Json(repository.SingleMantra(culture, Guid.Parse(id)));
            //}
            return Json(repository.Mantra(culture,PageSize()));
        }
        public JsonResult Events(string culture = "English", string id = null)
        {
            //return Json(_context..Select(x => new { x.Id, x.Title, x.ImageUrl, x.Summary }).ToList());
             return null;
        }
        public JsonResult Puja(string culture = "English", string id = null)
        {
            if (id != null)
            {
                return Json(repository.SinglePuja(culture, Guid.Parse(id)));
            }
            return Json(repository.Puja(culture,PageSize(),null));
        }
        public JsonResult Package(string culture = "English", string id = null)
        {
            if (id != null)
            {
                return Json(repository.SinglePackage(culture, Guid.Parse(id)));
            }
            return Json(repository.Packages(culture, PageSize()));
        }
        public JsonResult Bhajan(string culture = "English", string id = null)
        {
            if (id != null)
            {
                //return Json(repository.singleBhajan(culture, Guid.Parse(id)));
            }
            return Json(repository.Bhajan(culture, PageSize()));
        }
        public JsonResult Temple(string culture = "English", string id = null)
        {
            if (id != null)
            {
                return Json(repository.SingleTemple(culture, Guid.Parse(id)));
            }
            return Json(repository.Temples(culture, null, PageSize()));
        }
        public JsonResult Products(string culture = "English", string id = null) { 
            return Json(repository.Products(culture, null, PageSize()));
        }
        public string Languages(string lang)
        {
            lang = lang.ToLower();

            var jsonObject =
            _context.Globalizations.Where(x => x.Culture.Title.ToLower() == lang).Where(x => x.DelFlg == false).Select(x => new { x.LanguageTitle.Title, x.Value }).ToList();
            string json = "{ objects }";
            string jsval = string.Empty;
            foreach (var item in jsonObject)
            {
                jsval += "\"" + item.Title.Replace(Environment.NewLine, "").Trim() + "\"" + ":" + "\"" + item.Value.Replace(Environment.NewLine, "").Trim() + "\",";
            }
            if (jsval.Count() > 0)
            {
                jsval = jsval.Substring(0, jsval.Length - 1);
            }
            HttpContext.Session.SetString("Language", lang);
            return (json.Replace("objects", jsval));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        public int PageSize()
        {
            return 15;
        }
    }
}
