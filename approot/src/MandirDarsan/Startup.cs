﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MandirDarsan.Models;
using MandirDarsan.Services;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading;
using System.Web;
using hbehr.recaptcha;
//using Serilog;
//using Serilog.Events;
namespace MandirDarsan
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.

            string reCAPTCHASiteKey = "6LfTTBUTAAAAAKrbyKkpi6XwYLID9advNCJJcjXA";
            string reCAPTCHASecretKey = "6LfTTBUTAAAAAKrbyKkpi6XwYLID9advNCJJcjXA";
            ReCaptcha.Configure(reCAPTCHASiteKey, reCAPTCHASecretKey);

            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);


            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            //HttpContext.Session.SetString("Language","English");
            string language = "English";
            //var cookie = Request.Cookies.Get("__APPLICATION_LANGUAGE");
            //if (cookie != null)
            //    language = cookie.Value;
            //Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
        }

        public IConfigurationRoot Configuration { get; set; }

      //  public IConfiguration config { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

        //   
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddCors();
           
            //var policy = new Microsoft.AspNet.Cors.EnableCorsAttribute()

            //policy.Headers.Add("*");
            //policy.Methods.Add("*");
            //policy.Origins.Add("*");
            //policy.SupportsCredentials = true;

            //services.ConfigureCors(x => x.AddPolicy("mypolicy", policy));


            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));
               //.AddDbContext<ApplicationDbContext>(options =>
               //     options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddCaching();
            services.AddSession();
            services.AddMvc();
            

            // Add Logging
            services.AddLogging();
            // Add application services.




            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddSingleton<ILibrary, LibraryRepository>();
            //services.AddSingleton<IMandirRepository, MandirRepository>();
            services.Configure<AuthMessageSender>(Configuration);
            //services.AddTransient<IMandirRepository, MandirRepository>();
        }

        //public static LoggerConfiguration GetLoggerConfiguration()
        //{
        //    return new LoggerConfiguration()
        //        .Enrich.WithMachineName()
        //        .Enrich.WithProcessId()
        //        .Enrich.WithThreadId()
        //        .MinimumLevel.Debug()
        //        .WriteTo.RollingFile(@"c:\users\maste\desktop\errors\MandirDarsan\Log{Date}.txt",
        //            outputTemplate:
        //                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {Level}:{EventId} [{SourceContext}] {Message}{NewLine}{Exception}");
        //}
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseSession();

            app.UseCors(builder =>
            builder.WithOrigins("http://localhost:4400")
            .AllowAnyHeader()
            );

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // For more details on creating database during deployment see http://go.microsoft.com/fwlink/?LinkID=615859
                try
                {
                    using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                        .CreateScope())
                    {
                        serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                            .Database.Migrate();
                    }
                }
                catch
                {

                }
            }
            app.UseIISPlatformHandler(options => options.AuthenticationDescriptions.Clear());

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();
            //app.UseDeveloperExceptionPage();
            app.UseIdentity();
           
            // To configure external authentication please see http://go.microsoft.com/fwlink/?LinkID=532715
            app.UseFacebookAuthentication(options =>
            {
                options.AppId = "1448838608684709";
                options.AppSecret = "bbc2e3be8cb19ae129959de0277373f0";
            });

            //app.UseGoogleAuthentication(options =>
            //{
            //    options.ClientId = "";
            //    options.ClientSecret = "";
            //});

            app.UseSession();
            app.UseMvc(routes =>
            {

              //  routes.MapRoute(
              //name: "AccountUserLanguagesetGarneWala",
              //template: "Account/AddLanguageToUser/",
              //defaults: new { controller = "Account", action = "AddLanguageToUser" });


                //        routes.MapRoute(
                //name: "Languageselection",
                //    template: "/{area:exists}/Home/{Action}",
                //    defaults: new { Areas = "Admin", controller = "Home", action = "LanguageSelection" });


                routes.MapRoute(
           name: "ServicesDetails",
               template: "{culture}/Services/Details/{segment}/{title}/{id}",
               defaults: new { controller = "Services", action = "Details" });

                routes.MapRoute(
         name: "UserEditDetails",
             template: "{area:exists}/EditUserDetails",
             defaults: new { area="User", controller = "Home", action = "EditUserDetails" });

                routes.MapRoute(
                  name: "Contacts",
                  template: "Home/Contact/",
                  defaults: new { controller = "Home", action = "Contact"});


                //routes.MapRoute(
                // name: "AddToShopping",
                // template: "ShoppingCart/AddToCart/{id}/{qty}",
                // defaults: new { controller = "Home", action = "Contact" });
                routes.MapRoute(
           name: "AccountRegister",
           template: "{culture}Account/Register/",
           defaults: new { culture="English" , controller = "Account", action = "Register" });
                routes.MapRoute(
              name: "AccountRoleWala",
              template: "Account/AddToRoleUser/",
              defaults: new { controller = "Account", action = "AddToRoleUser" });
                routes.MapRoute(
            name: "Bhajan",
            template: "{culture}/Bhajan/{action}/",
            defaults: new { controller = "Bhajan", action = "Index" });
                //   routes.MapRoute(
                //name: "UserTemple",
                //template: "{area:exists}/Temples/{id?}",
                //defaults: new { controller = "Temples", action = "Index" });

                routes.MapRoute(
               name: "UserArea",
               template: "{area:exists}/Home/{id?}",
               defaults: new { Areas = "Admin", controller = "Home", action = "Index" });
                routes.MapRoute(
                name: "GalleryArea",
                template: "{area:exists}/Galleries/{action}",
                defaults: new { Areas = "Admin", controller = "Galleries"});
                routes.MapRoute(
               name: "GalleryAreaDelete",
               template: "{area:exists}/Galleries/{action}/{ctrl}/{cid?}",
               defaults: new { Areas = "Admin", controller = "Galleries", action = "Index" });
                routes.MapRoute(
                name: "areaRoute",
                template: "{area:exists}/{controller}/{action}/{id?}",
                defaults: new { Areas = "Admin", controller = "Home", action = "Index" });
                routes.MapRoute(
                name: "TempleDetails",
                template: "{culture}/Temples/Details/{title}/{id?}",
                defaults: new { controller = "Temples", action = "Details" });
                routes.MapRoute(
              
               name: "ArticleDetails",
               template: "{culture}/Library/Details/{segment}/{title}/{id}",
               defaults: new { controller = "Library", action = "Details" });

                routes.MapRoute(
                name: "Article",
                template: "{culture}/Library/Content/{segment}",
                defaults: new { controller = "Library", action = "Content" });

                routes.MapRoute(
                 name: "GodDetails",
                 template: "{culture}/Gods/Details/{title}/{id?}",
                 defaults: new { controller = "Gods", action = "Details" });

                routes.MapRoute(
                 name: "Store",
                 template: "{culture}/Store/{title}/{id?}",
                 defaults: new { controller = "Store", action = "Index" });
                routes.MapRoute(
                  name: "StoreBrowse",
                  template: "{culture}/Store/Details/{title?}/{id?}",
                  defaults: new { controller = "Store", action = "Details" });

                routes.MapRoute(
                name: "ShoppingCart",
                template: "{culture}/ShoppingCart/{action}",
                defaults: new { controller = "ShoppingCart", action = "Index" });

                routes.MapRoute(
                 name: "OtherNormal",
                 template: "{culture}/{controller}/{action}/{id?}",
                 defaults: new { controller = "Home", action = "Index",culture="English" });

                routes.MapRoute(
                  name: "Account",
                  template: "{controller}/{action}/{id?}",
                  defaults: new { controller = "Account", action = "Index" ,culture="English"});



            });
           // SampleData.InitializeMandirDarsanDatabaseAsync(app.ApplicationServices).Wait();
        }
        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}