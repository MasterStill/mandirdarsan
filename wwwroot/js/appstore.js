$(function () {

    var filtername = $("#user-selector").text();
    if( filtername === '*'){
        var  filtername = '*' ;
    }else{
        var filtername ='.'+filtername;
    };
    
    var $container = $('.filter-group');

    // bind filter button click
    var $filters = $('.filter-button-group').on('click', 'button', function () {
        var filterAttr = $(this).attr('data-filter');
        // set filter in hash
        $container.isotope({filter: filterAttr});
         $(this).addClass('btn-primary').siblings().removeClass('btn-primary');
    });

    var isIsotopeInit = false;

    function onHashchange() {
        var hashFilter = filtername;
        
        if (!hashFilter && isIsotopeInit) {
            return;
        }
        isIsotopeInit = true;
        // filter isotope
        $container.isotope({
            itemSelector: '.service-item',
            filter: hashFilter
        });
        // set selected class on button
        if (hashFilter) {
            $filters.find('.btn-primary').removeClass('btn-primary');
            $filters.find('[data-filter="' + hashFilter + '"]').addClass('btn-primary');
        }
    }
  
 $(window).on( 'hashchange', onHashchange );
  // trigger event handler to init Isotope
  onHashchange();


});

$(function () {

    $('.search-form a').click(function () {
        $('#search-header').slideToggle('slow');
    });




    $('.c-mobile-menu').click(function () {
        $('.menu-overlay').toggleClass('open');
    });

    $('#banner-slider').bxSlider({
        mode: "horizontal",
        pager: false,
        captions: true
    });
    $('.service-slider').bxSlider({
        mode: "horizontal",
        pager: false,
        captions: true
    });
    $('.image-gallery').bxSlider({
        minSlides: 10,
        maxSlides: 10,
        pager: false,
        controls: false,
        slideWidth: 300,
        speed: 800,
        moveSlides: 1,
        pause: 5000,
        auto: true,
        autoControls: true
    });
    $('.image-gallery1').bxSlider({
        minSlides: 10,
        maxSlides: 10,
        pager: false,
        controls: false,
        slideWidth: 300,
        speed: 700,
        auto: true,
        moveSlides: 1,
        autoControls: true
    });

    $(".gallery-popup").fancybox({
        padding: 2
    });

    $('#product-gallery').bxSlider({
        pagerCustom: '#product-gallery-pager'
    });



    if ($('#map-canvas').length > 0) {
        var myLatlng = new google.maps.LatLng(27.7089603, 85.3261328);
        function initialize() {
            // Create the map.
            var mapOptions = {
                zoom: 15,
                scrollwheel: false,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    }



});




var sf, body;
var breakpoint = 992;
jQuery(document).ready(function ($) {
    body = $('body');
    sf = $('ul.sf-menu');
    if (body.width() >= breakpoint) {
        // enable superfish when the page first loads if we're on desktop
        sf.superfish();
    }
    $(window).resize(function () {
        if (body.width() >= breakpoint && !sf.hasClass('sf-js-enabled')) {
            // you only want SuperFish to be re-enabled once (sf.hasClass)
            sf.superfish('init');
        } else if (body.width() < breakpoint) {
            // smaller screen, disable SuperFish
            sf.superfish('destroy');
        }
    });
});

jQuery(document).ready(function () {
    // This button will increment the value
    $('.plus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".minus").click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(0);
        }
    });
});


(function($) {
    
  var allPanels = $('.nav-lists li ul').hide();
  $('.nav-lists li ul.acc-show').show();
    
  $('.nav-lists > li > a').click(function() {
      $this = $(this);
      
      $target =  $this.next();
      if(!$target.hasClass('active')){
         allPanels.removeClass('active').slideUp();
         $target.addClass('active').slideDown();
      }
  });

})(jQuery); 