﻿(function () {
    'use strict';

    angular.module('MeroApp', [
        'MeroService'
    ]);
})();


//(function () {
//    'use strict';

//    angular.module('Tero', [
//        'TeroService'
//    ]);
//})();


(function() {
    'use strict';

    angular
        .module('MeroApp')
        .controller('MeroController', MeroController);

    MeroController.$inject = ['$scope', 'Mero'];
    //TeroController.$inject = ['$scope', 'Tero'];

    function MeroController($scope, Mero,kcha) {
        $scope.Mero = Mero.query();
    }

})();



//(function () {
//    'use strict';

//    angular
//        .module('TeroApp')
//        .controller('MeroController', TeroController);

//    TeroController.$inject = ['$scope', 'Tero'];
//    //TeroController.$inject = ['$scope', 'Tero'];

//    function TeroController($scope, Tero) {
//        $scope.Tero = Tero.query();
//    }

//})();

(function () {
    'use strict';
    var meroService = angular.module('MeroService', ['ngResource']);

    meroService.factory('Mero', ['$resource',
        function ($resource) {
            return $resource('/Admin/Library/GetLib/', {}, {
                query: { method: 'GET', params: {}, isArray: true }
            });
        }
    ]);
})();

//(function () {
//    'use strict';
//    var meroService = angular.module('TeroService', ['ngResource']);

//    meroService.factory('Tero', ['$resource',
//        function ($resource) {
//            return $resource('/Admin/Library/GetCulture/', {}, {
//                query: { method: 'GET', params: {}, isArray: true }
//            });
//        }
//    ]);
//})();